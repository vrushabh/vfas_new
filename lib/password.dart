import 'package:flutter/material.dart';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:vfas/otp.dart';

void main() {
  runApp(MaterialApp(home: Password()));
}

// ignore: must_be_immutable
class Password extends StatefulWidget {
  Password({this.name, this.domain});
  String name = '';
  String domain = '';

  @override
  _PasswordState createState() => _PasswordState();
}

class _PasswordState extends State<Password> {
  var  _passwordVisible = true;
   
  ButtonState stateTextWithIcon = ButtonState.idle;

  Widget buildTextWithIcon() {
    return ProgressButton.icon(iconedButtons: {
      ButtonState.idle: IconedButton(
          text: "Next",
          icon: Icon(Icons.login, color: Colors.white),
          color: Colors.orangeAccent[700]),
      ButtonState.loading:
          IconedButton(text: "Loading", color: Colors.orangeAccent[700]),
      ButtonState.fail: IconedButton(
          text: "Failed",
          icon: Icon(Icons.cancel, color: Colors.white),
          color: Colors.red.shade300),
      ButtonState.success: IconedButton(
          text: "",
          icon: Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          color: Colors.green.shade400)
    }, onPressed: onPressedIconWithText, state: stateTextWithIcon);
  }

  void onPressedIconWithText() {
    switch (stateTextWithIcon) {
      case ButtonState.idle:
        stateTextWithIcon = ButtonState.loading;
        if (passwordcntl.text == '') {
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: Text('Please Enter Password'),
                    actions: <Widget>[
                      TextButton(
                          onPressed: () {
                            setState(() {
                              stateTextWithIcon = ButtonState.idle;
                            });
                            Navigator.pop(context, false);
                          },
                          child: Text('OK'))
                    ],
                  ));
        } else {
          Future.delayed(Duration(seconds: 5), () async {
            var body = {
              'domain': widget.domain,
              'encType': 'IOS',
              'isSSORegisterationCheck': 'true',
              'login': widget.name,
              'pwd': passwordcntl.text
            };
            // print(body);
            var response = await http.post(
                Uri.parse(
                    'https://dfws.bseindia.com/vfs/VMSService.svc/isOMValidUserCredentials'),
                headers: {
                  'Content-Type': 'application/json',
                  'Charset': 'utf-8'
                },
                body: jsonEncode(body));

            var data = json.decode(response.body);
            print(data);
            print(data['data']);
            if (data['data'] == 'Invalid Credentials !!!') {
              showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                        title: Text(data['data']),
                        actions: <Widget>[
                          TextButton(
                              onPressed: () {
                                setState(() {
                                  stateTextWithIcon = ButtonState.idle;
                                });
                                Navigator.pop(context, false);
                              },
                              child: Text('OK'))
                        ],
                      ));
            } else {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new OTP(userdata: data),
                ),
              ).then((value) => setState(() {
                    stateTextWithIcon = ButtonState.idle;
                  }));
            }
          });
        }

        break;
      case ButtonState.loading:
        break;
      case ButtonState.success:
        stateTextWithIcon = ButtonState.idle;
        break;
      case ButtonState.fail:
        stateTextWithIcon = ButtonState.idle;
        break;
    }
    setState(() {
      stateTextWithIcon = stateTextWithIcon;
    });
  }

  TextEditingController passwordcntl = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    print(widget.name);
    print(widget.domain);
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/back.jpg'), fit: BoxFit.cover)),
        child: Container(
          margin: EdgeInsets.only(
            left: 20,
            right: 20,
          ),
          height: 250,
          width: double.infinity,
          child: Card(
            color: Colors.white.withOpacity(0.6),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Text(
                      widget.name,
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Colors.lightBlue[600]),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Text(
                        'Password',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      )),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 10.0, right: 10.0, top: 10.0),
                      child: TextField(
                        controller: passwordcntl,
                        obscureText: _passwordVisible,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.orangeAccent[700], width: 2.0)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.orangeAccent[700], width: 2.0)),
                          labelText: 'Enter Password',
                          labelStyle: TextStyle(
                              color: Colors.orangeAccent[700], fontSize: 17.0),
                          hintText: 'Password',
                          hintStyle: TextStyle(fontSize: 15.0),
                          suffixIcon: IconButton(
                              onPressed: () {
                                setState(() {
                                  _passwordVisible = !_passwordVisible;
                                });
                              },
                              icon: Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Icon(
                                  _passwordVisible
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                ),
                              )),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: buildTextWithIcon()),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
