import 'package:flutter/material.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:vfas/notification.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/office_manager/canteen.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/holiday.dart';
import 'package:vfas/office_manager/myAttendance.dart';
import 'package:vfas/office_manager/myProfile.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

import 'package:vfas/sidemenu.dart';

void main() {
  runApp(MaterialApp(home: OMNotify()));
}

class OMNotify extends StatefulWidget {
  @override
  _OMNotifyState createState() => _OMNotifyState();
}

class _OMNotifyState extends State<OMNotify> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();

  static int refreshNum = 10;
  Stream<int> counterStream =
      Stream<int>.periodic(Duration(seconds: 3), (x) => refreshNum);
  var visibleApprove = false;

  List entries = [];
  var loadingData = true;
  String Email = '', employeecode = '', employeetype = '';
  String Username = '';
  var name = '';
  String noData = 'false';

  @override
  void initState() {
    super.initState();
    this.getNotification();
  }

  getNotification() async {
    setState(() {
      loadingData = true;
    });
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      Email = preferences.getString('email');
      employeecode = preferences.getString('employeecode');
      employeetype = preferences.getString('employeetype');
      Username = preferences.getString('username');

      name = Username.split('.')[0];

      var body = {'emp_id': employeecode, 'employeeType': employeetype};

      print(body);
      print(jsonEncode(body));

      var response = await http.post(
          Uri.parse(
              'https://www.mkttech.in/OMService/OMJsonService.svc/ReceiveNotification'),
          headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        entries = data;
        loadingData = false;
      });
      entries.length > 0
          ? setState(() {
              noData = 'false';
            })
          : setState(() {
              noData = 'true';
            });
    } catch (e) {
      setState(() {
        noData = 'true';
        loadingData = false;
      });
    }
  }

  Future<void> _handleRefresh() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      Email = preferences.getString('email');
      employeecode = preferences.getString('employeecode');
      employeetype = preferences.getString('employeetype');
      Username = preferences.getString('username');

      name = Username.split('.')[0];

      var body = {'emp_id': employeecode, 'employeeType': employeetype};

      print(body);
      print(jsonEncode(body));

      var response = await http.post(
          Uri.parse(
              'https://www.mkttech.in/OMService/OMJsonService.svc/ReceiveNotification'),
          headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        entries = data;
        loadingData = false;
      });
      entries.length > 0
          ? setState(() {
              noData = 'false';
            })
          : setState(() {
              noData = 'true';
            });
    } catch (e) {
      setState(() {
        noData = 'true';
        loadingData = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
          appBar: AppBar(
            //centerTitle: true,
            backgroundColor: const Color(0xFF008577),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                final _state = _sideMenuKey.currentState;
                if (_state.isOpened)
                  _state.closeSideMenu();
                else
                  _state.openSideMenu();
              },
            ),
            actions: <Widget>[Count()],
            title: Text('OM Notification'),
          ),
          body: Container(
            child: loadingData == true
                ? Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(),
                  )
                : noData == 'true'
                    ? Center(
                        child: Container(
                          child: Text(
                            'No Data Found',
                            style: TextStyle(
                                fontSize: 23,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                          ),
                        ),
                      )
                    : Column(
                        children: [
                          Expanded(
                            child: LiquidPullToRefresh(
                              color: const Color(0xFF008577),
                              key: _refreshIndicatorKey,
                              onRefresh: _handleRefresh,
                              showChildOpacityTransition: true,
                              child: StreamBuilder<Object>(
                                  stream: null,
                                  builder: (context, snapshot) {
                                    return ListView.builder(
                                        padding: const EdgeInsets.all(8),
                                        itemCount: entries.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Container(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey[400],
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10.0),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        //color: Colors.red,
                                                        blurRadius:
                                                            7.0, // soften the shadow
                                                        spreadRadius:
                                                            1.0, //extend the shadow
                                                        offset: Offset(
                                                          1.0, // Move to right 10  horizontally
                                                          1.0, // Move to bottom 10 Vertically
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  height: 100,
                                                  child: Column(
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                top: 13,
                                                                right: 10),
                                                        child: Container(
                                                          alignment: Alignment
                                                              .centerRight,
                                                          child: Text(
                                                            entries[index]
                                                                ['date'],
                                                            style: TextStyle(),
                                                          ),
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(15.0),
                                                        child: Container(
                                                          alignment: Alignment
                                                              .centerRight,
                                                          child: Text(
                                                            entries[index]
                                                                ['msg'],
                                                            style: TextStyle(),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  )),
                                            ),
                                          );
                                        });
                                  }),
                            ),
                          ),
                        ],
                      ),
          ),
        ),
      ),
    );
  }
}
