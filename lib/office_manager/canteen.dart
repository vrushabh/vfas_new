import 'package:flutter/material.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:vfas/notification.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/holiday.dart';
import 'package:vfas/office_manager/myAttendance.dart';
import 'package:vfas/office_manager/myProfile.dart';
import 'package:vfas/office_manager/omNotification.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:ui';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/sidemenu.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

void main() {
  runApp(MaterialApp(home: Canteen()));
}

class Canteen extends StatefulWidget {
  @override
  _CanteenState createState() => _CanteenState();
}

class _CanteenState extends State<Canteen> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();

  static int refreshNum = 10;
  Stream<int> counterStream =
      Stream<int>.periodic(Duration(seconds: 3), (x) => refreshNum);
  var visibleApprove = false;
  bool loading = true;
  List month = [
    '',
    'January',
    'February',
    'March',
    'April',
    'May',
    'Jun',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  final dateController = TextEditingController();
  final yearController = TextEditingController();
  final monthController = TextEditingController();
  var fullDate;
  var monthNo;
  var details;
  var empcode = '';
  var empname = '';
  var emptotal = '';
  var finalmonth1;
  var finalyear;
  var finalmonth;
  String code = '';
  String type = '';
  @override
  void initState() {
    super.initState();
    DateTime date = DateTime.now();
    var monthFormat = new DateFormat('MM');
    var monthFormat1 = new DateFormat('MMM');
    var yearFormat1 = new DateFormat('yyyy');
    DateTime year = DateFormat("yyyy-MM-dd HH:mm:ss").parse(date.toString());
    //var finalmonth = monthFormat.format(year);
    // var finalyear = yearFormat1.format(year);
    setState(() {
      finalyear = yearFormat1.format(year);
      finalmonth = monthFormat.format(year);
    });
    finalmonth1 = monthFormat1.format(year);
    print(finalyear);
    monthController.text = finalmonth1;
    yearController.text = finalyear;

    getDetails(finalyear, finalmonth);
  }

  getDetails(yeardate, month) async {
    setState(() {
      loading = true;
    });

    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();

      code = preferences.getString('employeecode');
      type = preferences.getString('employeetype');

      var body = {
        "emp_id": code,
        "employeeType": type,
        "month": month,
        "year": yeardate
      };
      print(body);

      var response = await http.post(
          Uri.parse(
              'https://www.mkttech.in/OMService/OMJsonService.svc/getCanteenExpense'),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Charset': 'utf-8'
          },
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        details = data;
        loading = false;
        empcode = data['emp_code'] == null ? '' : data['emp_code'];
        empname = data['emp_firstname'] == null
            ? ''
            : data['emp_firstname'] + ' ' + data['emp_lastname'];
        emptotal = data['final_total'] == null ? '' : data['final_total'];
      });
      print(details);
    } catch (e) {
      setState(() {
        loading = false;
        details = null;
      });
    }
  }

  Future<void> _handleRefresh() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();

      code = preferences.getString('employeecode');
      type = preferences.getString('employeetype');

      var body = {
        "emp_id": code,
        "employeeType": type,
        "month": finalmonth,
        "year": finalyear
      };
      print(body);

      var response = await http.post(
          Uri.parse(
              'https://www.mkttech.in/OMService/OMJsonService.svc/getCanteenExpense'),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Charset': 'utf-8'
          },
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        details = data;
        loading = false;
        empcode = data['emp_code'] == null ? '' : data['emp_code'];
        empname = data['emp_firstname'] == null
            ? ''
            : data['emp_firstname'] + ' ' + data['emp_lastname'];
        emptotal = data['final_total'] == null ? '' : data['final_total'];
      });
      print(details);
    } catch (e) {
      setState(() {
        loading = false;
        details = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
          appBar: AppBar(
            //centerTitle: true,
            backgroundColor: const Color(0xFF008577),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                final _state = _sideMenuKey.currentState;
                if (_state.isOpened)
                  _state.closeSideMenu();
                else
                  _state.openSideMenu();
              },
            ),
            actions: <Widget>[Count()],
            title: Text('Canteen Details'),
          ),
          body: Center(
            child: loading == true
                ? Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    children: [
                      Container(
                        height: 60,
                        width: double.infinity,
                        color: const Color(0xFF008577),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 3,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: TextField(
                                    readOnly: true,
                                    controller: yearController,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        //  border: InputBorder.none,
                                        //  hintText: 'Select Year',
                                        hintStyle:
                                            TextStyle(color: Colors.white)),
                                    onTap: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: Text("Select Year"),
                                            content: Container(
                                              width: 300,
                                              height: 300,
                                              child: YearPicker(
                                                firstDate: DateTime(
                                                    DateTime.now().year - 10,
                                                    1),
                                                lastDate: DateTime(
                                                    DateTime.now().year + 30,
                                                    1),
                                                initialDate: DateTime.now(),
                                                selectedDate: DateTime.now(),
                                                onChanged: (DateTime dateTime) {
                                                  print(dateTime);
                                                  setState(() {
                                                    fullDate = dateTime;
                                                  });
                                                  yearController.text = dateTime
                                                      .toString()
                                                      .substring(0, 4);
                                                  Navigator.pop(context);
                                                },
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                  ),
                                )),
                            // Expanded(flex: 2,child: Text(''),),
                            Expanded(
                                flex: 3,
                                child: Container(
                                    // alignment: Alignment.bottomCenter,
                                    height: 60,
                                    color: const Color(0xFF008577),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15, top: 6),
                                      child: TextField(
                                        readOnly: true,
                                        controller: monthController,
                                        style: TextStyle(color: Colors.white),
                                        decoration: InputDecoration(
                                            // border: InputBorder.none,
                                            // hintText: 'Select Month',
                                            hintStyle:
                                                TextStyle(color: Colors.white)),
                                        onTap: () {
                                          showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                title: Text("Select Month"),
                                                content: Container(
                                                    child: ListView.builder(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8),
                                                        itemCount: month.length,
                                                        itemBuilder:
                                                            (BuildContext
                                                                    context,
                                                                int index) {
                                                          return Container(
                                                            decoration: BoxDecoration(
                                                                border: Border(
                                                                    bottom: BorderSide(
                                                                        width:
                                                                            1.0,
                                                                        color: Colors
                                                                            .grey[400]))),
                                                            alignment: Alignment
                                                                .center,
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      top: 5,
                                                                      bottom:
                                                                          5),
                                                              child: TextButton(
                                                                child: Text(
                                                                  month[index],
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        20,
                                                                  ),
                                                                ),
                                                                onPressed: () {
                                                                  print(index);
                                                                  setState(() {
                                                                    monthNo =
                                                                        index;
                                                                  });
                                                                  monthController
                                                                          .text =
                                                                      month[
                                                                          index];
                                                                  Navigator.pop(
                                                                      context,
                                                                      false);
                                                                },
                                                              ),
                                                            ),
                                                          );
                                                        })),
                                                actions: [
                                                  TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(
                                                            context, false);
                                                      },
                                                      child: Text('Cancel')),
                                                ],
                                              );
                                            },
                                          );
                                        },
                                      ),
                                    ))),
                            Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 15),
                                  child: Container(
                                    alignment: Alignment.centerRight,
                                    child: IconButton(
                                        onPressed: () {
                                          // print(fullDate);
                                          print(monthNo);
                                          getDetails(
                                              yearController.text, monthNo);
                                         
                                        },
                                        icon: Icon(Icons.search,
                                            size: 35, color: Colors.white)),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      details['subCanteenExpense'] == null
                          ? Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 130),
                                child: Container(
                                  child: Text(
                                    'Data Not Found',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 22,
                                        color: Colors.red),
                                  ),
                                ),
                              ),
                            )
                          : Expanded(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: 50,
                                    color: Colors.grey[700],
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 5, left: 5),
                                      child: Row(
                                        children: [
                                          Expanded(
                                              flex: 3,
                                              child: Center(
                                                  child: Text(
                                                'Employee Code',
                                                maxLines: 1,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ))),
                                          Expanded(
                                              flex: 5,
                                              child: Center(
                                                  child: Text(
                                                'Name',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ))),
                                          Expanded(
                                              flex: 1,
                                              child: Center(
                                                  child: Text(
                                                'Total',
                                                maxLines: 1,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              )))
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 50,
                                    color: Colors.grey[500],
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 5, left: 5),
                                      child: Row(
                                        children: [
                                          Expanded(
                                              flex: 3,
                                              child: Center(
                                                  child: Text(
                                                empcode,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ))),
                                          Expanded(
                                              flex: 5,
                                              child: Center(
                                                  child: Text(
                                                empname,
                                                maxLines: 1,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ))),
                                          Expanded(
                                              flex: 2,
                                              child: Center(
                                                  child: Text(
                                                emptotal,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              )))
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 50,
                                    color: Colors.green[300],
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 3,
                                            child: Text('Date',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white)),
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: Text('Slot',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white)),
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: Text('Remark',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white)),
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: Text('Amount',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white)),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      child: LiquidPullToRefresh(
                                    color: const Color(0xFF008577),
                                    key: _refreshIndicatorKey,
                                    onRefresh: _handleRefresh,
                                    showChildOpacityTransition: true,
                                    child: StreamBuilder<Object>(
                                        stream: null,
                                        builder: (context, snapshot) {
                                          return ListView.builder(
                                              padding: const EdgeInsets.all(8),
                                              itemCount:
                                                  details['subCanteenExpense']
                                                      .length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return Container(
                                                    height: 50,
                                                    width: double.infinity,
                                                    decoration: BoxDecoration(
                                                        border: Border(
                                                            bottom: BorderSide(
                                                                width: 1.0,
                                                                color:
                                                                    Colors.grey[
                                                                        400]))),
                                                    child: Row(children: [
                                                      Expanded(
                                                          flex: 3,
                                                          child: Container(
                                                              // alignment: Alignment.center,
                                                              child: Text(
                                                                  details['subCanteenExpense']
                                                                          [
                                                                          index]
                                                                      [
                                                                      'canteen_date'],
                                                                  style: TextStyle(
                                                                      fontSize:
                                                                          16,
                                                                      color: Colors
                                                                              .blue[
                                                                          300])))),
                                                      Expanded(
                                                          flex: 2,
                                                          child: Container(
                                                              // alignment: Alignment.center,
                                                              child: Text(
                                                                  details['subCanteenExpense']
                                                                          [
                                                                          index]
                                                                      ['slot'],
                                                                      maxLines: 1,
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                  )))),
                                                      Expanded(
                                                          flex: 2,
                                                          child: Center(
                                                            child: Container(
                                                                // alignment: Alignment.center,
                                                                child: Text(
                                                                    details['subCanteenExpense']
                                                                            [
                                                                            index]
                                                                        [
                                                                        'remarks'],
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          16,
                                                                    ))),
                                                          )),
                                                      Expanded(
                                                          flex: 2,
                                                          child: Center(
                                                            child: Container(
                                                                // alignment: Alignment.center,
                                                                child: Text(
                                                                    details['subCanteenExpense']
                                                                            [
                                                                            index]
                                                                        [
                                                                        'amount'],
                                                                        maxLines: 1,
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          16,
                                                                    ))),
                                                          )),
                                                    ]));
                                              });
                                        }),
                                  ))
                                ],
                              ),
                            ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
