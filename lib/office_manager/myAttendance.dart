import 'package:flutter/material.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:vfas/dashboard.dart';
import 'package:vfas/notification.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/office_manager/canteen.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/holiday.dart';
import 'package:vfas/office_manager/myProfile.dart';
import 'package:vfas/office_manager/omNotification.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:ui';
import 'dart:async';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/sidemenu.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

void main() {
  runApp(MaterialApp(home: Attendance()));
}

class Attendance extends StatefulWidget {
  @override
  _AttendanceState createState() => _AttendanceState();
}

class _AttendanceState extends State<Attendance> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();

  static int refreshNum = 10;
  Stream<int> counterStream =
      Stream<int>.periodic(Duration(seconds: 3), (x) => refreshNum);
  var visibleApprove = false;
  List teamAttendance = [];
  List myAttendance = [];
  bool loading = true;
  List month = [
    '',
    'January',
    'February',
    'March',
    'April',
    'May',
    'Jun',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  final dateController = TextEditingController();
  final yearController = TextEditingController();
  final monthController = TextEditingController();
  var fullDate;
  var monthNo;
  String code = '';
  String type = '';
  String url = '';
  bool showMyAttendanceModule = true;
  var name = '';
  String Username = '';
  var finaldate;
  var monthFormat;
  var finalmonth;

  @override
  void initState() {
    super.initState();
    // DateTime date = DateTime.now();
    // DateTime date1 = DateFormat("yyyy-MM-dd HH:mm:ss").parse(date.toString());
    var df = new DateFormat('yyyy-MM-dd');
    // var finaldate = df.format(date1);
    setState(() {
      DateTime date = DateTime.now();
      DateTime date1 = DateFormat("yyyy-MM-dd HH:mm:ss").parse(date.toString());
      finaldate = df.format(date1);
      monthFormat = new DateFormat('MM');
      DateTime year = DateFormat("yyyy-MM-dd HH:mm:ss").parse(date.toString());
      finalmonth = monthFormat.format(year);
      getManager();
      getAttendance(finaldate);
      getMyAttendance(date, finalmonth);
    });
    // var monthFormat = new DateFormat('MM');

    // var finalmonth = monthFormat.format(year);
  }

  getManager() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Username = preferences.getString('username');

    name = Username.split('.')[0];
    code = preferences.getString('employeecode');
    type = preferences.getString('employeetype');
    url = type == 'Marketplace Employee'
        ? 'https://www.mkttech.in/OMService/OMJsonService.svc/'
        : 'https://dfws.bseindia.com/OMService/OMJsonService.svc/';
    var body = {"emp_id": code, "employeeType": type};

    //G020014
    var response = await http.post(Uri.parse(url + 'getManager'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Charset': 'utf-8'
        },
        body: jsonEncode(body));

    var data = json.decode(response.body);
    // print(data);
    data['flag'] == '1'
        ? setState(() {
            showMyAttendanceModule = false;
          })
        : setState(() {
            showMyAttendanceModule = true;
          });
  }

  getAttendance(dt) async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      code = preferences.getString('employeecode');
      type = preferences.getString('employeetype');
      print(dt);
      setState(() {
        loading = true;
      });
      var body = {"date": dt, "emp_id": code, "employeeType": type};
      print(body);

      var response = await http.post(
          Uri.parse(
              'https://www.mkttech.in/OMService/OMJsonService.svc/getTeamAttendance'),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Charset': 'utf-8'
          },
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        teamAttendance = data;
        loading = false;
      });
      // print(data);
      print(teamAttendance);
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  Future<void> _handleRefreshTeamAttendance() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      code = preferences.getString('employeecode');
      type = preferences.getString('employeetype');
     // print(dt);
      setState(() {
        loading = true;
      });
      var body = {"date": finaldate, "emp_id": code, "employeeType": type};
      print(body);

      var response = await http.post(
          Uri.parse(
              'https://www.mkttech.in/OMService/OMJsonService.svc/getTeamAttendance'),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Charset': 'utf-8'
          },
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        teamAttendance = data;
        loading = false;
      });
      // print(data);
      print(teamAttendance);
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  getMyAttendance(yeardate, month) async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      code = preferences.getString('employeecode');
      type = preferences.getString('employeetype');
     
      var yearFormat = new DateFormat('yyyy');
      DateTime year =
          DateFormat("yyyy-MM-dd HH:mm:ss").parse(yeardate.toString());

      var finalyear = yearFormat.format(year);

      var body = {
        "emp_id": code,
        "employeeType": type,
        "month": month,
        "year": finalyear
      };
      print(body);

      var response = await http.post(
          Uri.parse(
              'https://www.mkttech.in/OMService/OMJsonService.svc/getAttendance'),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Charset': 'utf-8'
          },
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        myAttendance = data;
        loading = false;
      });
      print(myAttendance);
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  Future<void> _handleRefreshMyAttendance() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      code = preferences.getString('employeecode');
      type = preferences.getString('employeetype');
     
      DateTime date = DateTime.now();
      var yearFormat = new DateFormat('yyyy');
      DateTime year = DateFormat("yyyy-MM-dd HH:mm:ss").parse(date.toString());

      var finalyear = yearFormat.format(year);

      var body = {
        "emp_id": code,
        "employeeType": type,
        "month": finalmonth,
        "year": finalyear
      };
      print(body);

      var response = await http.post(
          Uri.parse(
              'https://www.mkttech.in/OMService/OMJsonService.svc/getAttendance'),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Charset': 'utf-8'
          },
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        myAttendance = data;
        loading = false;
      });
      print(myAttendance);
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
          appBar: AppBar(
            //centerTitle: true,
            backgroundColor: const Color(0xFF008577),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                final _state = _sideMenuKey.currentState;
                if (_state.isOpened)
                  _state.closeSideMenu();
                else
                  _state.openSideMenu();
              },
            ),
            actions: <Widget>[Count()],
            title: Text('Attendance'),
          ),
          body: loading == true
              ? Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                )
              : showMyAttendanceModule == true
                  ? Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 60,
                            width: double.infinity,
                            color: const Color(0xFF008577),
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 3,
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 20),
                                      child: TextField(
                                        readOnly: true,
                                        controller: yearController,
                                        style: TextStyle(color: Colors.white),
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            hintText: 'Select Year',
                                            hintStyle:
                                                TextStyle(color: Colors.white)),
                                        onTap: () {
                                          showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                title: Text("Select Year"),
                                                content: Container(
                                                  width: 300,
                                                  height: 300,
                                                  child: YearPicker(
                                                    firstDate: DateTime(
                                                        DateTime.now().year -
                                                            10,
                                                        1),
                                                    lastDate: DateTime(
                                                        DateTime.now().year +
                                                            30,
                                                        1),
                                                    initialDate: DateTime.now(),
                                                    selectedDate:
                                                        DateTime.now(),
                                                    onChanged:
                                                        (DateTime dateTime) {
                                                      print(dateTime);
                                                      setState(() {
                                                        fullDate = dateTime;
                                                      });
                                                      yearController.text =
                                                          dateTime
                                                              .toString()
                                                              .substring(0, 4);
                                                      Navigator.pop(context);
                                                    },
                                                  ),
                                                ),
                                              );
                                            },
                                          );
                                        },
                                      ),
                                    )),
                                // Expanded(flex: 2,child: Text(''),),
                                Expanded(
                                    flex: 3,
                                    child: Container(
                                        // alignment: Alignment.bottomCenter,
                                        height: 60,
                                        color: const Color(0xFF008577),
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 15, top: 6),
                                          child: TextField(
                                            readOnly: true,
                                            controller: monthController,
                                            style:
                                                TextStyle(color: Colors.white),
                                            decoration: InputDecoration(
                                                border: InputBorder.none,
                                                hintText: 'Select Month',
                                                hintStyle: TextStyle(
                                                    color: Colors.white)),
                                            onTap: () {
                                              showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return AlertDialog(
                                                    title: Text("Select Month"),
                                                    content: Container(
                                                        child: ListView.builder(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8),
                                                            itemCount:
                                                                month.length,
                                                            itemBuilder:
                                                                (BuildContext
                                                                        context,
                                                                    int index) {
                                                              return Container(
                                                                decoration: BoxDecoration(
                                                                    border: Border(
                                                                        bottom: BorderSide(
                                                                            width:
                                                                                1.0,
                                                                            color:
                                                                                Colors.grey[400]))),
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child: Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      top: 5,
                                                                      bottom:
                                                                          5),
                                                                  child:
                                                                      TextButton(
                                                                    child: Text(
                                                                      month[
                                                                          index],
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            20,
                                                                      ),
                                                                    ),
                                                                    onPressed:
                                                                        () {
                                                                      print(
                                                                          index);
                                                                      setState(
                                                                          () {
                                                                        monthNo =
                                                                            index;
                                                                      });
                                                                      monthController
                                                                              .text =
                                                                          month[
                                                                              index];
                                                                      Navigator.pop(
                                                                          context,
                                                                          false);
                                                                    },
                                                                  ),
                                                                ),
                                                              );
                                                            })),
                                                    actions: [
                                                      TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(
                                                                context, false);
                                                          },
                                                          child:
                                                              Text('Cancel')),
                                                    ],
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                        ))),
                                Expanded(
                                    flex: 2,
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 15),
                                      child: Container(
                                        alignment: Alignment.centerRight,
                                        child: IconButton(
                                            onPressed: () {
                                              print(fullDate);
                                              print(monthNo);
                                              getMyAttendance(
                                                  fullDate, monthNo);
                                            },
                                            icon: Icon(Icons.search,
                                                size: 35, color: Colors.white)),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            height: 60,
                            width: double.infinity,
                            color: Colors.grey[400],
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 2,
                                    child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Date(Day)',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 17),
                                        ))),
                                Expanded(
                                    flex: 3,
                                    child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Sign IN - Sign OUT',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 17),
                                        ))),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'WH',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 17),
                                        ))),
                              ],
                            ),
                          ),
                          Expanded(
                            child: LiquidPullToRefresh(
                              color: const Color(0xFF008577),
                              key: _refreshIndicatorKey,
                              onRefresh: _handleRefreshMyAttendance,
                              showChildOpacityTransition: true,
                              child: StreamBuilder<Object>(
                                  stream: null,
                                  builder: (context, snapshot) {
                                    return ListView.builder(
                                        padding: const EdgeInsets.all(8),
                                        itemCount: myAttendance.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Container(
                                            height: 50,
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        width: 1.0,
                                                        color:
                                                            Colors.grey[400]))),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                    flex: 2,
                                                    child: Container(
                                                        // alignment: Alignment.center,
                                                        child: Text(
                                                      myAttendance[index]
                                                          ['att_date'],
                                                    ))),
                                                Expanded(
                                                    flex: 3,
                                                    child: Container(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Text(
                                                          myAttendance[index][
                                                                  'in_timeAMPM'] +
                                                              ' - ' +
                                                              myAttendance[
                                                                      index][
                                                                  'out_timeAMPM'],
                                                          style: TextStyle(
                                                            color: Colors
                                                                .blue[200],
                                                          ),
                                                        ))),
                                                Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Text(
                                                          myAttendance[index]
                                                                      ['WH'] ==
                                                                  ''
                                                              ? '-'
                                                              : myAttendance[
                                                                  index]['WH'],
                                                        ))),
                                              ],
                                            ),
                                          );
                                        });
                                  }),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 60,
                            width: double.infinity,
                            color: const Color(0xFF008577),
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 2,
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 17),
                                      child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Icon(
                                            Icons.calendar_today,
                                            color: Colors.white,
                                          )),
                                    )),
                                // Expanded(flex: 2,child: Text(''),),
                                Expanded(
                                    flex: 4,
                                    child: Container(
                                      alignment: Alignment.bottomCenter,
                                      height: 60,
                                      color: const Color(0xFF008577),
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(left: 40),
                                        child: TextField(
                                          readOnly: true,
                                          controller: dateController,
                                          style: TextStyle(color: Colors.white),
                                          decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: DateFormat('dd/MM/yyyy')
                                                  .format(DateTime.now())
                                                  .toString(),
                                              hintStyle: TextStyle(
                                                  color: Colors.white)),
                                          onTap: () async {
                                            var date = await showDatePicker(
                                                context: context,
                                                initialDate: DateTime.now(),
                                                firstDate: DateTime(1900),
                                                lastDate: DateTime(2050));
                                            dateController.text = date
                                                .toString()
                                                .substring(0, 10);
                                            // print(date.toString().substring(0, 10));
                                          },
                                        ),
                                      ),
                                    )),
                                Expanded(
                                    flex: 2,
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 15),
                                      child: Container(
                                          alignment: Alignment.centerRight,
                                          child: IconButton(
                                              onPressed: () {
                                                print('seacrh');
                                                DateTime date1 =
                                                    DateFormat("yyyy-MM-dd")
                                                        .parse(dateController
                                                            .text
                                                            .toString());
                                                var df = new DateFormat(
                                                    'yyyy-MM-dd');
                                                var finaldate =
                                                    df.format(date1);
                                                getAttendance(finaldate);
                                              },
                                              icon: Icon(Icons.search,
                                                  size: 35,
                                                  color: Colors.white))
                                          // Icon(Icons.search,
                                          //     size: 35, color: Colors.white)
                                          ),
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            height: 60,
                            width: double.infinity,
                            color: Colors.grey[400],
                            child: Row(
                              children: [
                                Expanded(
                                    flex: 2,
                                    child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Name',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 17),
                                        ))),
                                Expanded(
                                    flex: 3,
                                    child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Sign IN - Sign OUT',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 17),
                                        ))),
                                Expanded(
                                    flex: 1,
                                    child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'WH',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 17),
                                        ))),
                              ],
                            ),
                          ),
                          Expanded(
                            child: LiquidPullToRefresh(
                              color: const Color(0xFF008577),
                              key: _refreshIndicatorKey,
                              onRefresh: _handleRefreshTeamAttendance,
                              showChildOpacityTransition: true,
                              child: StreamBuilder<Object>(
                                  stream: null,
                                  builder: (context, snapshot) {
                                    return ListView.builder(
                                        padding: const EdgeInsets.all(8),
                                        itemCount: teamAttendance.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Container(
                                            height: 50,
                                            width: double.infinity,
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        width: 1.0,
                                                        color:
                                                            Colors.grey[400]))),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                    flex: 2,
                                                    child: Container(
                                                        // alignment: Alignment.center,
                                                        child: Text(
                                                      teamAttendance[index]
                                                              ['f_name'] +
                                                          ' ' +
                                                          teamAttendance[index]
                                                              ['l_name'],
                                                    ))),
                                                Expanded(
                                                    flex: 3,
                                                    child: Container(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Text(
                                                          teamAttendance[index][
                                                                  'in_timeAMPM'] +
                                                              ' - ' +
                                                              teamAttendance[
                                                                      index][
                                                                  'out_timeAMPM'],
                                                          style: TextStyle(
                                                            color: Colors
                                                                .blue[200],
                                                          ),
                                                        ))),
                                                Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Text(
                                                          teamAttendance[index]
                                                                      ['WH'] ==
                                                                  ''
                                                              ? '-'
                                                              : teamAttendance[
                                                                  index]['WH'],
                                                        ))),
                                              ],
                                            ),
                                          );
                                        });
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
        ),
      ),
    );
  }
}
