import 'package:flutter/material.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/office_manager/canteen.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/holiday.dart';
import 'package:vfas/office_manager/myAttendance.dart';
import 'package:vfas/office_manager/myProfile.dart';
import 'package:vfas/office_manager/omNotification.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:ui';
import 'dart:async';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vfas/sidemenu.dart';

void main() {
  runApp(MaterialApp(home: Contact()));
}

class Contact extends StatefulWidget {
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _curvedAnimation;

  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  var visibleApprove = false;
  var data;
  var searchData;
  final nameController = TextEditingController();

  bool flipvalue = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPhone();
    _animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000));
    _curvedAnimation =
        CurvedAnimation(parent: _animationController, curve: Curves.easeInOut);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void _flip(bool reverse) {
    if (_animationController.isAnimating) return;
    if (reverse) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
  }

  getPhone() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    data = preferences.getString('phonedata');
    setState(() {
      data = json.decode(preferences.getString('phonedata'));
    });
    print(data);
  }

  onItemChanged(value) {
    print(value);
    searchData = data
        .where((i) => i['emp_fname']
            .toString()
            .toLowerCase()
            .contains(value.toString().toLowerCase()))
        .toList();
    print(searchData);
    setState(() {
      data = searchData;
    });
  }

  mail(mailid) async {
    var url = 'mailto:' + mailid;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  call(no) async {
    var url = 'tel:' + no;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
          appBar: AppBar(
            // centerTitle: true,
            backgroundColor: const Color(0xFF008577),

            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                final _state = _sideMenuKey.currentState;
                if (_state.isOpened)
                  _state.closeSideMenu();
                else
                  _state.openSideMenu();
              },
            ),
            actions: <Widget>[Count()],
            title: Text('Contact List'),
          ),
          body: Center(
            child: Column(
              children: <Widget>[
                Container(
                  height: 50,
                  color: const Color(0xFF008577),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: TextField(
                              controller: nameController,
                              style: TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                  hintText: 'Search by name',
                                  hintStyle: TextStyle(color: Colors.white)),
                              onChanged: (value) {
                                onItemChanged(value);
                              }),
                        ),
                      ),
                      Expanded(
                          flex: 1,
                          child: IconButton(
                              onPressed: () {
                                onItemChanged(nameController.text);
                              },
                              icon: Icon(Icons.search,
                                  size: 30, color: Colors.white)))
                    ],
                  ),
                ),
                Container(
                  height: 50,
                  color: Colors.grey[600],
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Text(
                            'Name',
                            style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 0),
                          child: Text(
                            'Mobile',
                            style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                      padding: const EdgeInsets.all(8),
                      itemCount: data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                          padding:
                              const EdgeInsets.only(left: 5, right: 5, top: 10),
                          child: GestureDetector(
                            onTap: () {
                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Column(
                                      children: [
                                        Text(''),
                                        Text(''),
                                        Container(
                                            // alignment: Alignment.centerLeft,
                                            child: TextButton.icon(
                                                onPressed: () {},
                                                icon: Icon(Icons.person),
                                                label: Text(
                                                  data[index]['emp_fname'] +
                                                      ' ' +
                                                      data[index]['emp_lname'],
                                                  style: TextStyle(
                                                      fontSize: 22,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ))),
                                        Text(''),
                                        Container(
                                            // alignment: Alignment.centerLeft,
                                            child: Text(
                                                'HOD :- ' +
                                                    ' ' +
                                                    data[index]['emp_mgr_name'],
                                                style: TextStyle(
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.grey[600]))),
                                        Text(''),
                                        Container(
                                            // alignment: Alignment.centerLeft,
                                            child: Text(
                                                'MarketPlace Employee' +
                                                    ' (' +
                                                    data[index]['emp_id'] +
                                                    ')',
                                                style: TextStyle(
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.grey[600]))),
                                        Text(''),
                                        Container(
                                            // alignment: Alignment.centerLeft,
                                            child: TextButton.icon(
                                                onPressed: () {
                                                  call(data[index]['mobile']);
                                                },
                                                icon: Icon(Icons.phone),
                                                label:
                                                    Text(data[index]['mobile'],
                                                        style: TextStyle(
                                                          fontSize: 17,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        )))),
                                        Text(''),
                                        Container(
                                            // alignment: Alignment.centerLeft,
                                            child: TextButton.icon(
                                                onPressed: () {
                                                  mail(data[index]['email']);
                                                },
                                                icon: Icon(Icons.mail),
                                                label:
                                                    Text(data[index]['email'],
                                                        style: TextStyle(
                                                          fontSize: 17,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ))))
                                      ],
                                    );
                                  });
                            },
                            child: Container(
                              //alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 5.0, // soften the shadow
                                    spreadRadius: 0.2, //extend the shadow
                                    offset: Offset(
                                      0.5, // Move to right 10  horizontally
                                      0.5, // Move to bottom 10 Vertically
                                    ),
                                  )
                                ],
                              ),
                              height: 70,
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 5,
                                    child: Container(
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 12, left: 10),
                                        child: Column(
                                          children: [
                                            Text(''),
                                            Container(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  data[index]['emp_fname'] +
                                                      ' ' +
                                                      data[index]['emp_lname'],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                )),
                                            Text(''),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10),
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(data[index]['mobile'],
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold))),
                                      ))
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Widget _buildCard(List data, GestureTapCallback onTap) {
  //   return GestureDetector(
  //     onTap: onTap,
  //     child: Container(
  //       height: 100,
  //       width: double.infinity,
  //       color: Colors.red,
  //       child: Text(title),
  //     ),
  //   );
  // }
}
