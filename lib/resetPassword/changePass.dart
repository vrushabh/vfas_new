import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:http/http.dart' as http;
import 'package:vfas/notifyCount.dart';
import 'dart:convert';
import 'dart:ui';
import 'dart:async';
import 'dart:io';

import 'package:vfas/sidemenu.dart';

void main() {
  runApp(MaterialApp(home: ChangePassword()));
}

class ChangePassword extends StatefulWidget {
  ChangePassword({this.token});
  var token;
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  var token;
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  TextEditingController password = new TextEditingController();
  TextEditingController confirmpassword = new TextEditingController();
  var _passwordVisible = true;
  var _passwordVisible2 = true;
  var key1;
  var key2;
  var loading = 'false';
  var showAppbar = 'false';
  var _currentRoute;
  String Username = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setUsername();

    setState(() {
      token = widget.token;
    });
    print(token);
  }

  setUsername() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    _currentRoute = preferences.getString('otpStatus');

    setState(() {
      Username = preferences.getString('username');

      showAppbar =
          _currentRoute == null || _currentRoute == '0' ? 'false' : 'true';
    });
  }

  changePassword() async {
    setState(() {
      loading = 'true';
    });
    var body = {
      "cpwd": confirmpassword.text,
      "encType": "IOS",
      "login": Username,
      "pwd": password.text,
      "token": token
    };
    print(body);
    var response = await http.post(
        Uri.parse(
            'https://api.bseindia.com/SSOServices/SSOService.svc/UserPasswordValidate'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Charset': 'utf-8'
        },
        body: jsonEncode(body));

    var data = json.decode(response.body);
    print(data);
    setState(() {
      loading = 'false';
    });

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        //  title: Text('Do you want to exit App.'),
        content: Text(data['data']),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text('OK')),
        ],
      ),
    );
    //{data: Change Password Failed, errorList: CHANGE_PWD_FAIL_LENGTH_RULE_MIN}
    // setState(() {
    //   loading = 'false';
    // });
  }

  backPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you want to exit App.'),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text('No')),
          TextButton(
              onPressed: () {
                exit(0);
                //Navigator.pop(context, true);
              },
              child: Text('Yes')),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
       background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
            appBar: showAppbar == 'true'
                ? AppBar(
                    backgroundColor: const Color(0xFF008577),
                    leading: IconButton(
                      icon: Icon(Icons.menu),
                      onPressed: () {
                        final _state = _sideMenuKey.currentState;
                        if (_state.isOpened)
                          _state.closeSideMenu();
                        else
                          _state.openSideMenu();
                      },
                    ),
                    actions: <Widget>[Count()],
                    title: Text('BSE SSO'),
                  )
                : AppBar(
                    backgroundColor: const Color(0xFF008577),
                    automaticallyImplyLeading: false,
                    actions: <Widget>[
                      IconButton(
                          onPressed: () {
                            backPressed();
                          },
                          icon: Icon(
                            Icons.power_settings_new,
                            size: 35,
                            color: Colors.white,
                          ))
                    ],
                    title: Text('BSE SSO'),
                  ),
            body: Container(
              // alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/back.jpg'), fit: BoxFit.cover)),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: TextButton.icon(
                              onPressed: () {},
                              icon: Icon(
                                Icons.person,
                                size: 30,
                                color: Colors.grey[900],
                              ),
                              label: Text(
                                Username,
                                style: TextStyle(
                                    color: Colors.grey[900], fontSize: 17),
                              )),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 80,
                  ),
                  loading == 'true'
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(0, 120, 0, 0),
                          child: Container(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(
                              color: Colors.black,
                            ),
                          ),
                        )
                      : Container(
                          //  color: Colors.red,

                          child: Column(
                            children: [
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 10.0),
                                  child: TextField(
                                    controller: password,
                                    obscureText: _passwordVisible,
                                    decoration: InputDecoration(
                                      fillColor: Colors.white.withOpacity(0.6),
                                      filled: true,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      labelText: 'Enter your new Password',
                                      labelStyle: TextStyle(
                                          color: Colors.black, fontSize: 17.0),
                                      hintText: 'Password',
                                      hintStyle: TextStyle(fontSize: 15.0),
                                      suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _passwordVisible =
                                                  !_passwordVisible;
                                            });
                                          },
                                          icon: Padding(
                                            padding: const EdgeInsets.all(0.0),
                                            child: Icon(
                                              _passwordVisible
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 10.0),
                                  child: TextField(
                                    controller: confirmpassword,
                                    obscureText: _passwordVisible2,
                                    decoration: InputDecoration(
                                      fillColor: Colors.white.withOpacity(0.6),
                                      filled: true,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      labelText: 'Enter confirm Password',
                                      labelStyle: TextStyle(
                                          color: Colors.black, fontSize: 17.0),
                                      hintText: 'Password',
                                      hintStyle: TextStyle(fontSize: 15.0),
                                      suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _passwordVisible2 =
                                                  !_passwordVisible2;
                                            });
                                          },
                                          icon: Padding(
                                            padding: const EdgeInsets.all(0.0),
                                            child: Icon(
                                              _passwordVisible2
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                  onPressed: () {
                                    changePassword();
                                  },
                                  child: Text('Submit')),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(20, 30, 20, 0),
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  color: Colors.white.withOpacity(0.8),
                                  height: 280,
                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 10, 0, 0),
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            'NOTE :-',
                                            style: TextStyle(color: Colors.red),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '1. The Password must be between 8 to 16 charachter long.')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '2. Atleast 1 numeric charachter required.')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '3. Atleast 1 uppercase charachter required.')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '4. Atleast 1 non alphanumeric charachter required.')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '5. Atleast 1 alphabetic charachter required.')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '6. Password cannot contain Login.')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '7. Password cannot equal to the users last name.')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '8. New Password and Confirmed Password are not equal.')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '9. Cannot reuse Previous Password.')),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                ],
              ),
            )),
      ),
    );
  }
}
