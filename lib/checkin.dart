import 'dart:convert';
import 'dart:io' show Platform;

import 'package:device_info/device_info.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:location/location.dart' as loc;
import 'package:shared_preferences/shared_preferences.dart';

import 'LoadingIndicator.dart';

void main() => runApp(MaterialApp(
      home: CheckIn(),
    ));

class CheckIn extends StatefulWidget {
  CheckIn({this.checkIn});

  String checkIn = '';

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<CheckIn> {
  // _MyAppState(String checkIn){
  //   this.checkIn = checkIn;
  // }
  //
  // String checkIn = '';

  LatLng _initialcameraposition = LatLng(20.5937, 78.9629);
  GoogleMapController _controller;
  GoogleMapController _controllerOff;
  String _currentAddress;
  String _officeAddress = "";
  loc.Location _location = loc.Location();
  var loadingdata = 'true';

  double latitude;
  double longitude;

  double officeLAt;
  double officeLong;

  bool loading = true;
  int selectedTab = 0;
  var address = '';
  var distanceLabel = '';
  var distanceKM = '';
  var timeLabel1 = '';
  var timeLabel2 = '';
  var jsonData;

  String deviceID;

  @override
  void initState() {
    super.initState();
    getDeviceDetails();
    // this.getAttendance();
  }

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;
    _location.onLocationChanged.listen((l) {
      latitude = l.latitude;
      longitude = l.longitude;
      if (selectedTab == 0) {
        _getAddressFromLatLng(l);
        mapMoveTo(latitude, longitude);
        getAttendance('0');
      }
    });
  }

  _getAddressFromLatLng(l) async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(l.latitude, l.longitude);
      Placemark place = placemarks[0];
      setState(() {
        _currentAddress =
            "${place.locality},${place.street}, ${place.postalCode}, ${place.country}";
        //_textController.text = _currentAddress;
        setState(() {
          address = _currentAddress;
        });
      });
    } catch (e) {
      print(e);
    }
  }

  final TextEditingController _textController = new TextEditingController();
  final TextEditingController _textDistanceController =
      new TextEditingController();
  final TextEditingController _textTimeController = new TextEditingController();
  final TextEditingController _lblDistanceController =
      new TextEditingController();
  final TextEditingController _lblTimeController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // _textController.text = 'Hello Flutter'; //Set value
    // encrypt();
    var checkInOutBtnText = widget.checkIn.isEmpty ? 'Check In' : 'Check Out';
    return new MaterialApp(
      home: new Scaffold(
        body: new Container(
          margin: const EdgeInsets.only(top: 20.0),
          //color: Colors.yellowAccent,
          child: new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                new Expanded(
                  child: new Container(
                    child: Column(
                      // Vertically center the widget inside the column
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(13, 8, 13, 0),
                            child: Text(
                              address,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                          ),
                          // child: TextField(
                          //   controller: _textController,
                          //   textAlign: TextAlign.center,
                          //   keyboardType: TextInputType.multiline,
                          //   maxLines: null,
                          //   enabled: false,
                          //   style: TextStyle(
                          //       fontWeight: FontWeight.bold,
                          //       fontSize: 15,
                          //       color: Colors.black.withOpacity(0.8)),
                          // ),
                        )
                      ],
                    ),
                    color: Colors.white,
                  ),
                  flex: 2,
                ),
                new Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: new Container(
                      child: DefaultTabController(
                        length: 2,
                        child: TabBar(
                          onTap: (index) {
                            selectedTab = index;
                            if (index == 1) {
                              mapMoveTo(officeLAt, officeLong);
                              setState(() {
                                address = _officeAddress;
                              });
                            } else {
                              mapMoveTo(latitude, longitude);
                            }
                          },
                          tabs: [
                            Tab(
                                text: 'CURRENT LOCATION',
                                icon: Icon(Icons.location_history)),
                            Tab(
                                text: 'OFFICE LOCATION',
                                icon: Icon(Icons.location_city)),
                          ],
                        ),
                      ),
                      color: Colors.black,
                    ),
                  ),
                  flex: 2,
                ),
                new Expanded(
                  child: new Container(
                    child: GoogleMap(
                      initialCameraPosition:
                          CameraPosition(target: _initialcameraposition),
                      mapType: MapType.normal,
                      onMapCreated: _onMapCreated,
                      myLocationEnabled: true,
                    ),
                    color: Colors.blue,
                  ),
                  flex: 6,
                ),
                new Expanded(
                  child: loadingdata == 'true'
                      ? Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Container(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(),
                          ),
                      )
                      : Container(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              new Expanded(
                                child: new Container(
                                  child: Column(
                                    // Vertically center the widget inside the column
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                          alignment: Alignment.center,
                                          child: Text(distanceLabel,
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.redAccent
                                                      .withOpacity(0.8))))
                                      //   new TextField(
                                      //     controller: _lblDistanceController,
                                      //     enabled: false,
                                      //     textAlign: TextAlign.center,
                                      //     style: TextStyle(
                                      //         fontWeight: FontWeight.normal,
                                      //         fontSize: 18,
                                      //         color:
                                      //             Colors.redAccent.withOpacity(0.8)),
                                      //   ),
                                      // )
                                    ],
                                  ),
                                ),
                              ),
                              new Expanded(
                                  child: new Container(
                                      alignment: Alignment.center,
                                      child: Text(distanceKM,
                                          style: TextStyle(
                                              fontSize: 25,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.redAccent
                                                  .withOpacity(0.8))))
                                  //   new TextField(
                                  //  controller: _textDistanceController,
                                  //     enabled: false,
                                  //     textAlign: TextAlign.center,
                                  //     style: TextStyle(
                                  //         fontWeight: FontWeight.bold,
                                  //         fontSize: 25,
                                  //         color: Colors.redAccent.withOpacity(0.8)),
                                  //   ),
                                  // ),
                                  ),
                              new Expanded(
                                child: new Container(
                                    alignment: Alignment.center,
                                    child: Text(timeLabel1,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.redAccent
                                                .withOpacity(0.8)))
                                    // new TextField(
                                    // controller: _lblTimeController,
                                    //   textAlign: TextAlign.center,
                                    //   enabled: false,
                                    //   style: TextStyle(
                                    //       fontWeight: FontWeight.bold,
                                    //       fontSize: 18,
                                    //       color: Colors.redAccent.withOpacity(0.8)),
                                    // ),
                                    ),
                              ),
                              new Expanded(
                                child: new Container(
                                  alignment: Alignment.center,
                                  child: Text(timeLabel2,
                                      style: TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.redAccent
                                              .withOpacity(0.8))),

                                  // new TextField(
                                  //   controller: _textTimeController,
                                  //   textAlign: TextAlign.center,
                                  //   enabled: false,
                                  //   readOnly: true,
                                  //   style: TextStyle(
                                  //       fontWeight: FontWeight.bold,
                                  //       fontSize: 25,
                                  //       color: Colors.redAccent.withOpacity(0.8)),
                                  // ),
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          color: Colors.white,
                        ),
                  flex: 4,
                ),
                new Expanded(
                  child: new Container(
                    child: new Row(
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 3,
                          child: Container(
                            alignment: Alignment.center,
                            child: RaisedButton(
                              child: Text(
                                checkInOutBtnText,
                                style: TextStyle(fontSize: 20.0),
                              ),
                              color: Colors.green,
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () {
                                DialogBuilder(context).showLoadingIndicator('');
                                getAttendance('1');
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                            alignment: Alignment.center,
                            child: RaisedButton(
                              child: Text(
                                'Cancel',
                                style: TextStyle(fontSize: 20.0),
                              ),
                              color: Colors.redAccent,
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        )
                        // new Expanded(
                        //   child: new Container(
                        //     child: Column(
                        //       // Vertically center the widget inside the column
                        //       children: [
                        //         Container(
                        //           margin: const EdgeInsets.only(right: 5.0),
                        //           child: RaisedButton(
                        //             child: Text(
                        //               checkInOutBtnText,
                        //               style: TextStyle(fontSize: 20.0),
                        //             ),
                        //             color: Colors.green,
                        //             textColor: Colors.white,
                        //             shape: RoundedRectangleBorder(
                        //                 borderRadius:
                        //                     BorderRadius.circular(10)),
                        //             onPressed: () {
                        //               DialogBuilder(context)
                        //                   .showLoadingIndicator('');
                        //               getAttendance('1');
                        //             },
                        //           ),
                        //         )
                        //       ],
                        //     ),
                        //   ),
                        //   flex: 1,
                        // ),
                        // new Expanded(
                        //   child: new Container(
                        //     margin: const EdgeInsets.only(left: 5.0),
                        //     child: RaisedButton(
                        //       child: Text(
                        //         'Cancel',
                        //         style: TextStyle(fontSize: 20.0),
                        //       ),
                        //       color: Colors.redAccent,
                        //       textColor: Colors.white,
                        //       shape: RoundedRectangleBorder(
                        //           borderRadius: BorderRadius.circular(10)),
                        //       onPressed: () {
                        //         Navigator.of(context).pop();
                        //       },
                        //     ),
                        //   ),
                        //   flex: 1,
                        // ),
                      ],
                    ),
                    color: Colors.white,
                  ),
                  flex: 1,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void mapMoveTo(double latitude, double longitude) {
    _controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(latitude, longitude), zoom: 15),
        // new HomePage()._getAddressFromLatLng(l);
      ),
    );
  }

  getAttendance(String punchFlag) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var Email = preferences.getString('email');
    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');

    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

    final encryptedDate = encrypter.encrypt(formattedDate, iv: iv);
    print(encryptedDate.base64.toString());

    var date = getCurrentDate('dd MMM yy');
    var fulldate = date.trim();
    DateTime date1 = DateFormat("dd MMM yy").parse(fulldate);
    final df = new DateFormat('yyyyMMdd');
    var finalDate = df.format(date1);
    print(finalDate);

    String body = '{"EmailID":' +
        '"' +
        Email +
        '"' +
        ',"IMEMINo":' +
        '"' +
        deviceID +
        '"' +
        ',"AttendType":' +
        '"' +
        (widget.checkIn.isEmpty ? '1' : '2') +
        '"' +
        ',"Lat":' +
        '"' +
        latitude.toString() +
        '"' +
        ',"Long":' +
        '"' +
        longitude.toString() +
        '"' +
        ',"punchFlag":' +
        '"' +
        punchFlag +
        '"' +
        ',"address":' +
        '"' +
        _currentAddress +
        '"' +
        ',"date":' +
        '"' +
        getCurrentDate('yyyy-MM-dd hh:mm:ss') +
        '" ' +
        '}';

    // String body1 = '{"EmailID":"pankaj.patil@mkttech.in","IMEMINo":"0b366f8d9bc62e66","AttendType":"2","Lat":"19.56693","Long":"72.7987505","punchFlag":"0","address":"Mande Bus Stop, Tembhikhodave Rd, Mande, Maharashtra 401102, India","date":"2021-05-26 11:53:56"}';

    print(body);
    // print(body1);

    final encrypted = encrypter.encrypt(body, iv: iv);
    //print(encrypted.base64.toString());
    var encryptBody = {
      "data": "",
      "type": "IOS",
      "encData": encrypted.base64.toString()
    };
    // print(encryptBody);
    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/vfs/VMSService.svc/emppunchinoutNew_Enc'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          "Accept": "application/json",
          'Charset': 'utf-8',
          'sdt': encryptedDate.base64.toString()
        },
        body: jsonEncode(encryptBody));

    Map<String, dynamic> map = json.decode(response.body);

    var dect = encrypter.decrypt64(map['result'], iv: iv);
    // setState(() {
    loading = false;
    jsonData = json.decode(dect);
    _officeAddress = jsonData['officeAdd'];

    String strofficeLAt = jsonData['officeLAt'];
    String strofficeLong = jsonData['officeLong'];

    // officeLAt = jsonData['officeLAt'];
    // officeLong = jsonData['officeLong'];

    if (strofficeLAt.isNotEmpty) officeLAt = double.parse(strofficeLAt);

    if (strofficeLong.isNotEmpty) officeLong = double.parse(strofficeLong);

    String MSg = jsonData['MSg'];
    String dttm = jsonData['dttm'];

    String distance = jsonData['distance'];
    String dispMsg = jsonData['dispMsg'];
    String distMsg = jsonData['distMsg'];

    if (MSg.contains('Your location is'))
      MSg = MSg.split('Your location is')[1];

    _lblDistanceController.text = 'Your location is';
    setState(() {
      distanceLabel = 'Your location is';
      distanceKM = MSg;
      timeLabel1 = 'Punch time';
      timeLabel2 = dttm;
      loadingdata = 'false';
    });
    _lblTimeController.text = 'Punch time';

    _textDistanceController.text = MSg;
    _textTimeController.text = dttm;

    var txtcolor = distance == '0' ? Colors.green : Colors.redAccent;

    // txt_msg.setTextColor(ContextCompat.getColor(EmpLocation.this, color));
    // lbl_msg.setTextColor(ContextCompat.getColor(EmpLocation.this, color));
    // txt_punch_time.setTextColor(ContextCompat.getColor(EmpLocation.this, color));
    // lbl_punch_time.setTextColor(ContextCompat.getColor(EmpLocation.this, color));

    if (distMsg.isEmpty) {
      setState(() {
        distanceKM = MSg;
        timeLabel2 = dttm;
      });
      _textDistanceController.text = MSg;
      _textTimeController.text = dttm;

      if (MSg.contains('GPS data not')) {
        setState(() {
          distanceLabel = '';
          timeLabel2 = '';
        });
        _lblDistanceController.text = '';
        _textTimeController.text = '';
        // lbl_punch_time.setVisibility(View.GONE);
        // txt_punch_time.setVisibility(View.GONE);
      }
      /*else {
          lbl_msg.setVisibility(View.VISIBLE);
          lbl_punch_time.setVisibility(View.VISIBLE);
          txt_punch_time.setVisibility(View.VISIBLE);
        }*/
    } else {
      setState(() {
        distanceKM = MSg;
        timeLabel2 = dttm;
      });
      _textDistanceController.text = MSg;
      _textTimeController.text = dttm;
    }
    // DialogBuilder(context).hideOpenDialog();

    if (punchFlag == '1') {
      DialogBuilder(context).hideOpenDialog();

      if (dispMsg.isNotEmpty) {
        DialogBuilder(context)
            ._showMyDialog('VFAS', MSg, dttm, txtcolor, 'Cancel');
      } else {
        DialogBuilder(context)._showMyDialog('VFAS', MSg, dttm, txtcolor, 'OK');
      }
    }
    // });
    print("data---" + dect);
  }

  getCurrentDate(String format) {
    return DateFormat(format).format(DateTime.now());
  }

  getDeviceDetails() async {
    String deviceName;
    String deviceVersion;
    String identifier;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.model;
        deviceVersion = build.version.toString();
        identifier = build.androidId; //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceName = data.name;
        deviceVersion = data.systemVersion;
        identifier = data.identifierForVendor; //UUID for iOS
      }
    } on PlatformException {
      print('Failed to get platform version');
    }
    deviceID = identifier;
  }
}

class DialogBuilder {
  DialogBuilder(this.context);

  final BuildContext context;

  void showLoadingIndicator([String text]) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              backgroundColor: Colors.black87,
              content: LoadingIndicator(text: text),
            ));
      },
    );
  }

  void hideOpenDialog() {
    // Navigator.of(context).pop();
    Navigator.of(context, rootNavigator: true).pop();
  }

  Future<void> _showMyDialog(String title, String location, String punchTime,
      MaterialAccentColor txtcolor, String buttonText) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(location, style: TextStyle(color: txtcolor)),
                Text(punchTime, style: TextStyle(color: txtcolor)),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(buttonText),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
                // hideOpenDialog();
              },
            ),
          ],
        );
      },
    );
  }
}
