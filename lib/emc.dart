import 'package:flutter/material.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:ui';
import 'dart:async';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/sidemenu.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

void main() {
  runApp(MaterialApp(home: Emc()));
}

class Emc extends StatefulWidget {
  @override
  _EmcState createState() => _EmcState();
}

class _EmcState extends State<Emc> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey1 =
      GlobalKey<LiquidPullToRefreshState>();

  static int refreshNum = 10;
  Stream<int> counterStream =
      Stream<int>.periodic(Duration(seconds: 3), (x) => refreshNum);
  var pendingData;
  var approveData;
  var searchData;
  var loadingData = true;
  var loadingApproveData = true;
  var noDataPending = 'true';
  var noDataApproved = 'true';
  var _chosenValue = 'Approved';
  var flag = '1';
  final nameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    getPendingTickets();
    getApprovedTickets(flag);
  }

  getPendingTickets() async {
    setState(() {
      loadingData = true;
    });

    try {
      var body = {
        "approver": "test.user1",
        "flag": "0",
        "hashKey": "#iwBHsWgadBQEFubBilw9SjfbWgX1bHpDrcQI2kvBFtM\u003d",
        "notetype": "1",
        "taskid": ""
      };

      print(body);

      var response = await http.post(
          Uri.parse(
              'https://dfws.bseindia.com/emcapproval/NoteAppService.svc/get_approval_test'),
          headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        pendingData = data;
        loadingData = false;
      });
      pendingData.length > 0
          ? setState(() {
              noDataPending = 'false';
            })
          : setState(() {
              noDataPending = 'true';
            });
      print(data);
    } catch (e) {
      setState(() {
        noDataPending = 'true';
        loadingData = false;
      });
    }
  }

  getApprovedTickets(flag) async {
    setState(() {
      loadingApproveData = true;
    });

    try {
      var body = {
        "approver": "kersi.tavadia",
        "flag": flag,
        "hashKey": "#iwBHsWgadBQEFubBilw9SjfbWgX1bHpDrcQI2kvBFtM\u003d",
        "notetype": "1",
        "taskid": ""
      };

      print(body);

      var response = await http.post(
          Uri.parse(
              'https://dfws.bseindia.com/emcapproval/NoteAppService.svc/get_approval'),
          headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        approveData = data;
        loadingApproveData = false;
      });
      approveData.length > 0
          ? setState(() {
              noDataApproved = 'false';
            })
          : setState(() {
              noDataApproved = 'true';
            });
      print(approveData);
    } catch (e) {
      setState(() {
        noDataApproved = 'true';
        loadingApproveData = false;
      });
    }
  }

  Future<void> _handleRefreshApprove() async {
    try {
      var body = {
        "approver": "kersi.tavadia",
        "flag": flag,
        "hashKey": "#iwBHsWgadBQEFubBilw9SjfbWgX1bHpDrcQI2kvBFtM\u003d",
        "notetype": "1",
        "taskid": ""
      };

      print(body);

      var response = await http.post(
          Uri.parse(
              'https://dfws.bseindia.com/emcapproval/NoteAppService.svc/get_approval'),
          headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        approveData = data;
      });
      approveData.length > 0
          ? setState(() {
              noDataApproved = 'false';
            })
          : setState(() {
              noDataApproved = 'true';
            });
    } catch (e) {
      setState(() {
        noDataApproved = 'true';
      });
    }

    //--------------------------
  }

  Future<void> _handleRefreshPending() async {
    try {
      var body = {
        "approver": "bala.inx",
        "flag": "0",
        "hashKey": "#iwBHsWgadBQEFubBilw9SjfbWgX1bHpDrcQI2kvBFtM\u003d",
        "notetype": "1",
        "taskid": ""
      };

      print(body);

      var response = await http.post(
          Uri.parse(
              'https://dfws.bseindia.com/emcapproval/NoteAppService.svc/get_approval'),
          headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        pendingData = data;
      });
      pendingData.length > 0
          ? setState(() {
              noDataPending = 'false';
            })
          : setState(() {
              noDataPending = 'true';
            });
      print(data);
    } catch (e) {
      setState(() {
        noDataPending = 'true';
      });
    }
  }

  onItemChanged(value) {
    print(value);
    searchData = approveData
        .where((i) => i['task_id'].toString().contains(value.toString()))
        .toList();
    print(searchData);
    setState(() {
      approveData = searchData;
    });
  }

  subjectShow(value) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text("Subject"),
        content: Text(value),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.pop(context, false);
            },
            child: Text("OK"),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
          length: 2,
          child: SideMenu(
            key: _endSideMenuKey,
            inverse: false, // end side menu
            background: const Color(0xFF008577),
            type: SideMenuType.slideNRotate,
            menu: Sidemenu(),

            child: SideMenu(
              key: _sideMenuKey,
              background: const Color(0xFF008577),
              menu: Sidemenu(),
              type: SideMenuType.shrinkNSlide,
              child: Scaffold(
                  appBar: AppBar(
                    backgroundColor: const Color(0xFF008577),
                    bottom: TabBar(
                      tabs: [
                        Tab(text: 'PENDING TICKETS'),
                        Tab(text: 'APPROVED TICKETS'),
                      ],
                      indicatorColor: Colors.orange,
                      labelColor: Colors.black,
                    ),
                    //centerTitle: true,
                    leading: IconButton(
                      icon: Icon(Icons.menu),
                      onPressed: () {
                        final _state = _sideMenuKey.currentState;
                        if (_state.isOpened)
                          _state.closeSideMenu();
                        else
                          _state.openSideMenu();
                      },
                    ),

                    title: Text('EMC Tickets'),
                  ),
                  body: TabBarView(children: [
                    // Container(
                    //     child: loadingData == true
                    //         ? Container(
                    //             alignment: Alignment.center,
                    //             child: CircularProgressIndicator(),
                    //           )
                    //         : noDataPending == 'true'
                    //             ? Center(
                    //                 child: Container(
                    //                   child: Text(
                    //                     'No Data Found',
                    //                     style: TextStyle(
                    //                         fontSize: 23,
                    //                         fontWeight: FontWeight.bold,
                    //                         color: Colors.black),
                    //                   ),
                    //                 ),
                    //               )
                    //             : Expanded(
                    //                 child: Column(
                    //                   children: [
                    //                     Expanded(
                    //                         child: LiquidPullToRefresh(
                    //                       color: const Color(0xFF008577),
                    //                       key: _refreshIndicatorKey,
                    //                       onRefresh: _handleRefreshPending,
                    //                       showChildOpacityTransition: true,
                    //                       child: StreamBuilder<Object>(
                    //                           stream: null,
                    //                           builder: (context, snapshot) {
                    //                             return ListView.builder(
                    //                                 itemCount:
                    //                                     pendingData.length,
                    //                                 itemBuilder:
                    //                                     (BuildContext context,
                    //                                         int index) {
                    //                                   return Padding(
                    //                                     padding:
                    //                                         const EdgeInsets
                    //                                             .all(8.0),
                    //                                     child: Container(
                    //                                       decoration:
                    //                                           BoxDecoration(
                    //                                         borderRadius:
                    //                                             BorderRadius
                    //                                                 .circular(
                    //                                                     10),
                    //                                         color: Colors.white,
                    //                                         boxShadow: [
                    //                                           BoxShadow(
                    //                                             blurRadius:
                    //                                                 5.0, // soften the shadow
                    //                                             spreadRadius:
                    //                                                 0.3, //extend the shadow
                    //                                             offset: Offset(
                    //                                               0.5, // Move to right 10  horizontally
                    //                                               0.5, // Move to bottom 10 Vertically
                    //                                             ),
                    //                                           )
                    //                                         ],
                    //                                       ),
                    //                                       // color: Colors.red,
                    //                                       //child: Text(approveData[index]['app_id']),
                    //                                       height: 420,
                    //                                       child: Column(
                    //                                         children: [
                    //                                           Padding(
                    //                                             padding:
                    //                                                 const EdgeInsets
                    //                                                         .fromLTRB(
                    //                                                     10,
                    //                                                     10,
                    //                                                     10,
                    //                                                     0),
                    //                                             child:
                    //                                                 Container(
                    //                                                     alignment:
                    //                                                         Alignment
                    //                                                             .centerLeft,
                    //                                                     child:
                    //                                                         Text(
                    //                                                       pendingData[index]
                    //                                                           [
                    //                                                           'initiator'],
                    //                                                       style:
                    //                                                           TextStyle(fontSize: 17),
                    //                                                     )),
                    //                                           ),
                    //                                           Padding(
                    //                                             padding:
                    //                                                 const EdgeInsets
                    //                                                         .fromLTRB(
                    //                                                     10,
                    //                                                     10,
                    //                                                     10,
                    //                                                     0),
                    //                                             child: Container(
                    //                                                 alignment:
                    //                                                     Alignment
                    //                                                         .centerRight,
                    //                                                 child: Text(
                    //                                                     pendingData[index]
                    //                                                         [
                    //                                                         'initiating_date'])),
                    //                                           ),
                    //                                           Container(
                    //                                             child: Column(
                    //                                               children: [
                    //                                                 DataTable(
                    //                                                     headingTextStyle: TextStyle(
                    //                                                         color:
                    //                                                             Colors.black,
                    //                                                         fontWeight: FontWeight.bold),
                    //                                                     //dividerThickness: 4,
                    //                                                     columns: [
                    //                                                       DataColumn(
                    //                                                           label: Text('Application Name')),
                    //                                                       DataColumn(
                    //                                                           label: Text('Note No./Ticket No.')),
                    //                                                     ],
                    //                                                     rows: [
                    //                                                       DataRow(
                    //                                                           cells: [
                    //                                                             DataCell(Text(pendingData[index]['app_id'])),
                    //                                                             DataCell(Text(pendingData[index]['task_id'])),
                    //                                                           ]),
                    //                                                     ]),
                    //                                                 DataTable(
                    //                                                     headingTextStyle: TextStyle(
                    //                                                         color:
                    //                                                             Colors.black,
                    //                                                         fontWeight: FontWeight.bold),
                    //                                                     //dividerThickness: 4,
                    //                                                     columns: [
                    //                                                       DataColumn(
                    //                                                           label: Text('Note Name / Request Type')),
                    //                                                     ],
                    //                                                     rows: [
                    //                                                       DataRow(
                    //                                                           cells: [
                    //                                                             DataCell(Text(
                    //                                                               pendingData[index]['note_name'],
                    //                                                               maxLines: 2,
                    //                                                             )),
                    //                                                           ]),
                    //                                                     ]),
                    //                                                 DataTable(
                    //                                                     headingTextStyle: TextStyle(
                    //                                                         color:
                    //                                                             Colors.black,
                    //                                                         fontWeight: FontWeight.bold),
                    //                                                     //dividerThickness: 4,
                    //                                                     columns: [
                    //                                                       DataColumn(
                    //                                                           label: Text('Subject')),
                    //                                                     ],
                    //                                                     rows: [
                    //                                                       DataRow(
                    //                                                           cells: [
                    //                                                             DataCell(Text(
                    //                                                               pendingData[index]['subject'],
                    //                                                               maxLines: 2,
                    //                                                             )),
                    //                                                           ]),
                    //                                                     ]),
                    //                                               ],
                    //                                             ),
                    //                                           ),
                    //                                           Row(
                    //                                             children: [
                    //                                               Expanded(
                    //                                                 flex: 2,
                    //                                                 child:
                    //                                                     Padding(
                    //                                                   padding: const EdgeInsets
                    //                                                           .only(
                    //                                                       left:
                    //                                                           15,
                    //                                                       right:
                    //                                                           15),
                    //                                                   child: ElevatedButton(
                    //                                                       onPressed:
                    //                                                           () {},
                    //                                                       child:
                    //                                                           Text('Approve')),
                    //                                                 ),
                    //                                               ),
                    //                                               Expanded(
                    //                                                 flex: 2,
                    //                                                 child:
                    //                                                     Padding(
                    //                                                   padding: const EdgeInsets
                    //                                                           .only(
                    //                                                       left:
                    //                                                           15,
                    //                                                       right:
                    //                                                           15),
                    //                                                   child: ElevatedButton(
                    //                                                       onPressed:
                    //                                                           () {},
                    //                                                       child:
                    //                                                           Text('Reject')),
                    //                                                 ),
                    //                                               ),
                    //                                               Expanded(
                    //                                                 flex: 2,
                    //                                                 child:
                    //                                                     Padding(
                    //                                                   padding: const EdgeInsets
                    //                                                           .only(
                    //                                                       left:
                    //                                                           5,
                    //                                                       right:
                    //                                                           5),
                    //                                                   child: ElevatedButton(
                    //                                                       onPressed:
                    //                                                           () {},
                    //                                                       child:
                    //                                                           Text('Send Back')),
                    //                                                 ),
                    //                                               )
                    //                                             ],
                    //                                           )
                    //                                         ],
                    //                                       ),
                    //                                     ),
                    //                                   );
                    //                                 });
                    //                           }),
                    //                     ))
                    //                   ],
                    //                 ),
                    //               )),

                    Container(
                      child: Column(
                        children: [
                          loadingData == true
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 100),
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: CircularProgressIndicator(),
                                  ),
                                )
                              : noDataPending == 'true'
                                  ? Center(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(top: 100),
                                        child: Container(
                                          child: Text(
                                            'No Data Found',
                                            style: TextStyle(
                                                fontSize: 23,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                        ),
                                      ),
                                    )
                                  : Expanded(
                                      child: Column(
                                        children: [
                                          Expanded(
                                              child: LiquidPullToRefresh(
                                            color: const Color(0xFF008577),
                                            key: _refreshIndicatorKey,
                                            onRefresh: _handleRefreshPending,
                                            showChildOpacityTransition: true,
                                            child: StreamBuilder<Object>(
                                                stream: null,
                                                builder: (context, snapshot) {
                                                  return ListView.builder(
                                                      itemCount:
                                                          pendingData.length,
                                                      itemBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                              color:
                                                                  Colors.white,
                                                              boxShadow: [
                                                                BoxShadow(
                                                                  blurRadius:
                                                                      5.0, // soften the shadow
                                                                  spreadRadius:
                                                                      0.3, //extend the shadow
                                                                  offset:
                                                                      Offset(
                                                                    0.5, // Move to right 10  horizontally
                                                                    0.5, // Move to bottom 10 Vertically
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                            // color: Colors.red,
                                                            //child: Text(approveData[index]['app_id']),
                                                            height: 420,
                                                            child: Column(
                                                              children: [
                                                                Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          10,
                                                                          10,
                                                                          10,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                          alignment: Alignment
                                                                              .centerLeft,
                                                                          child:
                                                                              Text(
                                                                            pendingData[index]['initiator'],
                                                                            style:
                                                                                TextStyle(fontSize: 17),
                                                                          )),
                                                                ),
                                                                Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          10,
                                                                          10,
                                                                          10,
                                                                          0),
                                                                  child: Container(
                                                                      alignment:
                                                                          Alignment
                                                                              .centerRight,
                                                                      child: Text(
                                                                          pendingData[index]
                                                                              [
                                                                              'initiating_date'])),
                                                                ),
                                                                Container(
                                                                  child: Column(
                                                                    children: [
                                                                      DataTable(
                                                                          headingTextStyle: TextStyle(
                                                                              color: Colors.black,
                                                                              fontWeight: FontWeight.bold),
                                                                          //dividerThickness: 4,
                                                                          columns: [
                                                                            DataColumn(label: Text('Application Name')),
                                                                            DataColumn(label: Text('Note No./Ticket No.')),
                                                                          ],
                                                                          rows: [
                                                                            DataRow(cells: [
                                                                              DataCell(Text(pendingData[index]['app_id'])),
                                                                              DataCell(Text(pendingData[index]['task_id'])),
                                                                            ]),
                                                                          ]),
                                                                      DataTable(
                                                                          headingTextStyle: TextStyle(
                                                                              color: Colors.black,
                                                                              fontWeight: FontWeight.bold),
                                                                          //dividerThickness: 4,
                                                                          columns: [
                                                                            DataColumn(label: Text('Note Name / Request Type')),
                                                                          ],
                                                                          rows: [
                                                                            DataRow(cells: [
                                                                              DataCell(Text(
                                                                                pendingData[index]['note_name'],
                                                                                maxLines: 2,
                                                                              )),
                                                                            ]),
                                                                          ]),
                                                                      DataTable(
                                                                          headingTextStyle: TextStyle(
                                                                              color: Colors.black,
                                                                              fontWeight: FontWeight.bold),
                                                                          //dividerThickness: 4,
                                                                          columns: [
                                                                            DataColumn(label: Text('Subject')),
                                                                          ],
                                                                          rows: [
                                                                            DataRow(cells: [
                                                                              DataCell(Text(
                                                                                pendingData[index]['subject'],
                                                                                maxLines: 2,
                                                                              )),
                                                                            ]),
                                                                          ]),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Row(
                                                                  children: [
                                                                    Expanded(
                                                                      flex: 2,
                                                                      child:
                                                                          Padding(
                                                                        padding: const EdgeInsets.only(
                                                                            left:
                                                                                15,
                                                                            right:
                                                                                15),
                                                                        child: ElevatedButton(
                                                                            onPressed:
                                                                                () {},
                                                                            child:
                                                                                Text('Approve')),
                                                                      ),
                                                                    ),
                                                                    Expanded(
                                                                      flex: 2,
                                                                      child:
                                                                          Padding(
                                                                        padding: const EdgeInsets.only(
                                                                            left:
                                                                                15,
                                                                            right:
                                                                                15),
                                                                        child: ElevatedButton(
                                                                            onPressed:
                                                                                () {},
                                                                            child:
                                                                                Text('Reject')),
                                                                      ),
                                                                    ),
                                                                    Expanded(
                                                                      flex: 2,
                                                                      child:
                                                                          Padding(
                                                                        padding: const EdgeInsets.only(
                                                                            left:
                                                                                5,
                                                                            right:
                                                                                5),
                                                                        child: ElevatedButton(
                                                                            onPressed:
                                                                                () {},
                                                                            child:
                                                                                Text('Send Back')),
                                                                      ),
                                                                    )
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      });
                                                }),
                                          ))
                                        ],
                                      ),
                                    )
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Container(
                            color: const Color(0xFF008577),
                            height: 60,
                            width: double.infinity,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 30, right: 30),
                              child: DropdownButton<String>(
                                focusColor: Colors.white,
                                value: _chosenValue,

                                //elevation: 5,
                                isExpanded: true,
                                style: TextStyle(color: Colors.white),
                                iconEnabledColor: Colors.white,
                                iconSize: 35,
                                items: <String>[
                                  'Approved',
                                  'Reject',
                                  'Hold',
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 17),
                                    ),
                                  );
                                }).toList(),

                                onChanged: (String value) {
                                  setState(() {
                                    _chosenValue = value;
                                    print(value);
                                    var flag = value == 'Approved'
                                        ? '1'
                                        : value == 'Reject'
                                            ? '2'
                                            : '3';
                                    getApprovedTickets(flag);
                                  });
                                },
                              ),
                            ),
                          ),
                          Container(
                            color: const Color(0xFF008577),
                            height: 60,
                            width: double.infinity,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 26),
                                    child: TextField(
                                        controller: nameController,
                                        keyboardType: TextInputType.number,
                                        style: TextStyle(color: Colors.white),
                                        decoration: InputDecoration(
                                            hintText: 'Search by Task Id',
                                            hintStyle:
                                                TextStyle(color: Colors.white)),
                                        onChanged: (value) {
                                          onItemChanged(value);
                                        }),
                                  ),
                                ),
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                        onPressed: () {
                                          print('seacrh');
                                          //  onItemChanged(nameController.text);
                                        },
                                        icon: Icon(Icons.search,
                                            size: 30, color: Colors.white)))
                              ],
                            ),
                          ),
                          loadingApproveData == true
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 100),
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: CircularProgressIndicator(),
                                  ),
                                )
                              : noDataApproved == 'true'
                                  ? Center(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(top: 100),
                                        child: Container(
                                          child: Text(
                                            'No Data Found',
                                            style: TextStyle(
                                                fontSize: 23,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                        ),
                                      ),
                                    )
                                  : Expanded(
                                      child: Column(
                                        children: [
                                          Expanded(
                                              child: LiquidPullToRefresh(
                                            color: const Color(0xFF008577),
                                            key: _refreshIndicatorKey1,
                                            onRefresh: _handleRefreshApprove,
                                            showChildOpacityTransition: true,
                                            child: StreamBuilder<Object>(
                                                stream: null,
                                                builder: (context, snapshot) {
                                                  return ListView.builder(
                                                      itemCount:
                                                          approveData.length,
                                                      itemBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10),
                                                              color:
                                                                  Colors.white,
                                                              boxShadow: [
                                                                BoxShadow(
                                                                  blurRadius:
                                                                      5.0, // soften the shadow
                                                                  spreadRadius:
                                                                      0.3, //extend the shadow
                                                                  offset:
                                                                      Offset(
                                                                    0.5, // Move to right 10  horizontally
                                                                    0.5, // Move to bottom 10 Vertically
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                            // color: Colors.red,
                                                            //child: Text(approveData[index]['app_id']),
                                                            height: 270,
                                                            child: Column(
                                                              children: [
                                                                Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          10,
                                                                          10,
                                                                          0,
                                                                          0),
                                                                  child: Container(
                                                                      alignment:
                                                                          Alignment
                                                                              .centerLeft,
                                                                      child: Text(
                                                                          'EMC SUPPORT',
                                                                          style:
                                                                              TextStyle(color: Colors.orange))),
                                                                ),
                                                                Container(
                                                                    child:
                                                                        Column(
                                                                  children: [
                                                                    DataTable(
                                                                        headingTextStyle: TextStyle(
                                                                            color:
                                                                                Colors.black,
                                                                            fontWeight: FontWeight.bold),
                                                                        //dividerThickness: 4,
                                                                        columns: [
                                                                          DataColumn(
                                                                              label: Text('Note No./Ticket No.')),
                                                                          DataColumn(
                                                                              label: Text('Task ID')),
                                                                        ],
                                                                        rows: [
                                                                          DataRow(
                                                                              cells: [
                                                                                DataCell(Text(approveData[index]['proc_inst_id'])),
                                                                                DataCell(Text(approveData[index]['task_id'])),
                                                                              ]),
                                                                        ]),
                                                                    Padding(
                                                                      padding:
                                                                          const EdgeInsets.fromLTRB(
                                                                              30,
                                                                              0,
                                                                              30,
                                                                              0),
                                                                      child: DataTable(
                                                                          headingTextStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                                                                          headingRowHeight: 36,
                                                                          // headingRowColor: MaterialStateProperty.all(Colors.red),
                                                                          dataRowHeight: 100,
                                                                          // dataRowColor: MaterialStateProperty.all(Colors.yellow),
                                                                          //dividerThickness: 4,
                                                                          columns: [
                                                                            DataColumn(label: Text('Subject')),
                                                                            DataColumn(
                                                                              label: Text('Remark'),
                                                                            ),
                                                                          ],
                                                                          rows: [
                                                                            DataRow(cells: [
                                                                              DataCell(
                                                                                GestureDetector(
                                                                                    onTap: () {
                                                                                      subjectShow(approveData[index]['subject']);
                                                                                    },
                                                                                    child: Container(
                                                                                        child: Text(
                                                                                      approveData[index]['subject'],
                                                                                      maxLines: 5,
                                                                                      overflow: TextOverflow.ellipsis,
                                                                                    ))),
                                                                              ),
                                                                              DataCell(Text(approveData[index]['remark'] == '' ? '-' : approveData[index]['remark'])),
                                                                            ]),
                                                                          ]),
                                                                    ),
                                                                  ],
                                                                ))
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      });
                                                }),
                                          ))
                                        ],
                                      ),
                                    )
                        ],
                      ),
                    )
                  ])),
            ),
          )),
    );
  }
}
