import 'dart:convert';
import 'dart:io' show Platform;

import 'package:device_info/device_info.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:location/location.dart' as loc;
import 'package:shared_preferences/shared_preferences.dart';

import '../LoadingIndicator.dart';

void main() => runApp(MaterialApp(
      home: CanteenLocation(),
    ));

class CanteenLocation extends StatefulWidget {
  CanteenLocation({this.checkIn});

  String checkIn = '';

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<CanteenLocation> {
  LatLng _initialcameraposition = LatLng(20.5937, 78.9629);
  GoogleMapController _controller;
  GoogleMapController _controllerOff;
  String _currentAddress;
  String _officeAddress = "";
  loc.Location _location = loc.Location();
  bool loadingdata = true;
  var address = '';

  double latitude;
  double longitude;

  double officeLAt;
  double officeLong;

  bool loading = true;

  int selectedTab = 0;

  var jsonData;

  String funKey = '';
  String email = '';
  @override
  void initState() {
    super.initState();
    getData();
    // getDeviceDetails();
    // this.getAttendance();
  }

  getData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    email = preferences.getString('email');
    funKey = preferences.getString('functionValue');
  }

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;
    _location.onLocationChanged.listen((l) {
      latitude = l.latitude;
      longitude = l.longitude;
      _getAddressFromLatLng(l);
      mapMoveTo(latitude, longitude);
      // getAttendance('0');
    });
  }

  _getAddressFromLatLng(l) async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(l.latitude, l.longitude);
      Placemark place = placemarks[0];
      setState(() {
        _currentAddress =
            "${place.locality},${place.street}, ${place.postalCode}, ${place.country}";
        // _textController.text = _currentAddress;
        setState(() {
          address = _currentAddress;
        });
      });
    } catch (e) {
      print(e);
    }
  }

  final TextEditingController _textController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // _textController.text = 'Hello Flutter'; //Set value
    // encrypt();
    return new MaterialApp(
      home: new Scaffold(
        body: new Container(
          margin: const EdgeInsets.only(top: 20.0),
          color: Colors.yellowAccent,
          child: new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                new Expanded(
                  child: new Container(
                    color: Colors.white,
                    child: Column(
                      // Vertically center the widget inside the column
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          
                         
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10,5,10,0),
                            child: Text(address,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                          ),
                        )
                      ],
                    ),
                    
                  ),
                  flex: 1,
                ),
                new Expanded(
                  child: new Container(
                    child: GoogleMap(
                      initialCameraPosition:
                          CameraPosition(target: _initialcameraposition),
                      mapType: MapType.normal,
                      onMapCreated: _onMapCreated,
                      myLocationEnabled: true,
                    ),
                    color: Colors.blue,
                  ),
                  flex: 6,
                ),
                new Expanded(
                  child: new Container(
                    child: new Row(
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 3,
                          child: Container(
                            alignment: Alignment.center,
                            child: RaisedButton(
                              child: Text(
                                'Submit',
                                style: TextStyle(fontSize: 20.0),
                              ),
                              color: Colors.green,
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () {
                                getAttendance();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Container(
                            alignment: Alignment.center,
                            child: RaisedButton(
                              child: Text(
                                'Cancel',
                                style: TextStyle(fontSize: 20.0),
                              ),
                              color: Colors.redAccent,
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        )
                        // new Expanded(
                        //   child: new Container(
                        //     child: Column(
                        //       // Vertically center the widget inside the column
                        //       children: [
                        //         Container(
                        //           margin: const EdgeInsets.only(right: 5.0),
                        //           child: RaisedButton(
                        //             child: Text(
                        //               'Submit',
                        //               style: TextStyle(fontSize: 20.0),
                        //             ),
                        //             color: Colors.green,
                        //             textColor: Colors.white,
                        //             shape: RoundedRectangleBorder(
                        //                 borderRadius:
                        //                     BorderRadius.circular(10)),
                        //             onPressed: () {
                        //               // DialogBuilder(context)
                        //               //     .showLoadingIndicator('');
                        //               getAttendance();
                        //             },
                        //           ),
                        //         )
                        //       ],
                        //     ),
                        //   ),
                        //   flex: 1,
                        // ),
                        // new Expanded(
                        //   child: new Container(
                        //     margin: const EdgeInsets.only(left: 5.0),
                        //     child: RaisedButton(
                        //       child: Text(
                        //         'Cancel',
                        //         style: TextStyle(fontSize: 20.0),
                        //       ),
                        //       color: Colors.redAccent,
                        //       textColor: Colors.white,
                        //       shape: RoundedRectangleBorder(
                        //           borderRadius: BorderRadius.circular(10)),
                        //       onPressed: () {
                        //         Navigator.of(context).pop();
                        //       },
                        //     ),
                        //   ),
                        //   flex: 1,
                        // ),
                      ],
                    ),
                    color: Colors.white,
                  ),
                  flex: 1,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void mapMoveTo(double latitude, double longitude) {
    _controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(latitude, longitude), zoom: 15),
        // new HomePage()._getAddressFromLatLng(l);
      ),
    );
  }

  getAttendance() async {
    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');

    final key1 = encrypt.Key.fromUtf8('VfasRestApi@2020');
    final iv1 = encrypt.IV.fromUtf8('VfasRestApi@2020');

    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));
    final encrypter1 =
        encrypt.Encrypter(encrypt.AES(key1, mode: encrypt.AESMode.cbc));

    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

    final encryptedDate = encrypter1.encrypt(formattedDate, iv: iv1);
    print(encryptedDate.base64.toString());

    var date = getCurrentDate('dd MMM yy');
    var fulldate = date.trim();
    DateTime date1 = DateFormat("dd MMM yy").parse(fulldate);
    final df = new DateFormat('yyyy-MM-dd HH:mm:ss');
    var finalDate = df.format(date1);
    print(finalDate);

    String body = '{"email":' +
        '"' +
        email +
        '"' +
        ',"funKey":' +
        '"' +
        funKey +
        '"' +
        ',"lat":' +
        '"' +
        latitude.toString() +
        '"' +
        ',"lng":' +
        '"' +
        longitude.toString() +
        '"' +
        ',"currAdd":' +
        '"' +
        _currentAddress +
        '"' +
        ',"devDttm":' +
        '"' +
        getCurrentDate('yyyy-MM-dd hh:mm:ss') +
        '" ' +
        '}';

    // String body1 = '{"EmailID":"pankaj.patil@mkttech.in","IMEMINo":"0b366f8d9bc62e66","AttendType":"2","Lat":"19.56693","Long":"72.7987505","punchFlag":"0","address":"Mande Bus Stop, Tembhikhodave Rd, Mande, Maharashtra 401102, India","date":"2021-05-26 11:53:56"}';

    print(body);
    // print(body1);

    final encrypted = encrypter.encrypt(body, iv: iv);
    //print(encrypted.base64.toString());
    var encryptBody = {
      "data": "",
      "type": "IOS",
      "encData": encrypted.base64.toString()
    };
    // print(encryptBody);
    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/vfs/VMSService.svc/AddEmpCanteenDetails_Enc'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          "Accept": "application/json",
          'Charset': 'utf-8',
          'sdt': encryptedDate.base64.toString()
        },
        body: jsonEncode(encryptBody));

    print(body);

    Map<String, dynamic> map = json.decode(response.body);

    print(map);

    // var dect = encrypter.decrypt64(map['result'], iv: iv);
    // // setState(() {
    // loading = false;
    // jsonData = json.decode(dect);
    // _officeAddress = jsonData['officeAdd'];
  }

  getCurrentDate(String format) {
    return DateFormat(format).format(DateTime.now());
  }
}

class DialogBuilder {
  DialogBuilder(this.context);
  final BuildContext context;

  void showLoadingIndicator([String text]) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              backgroundColor: Colors.black87,
              content: LoadingIndicator(text: text),
            ));
      },
    );
  }

  void hideOpenDialog() {
    // Navigator.of(context).pop();
    Navigator.of(context, rootNavigator: true).pop();
  }

  Future<void> _showMyDialog(String title, String location, String punchTime,
      MaterialAccentColor txtcolor, String buttonText) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(location, style: TextStyle(color: txtcolor)),
                Text(punchTime, style: TextStyle(color: txtcolor)),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(buttonText),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
                // hideOpenDialog();
              },
            ),
          ],
        );
      },
    );
  }
}
