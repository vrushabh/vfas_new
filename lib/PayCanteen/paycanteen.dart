import 'package:flutter/material.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:ui';
import 'dart:async';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:vfas/PayCanteen/canteenSubmit.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/sidemenu.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:intl/intl.dart';

void main() {
  runApp(MaterialApp(home: PayCanteen()));
}

class PayCanteen extends StatefulWidget {
  @override
  _PayCanteenState createState() => _PayCanteenState();
}

class _PayCanteenState extends State<PayCanteen>
    with SingleTickerProviderStateMixin {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  var canteenData;
  var loadingData = true;
  var email = '';
  var function = 'f1';
  @override
  void initState() {
    super.initState();
    getCanteenDetails();
  }

  getCanteenDetails() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    email = preferences.getString('email');
    setState(() {
      loadingData = true;
    });

    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');
    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    String body = '{"email":' + '"' + email + '"' + '}';

    final encrypted = encrypter.encrypt(body, iv: iv);
    print(encrypted.base64.toString());

    var encryptBody = {
      'type': 'IOS',
      'encData': '' + encrypted.base64.toString() + ''
    };

    print(body);
    print(encryptBody);
    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/vfs/VMSService.svc/getEmpCanteenDetails_Enc'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Charset': 'utf-8',
          'sdt': ''
        },
        body: jsonEncode(encryptBody));

    var data = json.decode(response.body);

    var dect = encrypter.decrypt64(data['result'], iv: iv);
    setState(() {
      canteenData = json.decode(dect);
      loadingData = false;
    });
    print(json.decode(canteenData));
  }

  payCanteen(value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    email = preferences.getString('email');
    var fun = value == 'f1'
        ? 'F1'
        : value == 'f2'
            ? 'F2'
            : 'F3';
    print(fun);
    preferences.setString('functionValue', fun);
    setState(() {
      loadingData = true;
    });
    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');

    final key1 = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv1 = encrypt.IV.fromUtf8('VirtualAttendanc');

    final encrypter1 =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));
    final encrypter2 =
        encrypt.Encrypter(encrypt.AES(key1, mode: encrypt.AESMode.cbc));

    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

    print(formattedDate);
    final encrypted1 = encrypter1.encrypt(formattedDate, iv: iv);
    print(encrypted1.base64.toString());

    String body = '{ "email" : ' +
        '"' +
        email +
        '" '
            ' }';
    print(body);
    final encrypted2 = encrypter2.encrypt(body, iv: iv1);
    print(encrypted2.base64.toString());

    var encbody = {
      'data': '',
      'type': 'IOS',
      'encData': '' + encrypted2.base64.toString() + ''
    };

    print(encbody);

    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/vfs/VMSService.svc/getCanteenStatus_Enc'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          "Accept": "application/json",
          'Charset': 'utf-8',
          'sdt': encrypted1.base64.toString()
        },
        body: jsonEncode(encbody));

    var data = json.decode(response.body);

    var dect = json.decode(encrypter2.decrypt64(data['result'], iv: iv));
    setState(() {
      loadingData = false;
    });

    print(dect);
    dect['flag'] == '0'
        ? showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  //  title: Text('Do you want to exit App.'),
                  content: Text(dect['msg']),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () async {
                          Navigator.pop(context, false);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => new CanteenLocation(),
                            ),
                          ).then((value) => setState(() {
                                getCanteenDetails();
                              }));
                        },
                        child: Text('OK')),
                  ],
                ))
        : dect['flag'] == '1'
            ? showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      //  title: Text('Do you want to exit App.'),
                      content: Text(dect['msg']),
                      actions: <Widget>[
                        TextButton(
                            onPressed: () async {
                              Navigator.pop(context, false);
                            },
                            child: Text('No')),
                        TextButton(
                            onPressed: () async {
                              Navigator.pop(context, false);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => new CanteenLocation(),
                                ),
                              ).then((value) => setState(() {
                                    getCanteenDetails();
                                  }));
                            },
                            child: Text('Yes'))
                      ],
                    ))
            : showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      //  title: Text('Do you want to exit App.'),
                      content: Text(dect['msg']),
                      actions: <Widget>[
                        TextButton(
                            onPressed: () async {
                              Navigator.pop(context, false);
                            },
                            child: Text('OK')),
                      ],
                    ));
  }

  setRadioValue(value) async {}

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
          appBar: AppBar(
            // centerTitle: true,
            backgroundColor: const Color(0xFF008577),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                final _state = _sideMenuKey.currentState;
                if (_state.isOpened)
                  _state.closeSideMenu();
                else
                  _state.openSideMenu();
              },
            ),
            actions: <Widget>[Count()],
            title: Text('Pay to Canteen'),
          ),
          body: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Note : This Facillity is available only for the Employees of PJ Tower, BSE',
                    style: TextStyle(color: Colors.red, fontSize: 17),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: Text(
                    'F1 - Full Breakfast/Snacks/Lunch',
                    style: TextStyle(
                        color: Colors.blue[400],
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Text(
                    'F2 - Only Breakfast/Snacks',
                    style: TextStyle(
                        color: Colors.blue[400],
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Text(
                    'F3 - Only Tea/Coffee',
                    style: TextStyle(
                        color: Colors.blue[400],
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 25, 15, 0),
                  child: Container(
                    //color: Colors.red,
                    child: CustomRadioButton(
                      // padding: 10,
                      elevation: 25,
                      height: 85,
                      width: 60,
                      spacing: 20.0,
                      // absoluteZeroSpacing: true,
                      enableShape: true,
                      unSelectedColor: Theme.of(context).canvasColor,
                      buttonLables: [
                        'F1',
                        'F2',
                        'F3',
                      ],
                      buttonValues: [
                        'f1',
                        'f2',
                        'f3',
                      ],
                      buttonTextStyle: ButtonTextStyle(
                          selectedColor: Colors.white,
                          unSelectedColor: Colors.black,
                          textStyle: TextStyle(fontSize: 16)),
                      radioButtonValue: (value) {
                        setState(() {
                          function = value;
                        });
                        print(value);
                      },
                      defaultSelected: 'f1',
                      selectedColor: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: ElevatedButton(
                      onPressed: () {
                        payCanteen(function);
                      },
                      child: Text('Pay to Canteen')),
                ),
                loadingData == true
                    ? Container(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator(),
                      )
                    : Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                              child: Row(
                                children: [
                                  Expanded(
                                      flex: 3,
                                      child: Container(
                                          alignment: Alignment.center,
                                          child: Text(
                                            'Function',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ))),
                                  Expanded(
                                      flex: 3,
                                      child: Container(
                                          alignment: Alignment.center,
                                          child: Text(
                                            'Date',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ))),
                                  Expanded(
                                      flex: 3,
                                      child: Container(
                                          alignment: Alignment.center,
                                          child: Text(
                                            'Time',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          )))
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(30, 5, 30, 0),
                              child: Divider(
                                height: 10,
                                thickness: 1.5,
                              ),
                            ),
                            Expanded(
                                child: ListView.builder(
                                    itemCount: canteenData.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            20, 10, 20, 0),
                                        child: Row(
                                          children: [
                                            Expanded(
                                                flex: 3,
                                                child: Container(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                        canteenData[index]
                                                            ['funKey']))),
                                            Expanded(
                                                flex: 3,
                                                child: Container(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                        canteenData[index]
                                                            ['dt']))),
                                            Expanded(
                                                flex: 3,
                                                child: Container(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                        canteenData[index]
                                                            ['tm'])))
                                          ],
                                        ),
                                      );
                                    })),
                          ],
                        ),
                      )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
