import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/MyDrawer.dart';
import 'package:vfas/auth.dart';
import 'package:vfas/PayCanteen/canteenSubmit.dart';
import 'package:vfas/checkin.dart';
import 'package:vfas/dashboard.dart';
import 'package:vfas/emc.dart';
import 'package:vfas/fingerprint.dart';
import 'package:vfas/healthForm.dart';
import 'package:vfas/login.dart';
import 'package:vfas/notification.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/office_manager/canteen.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/holiday.dart';
import 'package:vfas/office_manager/myAttendance.dart';
import 'package:vfas/office_manager/myProfile.dart';
import 'package:vfas/office_manager/omNotification.dart';
import 'package:vfas/otp.dart';
import 'package:vfas/password.dart';
import 'package:vfas/PayCanteen/paycanteen.dart';
import 'package:vfas/resetPassword/changePass.dart';
import 'package:vfas/resetPassword/questions.dart';
import 'package:vfas/resetPassword/resetPasswordUsername.dart';
import 'package:vfas/resetPassword/resetpassword.dart';
import 'package:vfas/sdCount.dart';
import 'package:vfas/sidemenu.dart';
import 'package:local_auth/local_auth.dart';
import 'dart:io';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  print('Handling a background message ${message}');
}

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  runApp(MainHome());
}

class MainHome extends StatefulWidget {
  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  final LocalAuthentication auth = LocalAuthentication();

  var _currentRoute;

  void initState() {
    super.initState();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
              ),
            ));
      }
    });
    _getCurrentRoute();
  }

  Future<void> _authenticateWithBiometrics() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool authenticated = false;
    try {
      authenticated = await auth.authenticate(
          localizedReason: 'Scan your fingerprint or face to Open VFAS',
          useErrorDialogs: true,
          stickyAuth: true,
          biometricOnly: true);
    } on PlatformException catch (e) {
      print('errorrrr----' + e.toString());
      _authenticate();
      return;
    }
    if (!mounted) return;
    print(authenticated);
    if (authenticated) {
      preferences.setString('otpStatus', '1');

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new Dashboard(),
        ),
      ).then((value) => setState(() {}));
    } else {
      exit(0);
    }
  }

  Future<void> _authenticate() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    bool authenticated = false;
    try {
      authenticated = await auth.authenticate(
          localizedReason: 'Scan your fingerprint or face to Open VFAS',
          useErrorDialogs: true,
          stickyAuth: true);
    } on PlatformException catch (e) {
      print(e);

      return;
    }
    if (!mounted) return;
    print(authenticated);
    if (authenticated) {
      preferences.setString('otpStatus', '1');

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new Dashboard(),
        ),
      ).then((value) => setState(() {}));
    } else {
      exit(0);
    }
  }

  _getCurrentRoute() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
     // preferences.setString('otpStatus', '0');
      _currentRoute = preferences.getString('otpStatus');
      print(_currentRoute);
      if (_currentRoute == null || _currentRoute == '0') {
        setState(() {
          _currentRoute = '/login';
        });
      }
      // else if (_currentRoute == '/finger') {
      //   _currentRoute = '/finger';
      // }
      else {
        _authenticateWithBiometrics();

        _currentRoute = '/dashboard';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return (_currentRoute == null)
        ? Container()
        : MaterialApp(
            home: Scaffold(
              drawer: MyDrawer(),
            ),
            initialRoute: _currentRoute,
            routes: {
                '/login': (context) => Login(),
                '/password': (context) => Password(),
                '/otp': (context) => OTP(),
                '/auth': (context) => Auth(),
                '/dashboard': (context) => Dashboard(),
                '/finger': (context) => Fingerprint(),
                '/checkin': (context) => CheckIn(),
                '/notify': (context) => Notify(),
                '/health': (context) => Health(),
                'attendance': (context) => Attendance(),
                'profile': (context) => Profile(),
                'contact': (context) => Contact(),
                'omnotify': (context) => OMNotify(),
                'canteen': (context) => Canteen(),
                'holiday': (context) => Holiday(),
                'paycanteen': (context) => PayCanteen(),
                'sidemenu': (context) => Sidemenu(),
                'reset': (context) => Reset(),
                'count': (context) => Count(),
                'emc': (context) => Emc(),
                'canteenSubmit': (context) => CanteenLocation(),
                'sd' : (context) => SD(),
                'resetusername' : (context) => ResetUsername(),
                'question' : (context) => Questions(),
                'canteenlocation' : (context) => CanteenLocation(),
                'changepassword' : (context) => ChangePassword(),
                
              });
  }
}
