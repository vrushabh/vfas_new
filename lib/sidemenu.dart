import 'package:flutter/material.dart';
// import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:ui';
import 'dart:async';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/PayCanteen/canteenSubmit.dart';
import 'package:vfas/dashboard.dart';
import 'package:vfas/emc.dart';
import 'package:vfas/login.dart';
import 'package:vfas/office_manager/canteen.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/holiday.dart';
import 'package:vfas/office_manager/myAttendance.dart';
import 'package:vfas/office_manager/myProfile.dart';
import 'package:vfas/office_manager/omNotification.dart';
import 'package:vfas/PayCanteen/paycanteen.dart';
import 'package:vfas/resetPassword/resetpassword.dart';
import 'package:vfas/sdCount.dart';

void main() {
  runApp(MaterialApp(home: Sidemenu()));
}

class Sidemenu extends StatefulWidget {
  @override
  _SidemenuState createState() => _SidemenuState();
}

class _SidemenuState extends State<Sidemenu> {
  var visibleApprove = true;
  String code = '';
  String type = '';
  String username = '';

  var notifycount = '';
  String emccount = '0';
  String sdcount = '0';
  String url = '';
  var name = '';
  var canteenShow = '1';
  var canteenValue = true;
  //final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  @override
  void initState() {
    super.initState();
    getManager();
    getCount();
  }

  getManager() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    code = preferences.getString('employeecode');
    type = preferences.getString('employeetype');
    username = preferences.getString('username');
    setState(() {
      canteenShow = preferences.getString('canteenFlag');
      canteenValue = canteenShow == '1' ? true : false;
    });
    print('canteenShow'+canteenShow);
    print(canteenValue);
    

    name = username.split('.')[0];
    url = type == 'Marketplace Employee'
        ? 'https://www.mkttech.in/OMService/OMJsonService.svc/'
        : 'https://dfws.bseindia.com/OMService/OMJsonService.svc/';
    var body = {"emp_id": code, "employeeType": type};
    //  print(body);
    //  print(url);
//G020014

    var response = await http.post(Uri.parse(url + 'getManager'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Charset': 'utf-8'
        },
        body: jsonEncode(body));

    var data = json.decode(response.body);
    print(data);
    // data['flag'] == '1'
    //     ? setState(() {
    //         visibleApprove = true;
    //       })
    //     : setState(() {
    //         visibleApprove = false;
    //       });
  }

  getCount() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    username = preferences.getString('username');
    var body = {
      "approver": username,
      "hashKey": "#iwBHsWgadBQEFubBilw9SjfbWgX1bHpDrcQI2kvBFtM\u003d"
    };
    // print(body);

    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/emcapproval/NoteAppService.svc/menudata'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Charset': 'utf-8'
        },
        body: jsonEncode(body));

    var data = json.decode(response.body);
    print(data);
    setState(() {
      emccount = data['ECMcount'];
      sdcount = data['SDcount'];
    });
  }

  Future<bool> backPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you want to exit App.'),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text('No')),
          TextButton(
              onPressed: () {
                exit(0);
                //Navigator.pop(context, true);
              },
              child: Text('Yes')),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Hello ' + name,
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    code,
                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    type,
                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                  ),
                  SizedBox(height: 20.0),
                ],
              ),
            ),
            SizedBox(height: 16.0),
            ListTile(
              dense: true,
              title: Text(
                "Dashboard",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ),
              leading: Icon(
                Icons.dashboard,
                color: Colors.white,
              ),
              onTap: () {
                // final _state = _sideMenuKey.currentState;
                // if (_state.isOpened)
                //   _state.closeSideMenu();
                // else
                //   _state.openSideMenu();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new Dashboard(),
                  ),
                ).then((value) => setState(() {}));
              },
            ),
            Visibility(
              visible: visibleApprove,
              child: ExpansionTile(
                  title: Text(
                    "Approval",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                  leading: Icon(
                    Icons.portrait_outlined,
                    color: Colors.white,
                  ),
                  children: <Widget>[
                    new ListTile(
                      title: Row(
                        children: [
                          Text(
                            "EMC Tickets",
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 13.0,
                            ),
                          ),
                          Text(''),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Container(
                              width: 30,
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Center(
                                  child: Text(
                                    emccount.toString(),
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      leading: Icon(
                        Icons.file_copy,
                        color: Colors.white,
                      ),
                      dense: true,
                      contentPadding: EdgeInsets.only(left: 50),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => new Emc(),
                          ),
                        ).then((value) => setState(() {}));
                      },
                    ),
                    new ListTile(
                      title: Row(
                        children: [
                          Text(
                            "SD Tickets   ",
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 13.0,
                            ),
                          ),
                          Text(''),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Container(
                              width: 30,
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Center(
                                  child: Text(
                                    sdcount.toString(),
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      leading: Icon(
                        Icons.file_copy,
                        color: Colors.white,
                      ),
                      dense: true,
                      contentPadding: EdgeInsets.only(left: 50),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => new SD(),
                          ),
                        ).then((value) => setState(() {}));
                      },
                    ),
                  ]),
            ),
            ExpansionTile(
                title: Text(
                  "Office Manager",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
                leading: Icon(
                  Icons.portrait_outlined,
                  color: Colors.white,
                ),
                children: <Widget>[
                  new ListTile(
                    title: Text(
                      "My Attendance",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 13.0,
                      ),
                    ),
                    leading: Icon(
                      Icons.work_rounded,
                      color: Colors.white,
                    ),
                    dense: true,
                    contentPadding: EdgeInsets.only(left: 50),
                    onTap: () {
                      // final _state = _sideMenuKey.currentState;
                      // if (_state.isOpened)
                      //   _state.closeSideMenu();
                      // else
                      //   _state.openSideMenu();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => new Attendance(),
                        ),
                      ).then((value) => setState(() {}));
                    },
                  ),
                  new ListTile(
                    title: Text(
                      "My Profile",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 13.0,
                      ),
                    ),
                    leading: Icon(
                      Icons.person_rounded,
                      color: Colors.white,
                    ),
                    dense: true,
                    contentPadding: EdgeInsets.only(left: 50),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => new Profile(),
                        ),
                      ).then((value) => setState(() {}));
                    },
                  ),
                  new ListTile(
                    title: Text(
                      "Contact List",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 13.0,
                      ),
                    ),
                    dense: true,
                    leading: Icon(
                      Icons.contact_phone,
                      color: Colors.white,
                    ),
                    contentPadding: EdgeInsets.only(left: 50),
                    onTap: () {
                      // final _state = _sideMenuKey.currentState;
                      // if (_state.isOpened)
                      //   _state.closeSideMenu();
                      // else
                      //   _state.openSideMenu();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => new Contact(),
                        ),
                      ).then((value) => setState(() {}));
                    },
                  ),
                  new ListTile(
                    title: Text(
                      "OM Notifications",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 13.0,
                      ),
                    ),
                    dense: true,
                    leading: Icon(
                      Icons.notifications,
                      color: Colors.white,
                    ),
                    contentPadding: EdgeInsets.only(left: 50),
                    onTap: () {
                      // final _state = _sideMenuKey.currentState;
                      // if (_state.isOpened)
                      //   _state.closeSideMenu();
                      // else
                      //   _state.openSideMenu();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => new OMNotify(),
                        ),
                      ).then((value) => setState(() {}));
                    },
                  ),
                  new ListTile(
                    title: Text(
                      "Canteen Details",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 13.0,
                      ),
                    ),
                    dense: true,
                    leading: Icon(
                      Icons.fastfood,
                      color: Colors.white,
                    ),
                    contentPadding: EdgeInsets.only(left: 50),
                    onTap: () {
                      // final _state = _sideMenuKey.currentState;
                      // if (_state.isOpened)
                      //   _state.closeSideMenu();
                      // else
                      //   _state.openSideMenu();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => new Canteen(),
                        ),
                      ).then((value) => setState(() {}));
                    },
                  ),
                  new ListTile(
                    title: Text(
                      "Holiday List",
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 13.0,
                      ),
                    ),
                    dense: true,
                    leading: Icon(
                      Icons.calendar_today_rounded,
                      color: Colors.white,
                    ),
                    contentPadding: EdgeInsets.only(left: 50),
                    onTap: () {
                      // final _state = _sideMenuKey.currentState;
                      // if (_state.isOpened)
                      //   _state.closeSideMenu();
                      // else
                      //   _state.openSideMenu();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => new Holiday(),
                        ),
                      ).then((value) => setState(() {}));
                    },
                  ),
                ]),
            ListTile(
              title: Text(
                "Reset Password",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ),
              leading: Icon(
                Icons.vpn_key,
                color: Colors.white,
              ),
              onTap: () {
                // final _state = _sideMenuKey.currentState;
                // if (_state.isOpened)
                //   _state.closeSideMenu();
                // else
                //   _state.openSideMenu();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new Reset(),
                  ),
                ).then((value) => setState(() {}));
              },
            ),
            ListTile(
              dense: true,
              title: Text(
                "Scan QR",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ),
              leading: Icon(
                Icons.qr_code,
                color: Colors.white,
              ),
              // onTap: () => ,
            ),
            Visibility(
              visible: canteenValue,
              child: ListTile(
                title: Text(
                  "Pay To Canteen",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
                leading: Icon(
                  Icons.emoji_food_beverage_rounded,
                  color: Colors.white,
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => new PayCanteen(),
                    ),
                  ).then((value) => setState(() {}));
                },
              ),
            ),
            ListTile(
              title: Text(
                "Logout",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ),
              leading: Icon(
                Icons.logout,
                color: Colors.white,
              ),
              onTap: () async {
                SharedPreferences preferences =
                    await SharedPreferences.getInstance();

                preferences.setString('otpStatus', '0');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new Login(),
                  ),
                ).then((value) => setState(() {}));
              },
            ),
            ListTile(
              title: Text(
                "Exit",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ),
              leading: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onTap: () {
                backPressed();
              },
            ),
          ],
        ),
      ),
    );
  }
}
