
import 'package:encrypt/encrypt.dart' as keyEnc;
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as loc;

void main() => runApp(MaterialApp(
  home: CheckIn(),
));

class CheckIn extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<CheckIn> {
  LatLng _initialcameraposition = LatLng(20.5937, 78.9629);
  GoogleMapController _controller;
  String _currentAddress;
  loc.Location _location = loc.Location();

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;
    _location.onLocationChanged.listen((l) {
      _getAddressFromLatLng(l);
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(l.latitude, l.longitude), zoom: 15),
          // new HomePage()._getAddressFromLatLng(l);
        ),
      );
    });
  }

  _getAddressFromLatLng(l) async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(l.latitude, l.longitude);
      Placemark place = placemarks[0];
      setState(() {
        _currentAddress =
            "${place.locality},${place.street}, ${place.postalCode}, ${place.country}";
        _textController.text = _currentAddress;
      });
    } catch (e) {
      print(e);
    }
  }

  final TextEditingController _textController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // _textController.text = 'Hello Flutter'; //Set value
    // encrypt();
    return new MaterialApp(
      home: new Scaffold(
        body: new Container(
          margin: const EdgeInsets.only(top: 20.0),
          color: Colors.yellowAccent,
          child: new Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                new Expanded(
                  child: new Container(
                    child: Column(
                      // Vertically center the widget inside the column
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: TextField(
                            controller: _textController,
                            textAlign: TextAlign.center,
                            enabled: false,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                                color: Colors.black.withOpacity(0.8)),
                          ),
                        )
                      ],
                    ),
                    color: Colors.grey,
                  ),
                  flex: 1,
                ),
                new Expanded(
                  child: new Container(
                    child: DefaultTabController(
                      length: 2,
                      child: TabBar(
                        tabs: [
                          Tab(
                              text: 'Current Location',
                              icon: Icon(Icons.person)),
                          Tab(
                              text: 'Office Location',
                              icon: Icon(Icons.apartment)),
                        ],
                      ),
                    ),
                    color: Colors.blue,
                  ),
                  flex: 1,
                ),
                new Expanded(
                  child: new Container(
                    child: GoogleMap(
                      initialCameraPosition:
                          CameraPosition(target: _initialcameraposition),
                      mapType: MapType.normal,
                      onMapCreated: _onMapCreated,
                      myLocationEnabled: true,
                    ),
                    color: Colors.blue,
                  ),
                  flex: 6,
                ),
                new Expanded(
                  child: new Container(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        new Expanded(
                          child: new Container(
                            child: Column(
                              // Vertically center the widget inside the column
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child: new Text(
                                    'Your location is',
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 18,
                                        color:
                                            Colors.redAccent.withOpacity(0.8)),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        new Expanded(
                          child: new Container(
                            child: new Text(
                              '56.69 Kms away.',
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                  color: Colors.redAccent.withOpacity(0.8)),
                            ),
                          ),
                        ),
                        new Expanded(
                          child: new Container(
                            child: new Text(
                              'Punch time',
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.redAccent.withOpacity(0.8)),
                            ),
                          ),
                        ),
                        new Expanded(
                          child: new Container(
                            child: new Text(
                              '29/04/2021 07:49 PM',
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                  color: Colors.redAccent.withOpacity(0.8)),
                            ),
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    color: Colors.white,
                  ),
                  flex: 4,
                ),
                new Expanded(
                  child: new Container(
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        new Expanded(
                          child: new Container(
                            child: Column(
                              // Vertically center the widget inside the column
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(right: 5.0),
                                  child: RaisedButton(
                                    child: Text(
                                      'Check Out',
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    color: Colors.green,
                                    textColor: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    onPressed: () {
                                      encrypt();
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                          flex: 1,
                        ),
                        new Expanded(
                          child: new Container(
                            margin: const EdgeInsets.only(left: 5.0),
                            child: RaisedButton(
                              child: Text(
                                'Cancel',
                                style: TextStyle(fontSize: 20.0),
                              ),
                              color: Colors.redAccent,
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              onPressed: () {},
                            ),
                          ),
                          flex: 1,
                        ),
                      ],
                    ),
                    color: Colors.white,
                  ),
                  flex: 1,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void encrypt() {
    try {
      String keySTR = "16 characters"; //16 byte
      String ivSTR = "16 characters"; //16 byte

      final plainText = 'Works!';

      final key = keyEnc.Key.fromUtf8(keySTR);
      final iv = keyEnc.IV.fromUtf8(ivSTR);

      final encrypter = keyEnc.Encrypter(
          keyEnc.AES(key, mode: keyEnc.AESMode.cbc, padding: 'PKCS7'));
      final encrypted = encrypter.encrypt(plainText, iv: iv);
      final decrypted = encrypter.decrypt(encrypted, iv: iv);
      print('decrypted:' + decrypted);
      print('encrypted.base64:' + encrypted.base64);
      /* final plainText = 'Rakesh';
      final key = keyEnc.Key.fromUtf8('VirtualAttendance');
      final iv = keyEnc.IV.fromLength(16);

      final encrypter = keyEnc.Encrypter(keyEnc.AES(key));

      final encrypted = encrypter.encrypt(plainText, iv: iv);
      final decrypted = encrypter.decrypt(encrypted, iv: iv);

      print(decrypted); // Lorem ipsum dolor sit amet, consectetur adipiscing elit
      print(encrypted.base64);*/
    } catch (e) {
      print(e);
    } // R4PxiU3h8YoIRqVowBXm36ZcCeNeZ4s1OvVBTfFlZRdmohQqOpPQqD1YecJeZMAop/hZ4OxqgC1WtwvX/hP9mw==
  }
}