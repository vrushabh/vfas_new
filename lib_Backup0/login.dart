import 'package:flutter/material.dart';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';
import 'package:vfas/password.dart';

void main() {
  runApp(MaterialApp(home: Login()));
}

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  List domain = [];
  String selectedDomain;
  String login;
  static const _key = "2016SSO@";
  static const _iv = "12345678";

  ButtonState stateTextWithIcon = ButtonState.idle;

  Widget buildTextWithIcon() {
    return ProgressButton.icon(iconedButtons: {
      ButtonState.idle: IconedButton(
          text: "Login",
          icon: Icon(Icons.login, color: Colors.white),
          color: Colors.orangeAccent[700]),
      ButtonState.loading:
          IconedButton(text: "Loading", color: Colors.orangeAccent[700]),
      ButtonState.fail: IconedButton(
          text: "Failed",
          icon: Icon(Icons.cancel, color: Colors.white),
          color: Colors.red.shade300),
      ButtonState.success: IconedButton(
          text: "",
          icon: Icon(
            Icons.check_circle,
            color: Colors.white,
          ),
          color: Colors.green.shade400)
    }, onPressed: onPressedIconWithText, state: stateTextWithIcon);
  }

  void onPressedIconWithText() {
    switch (stateTextWithIcon) {
      case ButtonState.idle:
        stateTextWithIcon = ButtonState.loading;
        if (selectedDomain != '' && emailController.text == '') {
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: Text('Please Enter Userid and select Domain'),
                    actions: <Widget>[
                      TextButton(
                          onPressed: () {
                            setState(() {
                              stateTextWithIcon = ButtonState.idle;
                            });
                            Navigator.pop(context, false);
                          },
                          child: Text('OK'))
                    ],
                  ));
        } else if ((selectedDomain == '' && emailController.text != '') ||
            (selectedDomain != '' && emailController.text == '')) {
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: Text('Please Enter Userid Or Select Domain'),
                    actions: <Widget>[
                      TextButton(
                          onPressed: () {
                            setState(() {
                              stateTextWithIcon = ButtonState.idle;
                            });
                            Navigator.pop(context, false);
                          },
                          child: Text('OK'))
                    ],
                  ));
        } else {
          Future.delayed(Duration(seconds: 5), () async {
            var body = {
              'domain': selectedDomain,
              'encType': 'IOS',
              'isSSORegisterationCheck': 'true',
              'login': emailController.text
            };
            var response = await http.post(
                Uri.parse(
                    'https://dfws.bseindia.com/vfs/VMSService.svc/isOMValidUser'),
                headers: {
                  'Content-Type': 'application/json',
                  'Charset': 'utf-8'
                },
                body: jsonEncode(body));

            var data = json.decode(response.body);
            print(data['data']);

            switch (data['data']) {
              case 'success':
                // Navigator.of(context)
                //     .pushNamed("/password")
                //     .then((value) => setState(() {
                //           stateTextWithIcon = ButtonState.idle;
                //         }));

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new Password(
                        name: emailController.text, domain: selectedDomain),
                  ),
                ).then((value) => setState(() {
                      stateTextWithIcon = ButtonState.idle;
                    }));

                break;

              case 'Invalid User':
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          title: Text(data['data']),
                          actions: <Widget>[
                            TextButton(
                                onPressed: () {
                                  setState(() {
                                    stateTextWithIcon = ButtonState.idle;
                                  });
                                  Navigator.pop(context, false);
                                },
                                child: Text('OK'))
                          ],
                        ));
            }
          });
        }

        break;
      case ButtonState.loading:
        break;
      case ButtonState.success:
        stateTextWithIcon = ButtonState.idle;
        break;
      case ButtonState.fail:
        stateTextWithIcon = ButtonState.idle;
        break;
    }
    setState(() {
      stateTextWithIcon = stateTextWithIcon;
    });
  }

  @override
  void initState()  {
    super.initState();
    setState(() {
      stateTextWithIcon = ButtonState.loading;
    });
    getData();
  }

  Future getData() async {
    var response = await http.post(
        Uri.parse('https://dfws.bseindia.com/vfs/VMSService.svc/getDomain'));

    var data = jsonDecode(response.body);

    setState(() {
      domain = data;
      stateTextWithIcon = ButtonState.idle;
      selectedDomain = data[0]['name'];
    });
  }

  Future<bool> backPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you want to exit App.'),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text('No')),
          TextButton(
              onPressed: () {
                  exit(0);
              },
              child: Text('Yes')),
        ],
      ),
    );
  }

  TextEditingController emailController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: WillPopScope(
      onWillPop: backPressed,
      child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/back.jpg'), fit: BoxFit.cover)),
          child: Container(
              margin: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              height: 370,
              width: double.infinity,
              child: Card(
                  color: Colors.white.withOpacity(0.6),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 15),
                              child: Text(
                                'Domain Login ID',
                                style: TextStyle(
                                  fontSize: 22,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0, top: 10.0),
                              child: TextField(
                                controller: emailController,
                                decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.orangeAccent[700],
                                          width: 2.0)),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.orangeAccent[700],
                                          width: 2.0)),
                                  labelText: 'Enter Login ID',
                                  labelStyle: TextStyle(
                                      color: Colors.orangeAccent[700],
                                      fontSize: 17.0),
                                  hintText: 'abc.xyz',
                                  hintStyle: TextStyle(fontSize: 15.0),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            margin: EdgeInsets.all(10.0),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Select Domain",
                              style: TextStyle(
                                  color: Colors.orangeAccent[700],
                                  fontSize: 19.0),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(10.0),
                            alignment: Alignment.centerLeft,
                            child: DropdownButton(
                              style: TextStyle(color: Colors.black),
                              iconSize: 30,
                              isExpanded: true,
                              value: selectedDomain,
                              items: domain.map((e) {
                                return DropdownMenuItem(
                                  child: Text(
                                    e['name'],
                                    style: TextStyle(
                                        color: Colors.orangeAccent[700],
                                        fontSize: 19.0),
                                  ),
                                  value: e['name'],
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  selectedDomain = value;
                                });
                              },
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: buildTextWithIcon()),
                          Container(
                              margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                              child: Text('Reset Password',
                                  style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    color: Colors.blue[400],
                                    fontSize: 20,
                                  ))),
                        ],
                      ))))),
    ));
  }
}
