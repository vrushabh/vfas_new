import 'package:flutter/material.dart';
import 'package:vfas/dashboard.dart';

class MyDrawer extends StatelessWidget {
  @override
  MyDrawer createState() => new MyDrawer();
  // Push the page and remove everything else
  navigateToPage(BuildContext context, String page) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(page, (Route<dynamic> route) => false);
  }

  final GlobalKey<AppExpansionTileState> expansionTile = new GlobalKey();
  String foos = 'One';

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(children: <Widget>[
      Container(
        // padding: EdgeInsets.only(top: 50, left: 0, right: 8, bottom: 8),
        // color: HexColor("#31343E"),
        padding: EdgeInsets.fromLTRB(0, 10, 155, 10),
        //padding: const EdgeInsets.all(8.0),
        color: Colors.blueAccent,
        child: Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(0),
              child: Image.asset(
                "assets/bse_nav_logo.png",
                width: 80,
                height: 80,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              width: 8,
            ),
            RichText(
              text: TextSpan(children: [
                TextSpan(
                    text: "Shrikrishna.parab\n",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                        color: Colors.black87)),
                TextSpan(
                    text: "P020056\n",
                    style: TextStyle(
                        fontFamily: 'Montserrat', color: Colors.black54)),
                TextSpan(
                    text: "BSE Employee",
                    style: TextStyle(
                        fontFamily: 'Montserrat', color: Colors.black54)),
              ]),
            ),
          ],
        ),
      ),
      SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              ListTile(
                dense: true,
                title: Text(
                  "Dashboard",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                leading: Icon(
                  Icons.dashboard,
                  color: Colors.blueAccent,
                ),
              //  onTap: () => 
              ),
              Divider(height: 1, thickness: 1, color: Colors.black12),
              /*ExpansionTile(
                  expandedAlignment: Alignment.centerLeft,
                  childrenPadding: EdgeInsets.fromLTRB(75,0,0,0),
                  title: Text("Expansion Title"),
                  leading: Icon(Icons.notifications),

                  children: <Widget>[Text("children 1",style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.black45 ,

                      fontWeight: FontWeight.normal
                  ),), Text("children 2",style: TextStyle(
                      fontSize: 15.0,

                      color: Colors.black45 ,
                      fontWeight: FontWeight.normal
                  ),)],
                ),*/
              new AppExpansionTile(
                  key: expansionTile,
                  title: Text(
                    "Office Manager",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                  leading: Icon(
                    Icons.portrait_outlined,
                    color: Colors.blueAccent,
                  ),
                  backgroundColor:
                      Theme.of(context).accentColor.withOpacity(0.025),
                  children: <Widget>[
                    new ListTile(
                      title: Text(
                        "My Attendance",
                        style: const TextStyle(
                          color: Colors.black54,
                          fontSize: 13.0,
                        ),
                      ),
                      leading: Icon(
                        Icons.work_rounded,
                        color: Colors.blueAccent,
                      ),
                      dense: true,
                      contentPadding: EdgeInsets.fromLTRB(74, -0.12, 0.0, 1),
                      onTap: () {
                       
                      },
                    ),
                    new ListTile(
                      title: Text(
                        "My Profile",
                        style: const TextStyle(
                          color: Colors.black54,
                          fontSize: 13.0,
                        ),
                      ),
                      leading: Icon(
                        Icons.person_rounded,
                        color: Colors.blueAccent,
                      ),
                      dense: true,
                      contentPadding: EdgeInsets.fromLTRB(74, -0.12, 0.0, 1),
                      onTap: () {
                        //setState(() {
                        // this.foos = 'Three';
                        expansionTile.currentState.collapse();
                        // });
                      },
                    ),
                    new ListTile(
                      title: Text(
                        "Contect List",
                        style: const TextStyle(
                          color: Colors.black54,
                          fontSize: 13.0,
                        ),
                      ),
                      dense: true,
                      leading: Icon(
                        Icons.contact_phone,
                        color: Colors.blueAccent,
                      ),
                      contentPadding: EdgeInsets.fromLTRB(74, -0.12, 0.0, 1),
                      onTap: () {
                        //setState(() {
                        // this.foos = 'Three';
                        expansionTile.currentState.collapse();
                        // });
                      },
                    ),
                    new ListTile(
                      title: Text(
                        "OM Notifications",
                        style: const TextStyle(
                          color: Colors.black54,
                          fontSize: 13.0,
                        ),
                      ),
                      dense: true,
                      leading: Icon(
                        Icons.notifications,
                        color: Colors.blueAccent,
                      ),
                      contentPadding: EdgeInsets.fromLTRB(74, -0.12, 0.0, 1),
                      onTap: () {
                        //setState(() {
                        // this.foos = 'Three';
                        expansionTile.currentState.collapse();
                        // });
                      },
                    ),
                    new ListTile(
                      title: Text(
                        "Canteen Details",
                        style: const TextStyle(
                          color: Colors.black54,
                          fontSize: 13.0,
                        ),
                      ),
                      dense: true,
                      leading: Icon(
                        Icons.fastfood,
                        color: Colors.blueAccent,
                      ),
                      contentPadding: EdgeInsets.fromLTRB(74, -0.12, 0.0, 1),
                      onTap: () {
                        //setState(() {
                        // this.foos = 'Three';
                        expansionTile.currentState.collapse();
                        // });
                      },
                    ),
                    new ListTile(
                      title: Text(
                        "Holiday List",
                        style: const TextStyle(
                          color: Colors.black54,
                          fontSize: 13.0,
                        ),
                      ),
                      dense: true,
                      leading: Icon(
                        Icons.calendar_today_rounded,
                        color: Colors.blueAccent,
                      ),
                      contentPadding: EdgeInsets.fromLTRB(74, -0.12, 0.0, 1),
                      onTap: () {
                        //setState(() {
                        // this.foos = 'Three';
                        expansionTile.currentState.collapse();
                        // });
                      },
                    ),
                  ]),
              Divider(height: 1, thickness: 1, color: Colors.black12),
              ListTile(
                title: Text(
                  "Reset Password",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                leading: Icon(
                  Icons.vpn_key,
                  color: Colors.blueAccent,
                ),
                onTap: () => navigateToPage(context, 'anotherPage'),
              ),
              Divider(height: 1, thickness: 1, color: Colors.black12),
              ListTile(
                dense: true,
                title: Text(
                  "Sacn QR",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                leading: Icon(
                  Icons.qr_code,
                  color: Colors.blueAccent,
                ),
                onTap: () => navigateToPage(context, 'list'),
              ),
              Divider(height: 1, thickness: 1, color: Colors.black12),
              ListTile(
                title: Text(
                  "Pay To Canteen",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                leading: Icon(
                  Icons.emoji_food_beverage_rounded,
                  color: Colors.blueAccent,
                ),
                onTap: () => navigateToPage(context, 'anotherPage'),
              ),
              Divider(height: 1, thickness: 1, color: Colors.black12),
              ListTile(
                title: Text(
                  "Logout",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                leading: Icon(
                  Icons.logout,
                  color: Colors.blueAccent,
                ),
                onTap: () => navigateToPage(context, 'anotherPage'),
              ),
              Divider(height: 1, thickness: 1, color: Colors.black12),
              ListTile(
                title: Text(
                  "Exit",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                leading: Icon(
                  Icons.close,
                  color: Colors.blueAccent,
                ),
                onTap: () => navigateToPage(context, 'anotherPage'),
              ),
            ],
          ),
        ),
      ),
    ]));
  }
}

const Duration _kExpand = const Duration(milliseconds: 200);

class AppExpansionTile extends StatefulWidget {
  const AppExpansionTile({
    Key key,
    this.leading,
    @required this.title,
    this.backgroundColor,
    this.onExpansionChanged,
    this.children: const <Widget>[],
    this.trailing,
    this.initiallyExpanded: false,
  })  : assert(initiallyExpanded != null),
        super(key: key);

  final Widget leading;
  final Widget title;
  final ValueChanged<bool> onExpansionChanged;
  final List<Widget> children;
  final Color backgroundColor;
  final Widget trailing;
  final bool initiallyExpanded;

  @override
  AppExpansionTileState createState() => new AppExpansionTileState();
}

class AppExpansionTileState extends State<AppExpansionTile>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  CurvedAnimation _easeOutAnimation;
  CurvedAnimation _easeInAnimation;
  ColorTween _borderColor;
  ColorTween _headerColor;
  ColorTween _iconColor;
  ColorTween _backgroundColor;
  Animation<double> _iconTurns;

  bool _isExpanded = false;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(duration: _kExpand, vsync: this);
    _easeOutAnimation =
        new CurvedAnimation(parent: _controller, curve: Curves.easeOut);
    _easeInAnimation =
        new CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _borderColor = new ColorTween();
    _headerColor = new ColorTween();
    _iconColor = new ColorTween();
    _iconTurns =
        new Tween<double>(begin: 0.0, end: 0.5).animate(_easeInAnimation);
    _backgroundColor = new ColorTween();

    _isExpanded =
        PageStorage.of(context)?.readState(context) ?? widget.initiallyExpanded;
    if (_isExpanded) _controller.value = 1.0;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void expand() {
    _setExpanded(true);
  }

  void collapse() {
    _setExpanded(false);
  }

  void toggle() {
    _setExpanded(!_isExpanded);
  }

  void _setExpanded(bool isExpanded) {
    if (_isExpanded != isExpanded) {
      setState(() {
        _isExpanded = isExpanded;
        if (_isExpanded)
          _controller.forward();
        else
          _controller.reverse().then<void>((value) {
            setState(() {
              // Rebuild without widget.children.
            });
          });
        PageStorage.of(context)?.writeState(context, _isExpanded);
      });
      if (widget.onExpansionChanged != null) {
        widget.onExpansionChanged(_isExpanded);
      }
    }
  }

  Widget _buildChildren(BuildContext context, Widget child) {
    final Color borderSideColor =
        _borderColor.evaluate(_easeOutAnimation) ?? Colors.transparent;
    final Color titleColor = _headerColor.evaluate(_easeInAnimation);

    return new Container(
      decoration: new BoxDecoration(
          color: _backgroundColor.evaluate(_easeOutAnimation) ??
              Colors.transparent,
          border: new Border(
            top: new BorderSide(color: borderSideColor),
            bottom: new BorderSide(color: borderSideColor),
          )),
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconTheme.merge(
            data:
                new IconThemeData(color: _iconColor.evaluate(_easeInAnimation)),
            child: new ListTile(
              onTap: toggle,
              leading: widget.leading,
              title: new DefaultTextStyle(
                style: Theme.of(context)
                    .textTheme
                    .subhead
                    .copyWith(color: titleColor),
                child: widget.title,
              ),
              trailing: widget.trailing ??
                  new RotationTransition(
                    turns: _iconTurns,
                    child: const Icon(Icons.expand_more),
                  ),
            ),
          ),
          new ClipRect(
            child: new Align(
              heightFactor: _easeInAnimation.value,
              child: child,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    _borderColor.end = theme.dividerColor;
    _headerColor
      ..begin = theme.textTheme.subhead.color
      ..end = theme.accentColor;
    _iconColor
      ..begin = theme.unselectedWidgetColor
      ..end = theme.accentColor;
    _backgroundColor.end = widget.backgroundColor;

    final bool closed = !_isExpanded && _controller.isDismissed;
    return new AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: closed ? null : new Column(children: widget.children),
    );
  }
}
