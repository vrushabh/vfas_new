import 'package:flutter/material.dart';
import 'package:vfas/MyDrawer.dart';
import 'package:vfas/auth.dart';
import 'package:vfas/checkin.dart';
import 'package:vfas/dashboard.dart';
import 'package:vfas/fingerprint.dart';
import 'package:vfas/healthForm.dart';
import 'package:vfas/login.dart';
import 'package:vfas/notification.dart';
import 'package:vfas/otp.dart';
import 'package:vfas/password.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  print('Handling a background message ${message}');
}

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void main() async {
 WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  runApp(MainHome());
}

class MainHome extends StatefulWidget {
  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  var _currentRoute;

  void initState() {
    super.initState();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
              ),
            ));
      }
    });
    _getCurrentRoute();
  }
   

  _getCurrentRoute() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      _currentRoute = preferences.getString('Screen');
      print(_currentRoute);
      if (_currentRoute == null) {
        setState(() {
          _currentRoute = '/login';
        });
      } else if (_currentRoute == '/finger') {
        _currentRoute = '/finger';
      } else if (_currentRoute == '/dashboard') {
        _currentRoute = '/dashboard';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return (_currentRoute == null)
        ? Container()
        : MaterialApp(
            home: Scaffold(
              drawer: MyDrawer(),
            ),
            initialRoute: _currentRoute,
            routes: {
                '/login': (context) => Login(),
                '/password': (context) => Password(),
                '/otp': (context) => OTP(),
                '/auth': (context) => Auth(),
                '/dashboard': (context) => Dashboard(),
                '/finger': (context) => Fingerprint(),
                '/checkin': (context) => CheckIn(),
                '/notify': (context) => Notify(),
                '/health': (context) => Health()
              });
  }
}
