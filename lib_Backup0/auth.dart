import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:flutter/services.dart';
import 'package:vfas/dashboard.dart';
import 'package:vfas/fingerprint.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(home: Auth()));
}

class Auth extends StatefulWidget {
  @override
  _AuthState createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  final LocalAuthentication auth = LocalAuthentication();
  _SupportState _supportState = _SupportState.unknown;
  bool _canCheckBiometrics;
  List<BiometricType> _availableBiometrics;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;

  @override
  void initState() {
    super.initState();
    auth.isDeviceSupported().then(
          (isSupported) => setState(() => _supportState = isSupported
              ? _SupportState.supported
              : _SupportState.unsupported),
        );
  }

  // getpref() async {
  //   preferences.clear();
  // }

  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      canCheckBiometrics = false;
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _canCheckBiometrics = canCheckBiometrics;
    });
  }

  Future<void> _getAvailableBiometrics() async {
    List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      availableBiometrics = <BiometricType>[];
      print(e);
    }
    if (!mounted) return;

    setState(() {
      _availableBiometrics = availableBiometrics;
    });
  }

  Future<void> _authenticate() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });
      authenticated = await auth.authenticate(
          localizedReason: 'Scan your fingerprint or face to Open VFAS',
          useErrorDialogs: true,
          stickyAuth: true);
      setState(() {
        _isAuthenticating = false;
      });
    } on PlatformException catch (e) {
      preferences.setString('Screen', '/dashboard');
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new Dashboard(),
        ),
      ).then((value) => setState(() {}));
      print(e);
      setState(() {
        _isAuthenticating = false;
        _authorized = "Error - ${e.message}";
      });
      return;
    }
    if (!mounted) return;
    print(authenticated);
    if (authenticated) {
      
      preferences.setString('Screen', '/finger');
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new Dashboard(),
        ),
      ).then((value) => setState(() {}));
    }

    setState(
        () => _authorized = authenticated ? 'Authorized' : 'Not Authorized');
  }

  Future<void> _authenticateWithBiometrics() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });
      authenticated = await auth.authenticate(
          localizedReason: 'Scan your fingerprint or face to Open VFAS',
          useErrorDialogs: true,
          stickyAuth: true,
          biometricOnly: true);
      setState(() {
        _isAuthenticating = false;
        _authorized = 'Authenticating';
      });
    } on PlatformException catch (e) {
      print(e);

      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text(
                    'Please Register your Fingerprint or Face to unclock VFAS App.'),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        setState(() {});
                        Navigator.pop(context, false);
                      },
                      child: Text('OK'))
                ],
              ));

      return;
    }
    if (!mounted) return;
    print(authenticated);
    if (authenticated) {
      preferences.setString('Screen', '/finger');
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new Dashboard(),
        ),
      ).then((value) => setState(() {}));
    }
    final String message = authenticated ? 'Authorized' : 'Not Authorized';
    setState(() {
      _authorized = message;
    });
  }

  void _cancelAuthentication() async {
    await auth.stopAuthentication();
    setState(() => _isAuthenticating = false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/back.jpg'), fit: BoxFit.cover)),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 250,
            ),
            Container(
              height: 60,
              width: 300,
              decoration: BoxDecoration(
                  color: Colors.blue[600],
                  borderRadius: BorderRadiusDirectional.circular(10.0)),
              child: TextButton.icon(
                onPressed: () {
                  _authenticateWithBiometrics();
                },
                icon: Icon(
                  Icons.fingerprint,
                  color: Colors.white,
                  size: 40,
                ),
                label: Text(
                  'Fingerprint',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Container(
              height: 60,
              width: 300,
              decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadiusDirectional.circular(10.0)),
              child: TextButton(
                onPressed: () {
                  _authenticate();
                },
                child: Text(
                  'Pin / Pattern',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
          ],
        ),
      )

          // ListView(
          //   padding: const EdgeInsets.only(top: 30),
          //   children: [
          //     Column(
          //       mainAxisAlignment: MainAxisAlignment.center,
          //       children: [
          //         if (_supportState == _SupportState.unknown)
          //           CircularProgressIndicator()
          //         else if (_supportState == _SupportState.supported)
          //           Text("This device is supported")
          //         else
          //           Text("This device is not supported"),
          //         Divider(height: 100),
          //         Text('Can check biometrics: $_canCheckBiometrics\n'),
          //         ElevatedButton(
          //           child: const Text('Check biometrics'),
          //           onPressed: _checkBiometrics,
          //         ),
          //         Divider(height: 100),
          //         Text('Available biometrics: $_availableBiometrics\n'),
          //         ElevatedButton(
          //           child: const Text('Get available biometrics'),
          //           onPressed: _getAvailableBiometrics,
          //         ),
          //         Divider(height: 100),
          //         Text('Current State: $_authorized\n'),
          //         (_isAuthenticating)
          //             ? ElevatedButton(
          //                 onPressed: _cancelAuthentication,
          //                 child: Row(
          //                   mainAxisSize: MainAxisSize.min,
          //                   children: [
          //                     Text("Cancel Authentication"),
          //                     Icon(Icons.cancel),
          //                   ],
          //                 ),
          //               )
          //             : Column(
          //                 children: [
          //                   ElevatedButton(
          //                     child: Row(
          //                       mainAxisSize: MainAxisSize.min,
          //                       children: [
          //                         Text('Authenticate'),
          //                         Icon(Icons.perm_device_information),
          //                       ],
          //                     ),
          //                     onPressed: _authenticate,
          //                   ),
          //                   ElevatedButton(
          //                     child: Row(
          //                       mainAxisSize: MainAxisSize.min,
          //                       children: [
          //                         Text(_isAuthenticating
          //                             ? 'Cancel'
          //                             : 'Authenticate: biometrics only'),
          //                         Icon(Icons.fingerprint),
          //                       ],
          //                     ),
          //                     onPressed: _authenticateWithBiometrics,
          //                   ),
          //                 ],
          //               ),
          //       ],
          //     ),
          //   ],
          // ),
          ),
    );
  }
}

enum _SupportState {
  unknown,
  supported,
  unsupported,
}
