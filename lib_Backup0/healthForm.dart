import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

void main() {
  runApp(MaterialApp(home: Health()));
}

class Health extends StatefulWidget {
  Health({this.email});
  String email = '';
  @override
  _HealthState createState() => _HealthState();
}

class _HealthState extends State<Health> {
  
 num position = 1 ;
 
  final key = UniqueKey();
 
  doneLoading(String A) {
    setState(() {
      position = 0;
    });
  }
 
  startLoading(String A){
    setState(() {
      position = 1;
    });
  }
  
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    
  }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Health Form'),
automaticallyImplyLeading: false,
          backgroundColor: Colors.tealAccent[700],
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  Navigator.pop(context, false);
                },
                icon: Icon(Icons.cancel))
          ],
        ),
        body: 
IndexedStack(index: position, children: <Widget>[
          WebView(
            initialUrl: 'https://m.bseindia.com/healthForm.aspx?id='+widget.email+'&flag=1',
            javascriptMode: JavascriptMode.unrestricted,
            key: key,
            onPageFinished: doneLoading,
            onPageStarted: startLoading,
          ),
          Container(
            color: Colors.white,
            child: Center(child: CircularProgressIndicator()),
          ),
        ]));


  }
}
