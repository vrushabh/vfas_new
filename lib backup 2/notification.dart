import 'package:flutter/material.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    home: Notify(),
  ));
}

class Notify extends StatefulWidget {
  @override
  _NotifyState createState() => _NotifyState();
}

class _NotifyState extends State<Notify> {
  List entries = [];
  var loadingData = true;
  String Email = '';
  @override
  void initState() {
    super.initState();
    this.getNotification();
  }

  getNotification() async {
    setState(() {
      loadingData = true;
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Email = preferences.getString('email');
    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');
    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    String body = '{ "email" : ' + '"' + Email + '" ' + ' }';

    final encrypted = encrypter.encrypt(body, iv: iv);
    print(encrypted.base64.toString());
    var encryptBody = {
      "data": "",
      "type": "Android",
      "encData": encrypted.base64.toString()
    };
    print(encryptBody);

    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/vfs/VMSService.svc/DisplayNotification_Enc'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Charset': 'utf-8'
        },
        body: jsonEncode(encryptBody));

    var data = json.decode(response.body);

    var dect = encrypter.decrypt64(data['result'], iv: iv);
    setState(() {
      entries = json.decode(dect);
      loadingData = false;
    });
    print(entries);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: SafeArea(
          child: Container(
        child: loadingData == true
            ? Container(
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              )
            : Column(
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 15, right: 15, top: 10),
                    child: Container(
                      height: 70,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.blue[400],
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 5,
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: Image(
                                  alignment: Alignment.centerLeft,
                                  image: AssetImage('assets/bse_logo.jpg'),
                                  height: 45,
                                  width: 45,
                                ),
                              ),
                              // height: 60,
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: Container(
                                child: IconButton(
                                    onPressed: () {
                                      Navigator.pop(context, false);
                                    },
                                    icon: Icon(Icons.cancel_sharp)),
                              ))
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemCount: entries.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: entries[index]['readFlag'] == '0'
                                        ? Colors.white
                                        : Colors.grey[400],
                                    borderRadius: BorderRadius.circular(10.0),
                                    boxShadow: [
                                      BoxShadow(
                                        //color: Colors.red,
                                        blurRadius: 7.0, // soften the shadow
                                        spreadRadius: 1.0, //extend the shadow
                                        offset: Offset(
                                          1.0, // Move to right 10  horizontally
                                          1.0, // Move to bottom 10 Vertically
                                        ),
                                      )
                                    ],
                                  ),
                                  height: 100,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 13, right: 10),
                                        child: Container(
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                            entries[index]['date'],
                                            style: TextStyle(),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(15.0),
                                        child: Container(
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                            entries[index]['msg'],
                                            style: TextStyle(),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                          );
                        }),
                  ),
                ],
              ),
      )),
    );
  }
}
