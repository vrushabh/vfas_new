import 'package:flutter/material.dart';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'dart:io';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:vfas/auth.dart';
import 'package:local_auth/local_auth.dart';
import 'package:vfas/dashboard.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:async';

void main() {
  runApp(MaterialApp(
    home: OTP(),
  ));
}

class OTP extends StatefulWidget {
  OTP({this.userdata});
  var userdata;
  @override
  _OTPState createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  String uniqueId = "Unknown";
  String imei = 'unn';
  String id;
  String Mobile = '';
  bool visibleseconds = false;
  _SupportState _supportState = _SupportState.unknown;
  final LocalAuthentication auth = LocalAuthentication();
  var otpdata = {};

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 30), () {
      print('vrushabh completed 30 sec');
      setState(() {
        visibleseconds = true;
      });
    });
    initPlatformState();
    print(widget.userdata);

    Mobile = widget.userdata['mobile'].substring(8, 10);
    auth.isDeviceSupported().then(
          (isSupported) => setState(() => _supportState = isSupported
              ? _SupportState.supported
              : _SupportState.unsupported),
        );
  }

  Future<void> initPlatformState() async {
    String _uniqueId;
    String _imei;

    // Platform messages may fail, so we use a try/catch PlatformException.

    _imei =
        await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: false);

    _uniqueId = await ImeiPlugin.getId();

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      uniqueId = _uniqueId;
      imei = _imei;
    });
    id = Platform.isAndroid == true ? imei : uniqueId;
    addEmployee();
  }

  addEmployee() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('username', widget.userdata['ssoLoginId']);
    var domain = preferences.getString('domain');
    if (widget.userdata['email'] == '' ||
        widget.userdata['email'] == 'null' ||
        widget.userdata['employeeType'] == 'VENDOR') {
      var emailid = widget.userdata['ssoLoginId'] +
          widget.userdata['employeeCode'] +
          '@' +
          domain;
      preferences.setString('email', emailid);
    } else {
      preferences.setString('email', widget.userdata['email']);
    }
    preferences.setString('imei', id);
    preferences.setString('employeecode', widget.userdata['employeeCode']);
    preferences.setString('employeetype', widget.userdata['employeeType']);
    preferences.setString('mobile', widget.userdata['mobile']);

    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');
    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    String body = '{ "empId" : ' +
        ' "' +
        widget.userdata['employeeCode'] +
        '" ' +
        ', "email" : ' +
        ' "' +
        widget.userdata['email'] +
        '" ' +
        ', "mobNo" : ' +
        ' "' +
        widget.userdata['mobile'] +
        '" ' +
        ', "loginId" : ' +
        ' "' +
        widget.userdata['ssoLoginId'] +
        '" ' +
        ', "empType" : ' +
        ' "' +
        widget.userdata['employeeType'] +
        '" ' +
        ', "devId" :' +
        ' "" ' +
        ',' +
        ' "imei" : ' +
        ' "' +
        id +
        '" ' +
        ', "otpFlag" :' +
        ' "1" ' +
        '}';

    print(body);

    final encrypted = encrypter.encrypt(body, iv: iv);
    print(encrypted.base64.toString());
    var encryptBody = {
      "data": "",
      "type": "IOS",
      "encData": encrypted.base64.toString()
    };
    print(encryptBody);

    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/vfs/VMSService.svc/AddEmployeeDetailsNew_Enc'),
        headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
        body: jsonEncode(encryptBody));

    var data = json.decode(response.body);

    var dect = encrypter.decrypt64(data['result'], iv: iv);
    print(dect);
    setState(() {
      otpdata = json.decode(dect);
    });
    print(otpdata);
  }

  ButtonState stateTextWithIcon = ButtonState.idle;

  Widget buildTextWithIcon() {
    return ProgressButton.icon(iconedButtons: {
      ButtonState.idle: IconedButton(
          text: "Submit",
          icon: Icon(Icons.login, color: Colors.white),
          color: Colors.orangeAccent[700]),
      ButtonState.success: IconedButton(
          text: "Submit",
          icon: Icon(Icons.login, color: Colors.white),
          color: Colors.orangeAccent[700]),
      ButtonState.loading:
          IconedButton(text: "Loading", color: Colors.orangeAccent[700]),
      ButtonState.fail: IconedButton(
          text: "Submit",
          icon: Icon(Icons.cancel, color: Colors.white),
          color: Colors.red.shade300),
    }, onPressed: onPressedIconWithText, state: stateTextWithIcon);
  }

  void onPressedIconWithText() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    switch (stateTextWithIcon) {
      case ButtonState.success:
        stateTextWithIcon = ButtonState.loading;
        Future.delayed(Duration(seconds: 5), () async {
          final key = encrypt.Key.fromUtf8('VirtualAttendanc');
          final iv = encrypt.IV.fromUtf8('VirtualAttendanc');
          final encrypter =
              encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

          String body = '{ "mobNo" : ' +
              ' "' +
              otpdata['mobNo'] +
              '" ' +
              ', "imei" : ' +
              ' "' +
              id +
              '" ' +
              ', "otp" : ' +
              ' "' +
              otpdata['otp'] +
              '" ' +
              ',' +
              ' "email" : ' +
              ' "' +
              otpdata['email'] +
              '" ' +
              '}';

          print(body);

          final encrypted = encrypter.encrypt(body, iv: iv);
          print(encrypted.base64.toString());
          var encryptBody = {
            "data": "",
            "type": "Android",
            "encData": encrypted.base64.toString()
          };
          print(encryptBody);
          var response = await http.post(
              Uri.parse(
                  'https://dfws.bseindia.com/vfs/VMSService.svc/VerifyOtp_Enc'),
              headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
              body: jsonEncode(encryptBody));

          var data = json.decode(response.body);
          print(data);
          if (_supportState == _SupportState.supported) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => new Auth(),
              ),
            ).then((value) => setState(() {
                  stateTextWithIcon = ButtonState.idle;
                }));
          } else {
            preferences.setString('otpStatus', '1');

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => new Dashboard(),
              ),
            ).then((value) => setState(() {}));
          }
        });
        break;
      case ButtonState.loading:
        break;
      case ButtonState.idle:
        stateTextWithIcon = ButtonState.idle;
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: Text('Enter Your OTP'),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () {
                          setState(() {
                            stateTextWithIcon = ButtonState.idle;
                          });
                          Navigator.pop(context, false);
                        },
                        child: Text('OK'))
                  ],
                ));
        break;
      case ButtonState.fail:
        break;
    }
    setState(() {
      stateTextWithIcon = stateTextWithIcon;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/back.jpg'), fit: BoxFit.cover)),
        child: Container(
          margin: EdgeInsets.only(
            left: 20,
            right: 20,
          ),
          height: 400,
          width: double.infinity,
          child: Card(
            color: Colors.white.withOpacity(0.6),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Text(
                      widget.userdata['ssoLoginId'],
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Colors.lightBlue[600]),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Text(
                        'OTP',
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      )),
                  Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Text('Enter OTP Code Here',
                          style: TextStyle(
                              fontSize: 18, color: Colors.orangeAccent[700]))),
                  SizedBox(
                    height: 30.0,
                  ),
                  Container(
                    child: PinEntryTextField(
                      showFieldAsBox: true,
                      fields: 6,
                      fontSize: 15.0,
                      fieldWidth: 40.0,
                      onSubmit: (String pin) {
                        if (pin == otpdata['otp']) {
                          stateTextWithIcon = ButtonState.success;
                          buildTextWithIcon();
                        } else {
                          showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                    title: Text('Enter Your Correct OTP'),
                                    actions: <Widget>[
                                      TextButton(
                                          onPressed: () {
                                            setState(() {
                                              stateTextWithIcon =
                                                  ButtonState.idle;
                                            });
                                            Navigator.pop(context, false);
                                          },
                                          child: Text('OK'))
                                    ],
                                  ));
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    child: Text('Resend OTP is disabled for 30 secs'),
                  ),
                  Visibility(
                    visible: visibleseconds,
                    child: Container(
                        child: TextButton(
                            onPressed: () {
                              addEmployee();
                            },
                            child: Text('Resend OTP'))),
                  ),
                  Container(
                    child: Text('OTP has been sent to  ********' + Mobile),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: buildTextWithIcon()),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

enum _SupportState {
  unknown,
  supported,
  unsupported,
}
