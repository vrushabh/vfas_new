import 'package:flutter/material.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/resetPassword/resetpassword.dart';

void main() {
  runApp(MaterialApp(
    home: ResetUsername(),
  ));
}

class ResetUsername extends StatefulWidget {
  @override
  _ResetUsernameState createState() => _ResetUsernameState();
}

class _ResetUsernameState extends State<ResetUsername> {
  TextEditingController userController = new TextEditingController();
  var loading = 'false';
  changePassword() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    print(userController.text);

    if (userController.text == '') {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          //  title: Text('Do you want to exit App.'),
          content: Text('Enter Username first.'),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.pop(context, false);
                },
                child: Text('OK')),
          ],
        ),
      );
    } else {
      setState(() {
        loading = 'true';
      });
      var body = {"encType": "IOS", "login": userController.text};
      print(body);
      var response = await http.post(
          Uri.parse(
              'https://api.bseindia.com/SSOServices/SSOService.svc/userValidation'),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Charset': 'utf-8'
          },
          body: jsonEncode(body));

      var data = json.decode(response.body);
      preferences.setString('username', userController.text);
      setState(() {
        loading = 'false';
      });
      data['data'] != 'success'
          ? showDialog(
              context: context,
              builder: (context) => AlertDialog(
                //  title: Text('Do you want to exit App.'),
                content: Text(data['data']),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      child: Text('OK')),
                ],
              ),
            )
          : Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => new Reset(),
              ),
            ).then((value) => setState(() {}));
      print(data);
    }
  }

  backPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you want to exit App.'),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text('No')),
          TextButton(
              onPressed: () {
                exit(0);
                //Navigator.pop(context, true);
              },
              child: Text('Yes')),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF008577),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
              onPressed: () {
                backPressed();
              },
              icon: Icon(
                Icons.power_settings_new,
                size: 35,
                color: Colors.white,
              ))
        ],
        title: Text('BSE SSO'),
      ),
      // backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/back.jpg'), fit: BoxFit.cover)),
          child: Column(
            children: [
              // Padding(
              //   padding: const EdgeInsets.only(top: 20),
              //   child: Row(
              //     children: [
              //       Expanded(
              //           flex: 3,
              //           child: Padding(
              //             padding: const EdgeInsets.only(left: 20),
              //             child: Container(
              //                 child: Text(
              //               'BSE SSO',
              //               style: TextStyle(
              //                   fontSize: 22,
              //                   fontWeight: FontWeight.bold,
              //                   color: Colors.white),
              //             )),
              //           )),
              //       Expanded(
              //         child: Container(
              //           child: IconButton(
              //               onPressed: () {
              //                 backPressed();
              //               },
              //               icon: Icon(
              //                 Icons.power_settings_new,
              //                 size: 35,
              //                 color: Colors.white,
              //               )),
              //         ),
              //       )
              //     ],
              //   ),
              // ),
              SizedBox(
                height: 120,
              ),
              loading == 'true'
                  ? Padding(
                      padding: const EdgeInsets.fromLTRB(0, 120, 0, 0),
                      child: Container(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator(
                          color: Colors.black,
                        ),
                      ),
                    )
                  : Container(
                      //  color: Colors.red,

                      child: Column(
                        children: [
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0, top: 0.0),
                              child: TextField(
                                controller: userController,
                                decoration: InputDecoration(
                                  fillColor: Colors.white.withOpacity(0.6),
                                  filled: true,
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 2.0)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 2.0)),
                                  labelText:
                                      'Enter Username to Change or Reset Password',
                                  labelStyle: TextStyle(
                                      color: Colors.black, fontSize: 17.0),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 40),
                            child: ElevatedButton(
                                onPressed: () {
                                  changePassword();
                                },
                                child: Text('Change Password')),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 30, 20, 0),
                            child: Container(
                              alignment: Alignment.centerLeft,
                              color: Colors.white.withOpacity(0.8),
                              height: 150,
                              width: double.infinity,
                              child: Column(
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(9, 10, 0, 0),
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'NOTE :-',
                                        style: TextStyle(color: Colors.red),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 10, 0, 0),
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: Row(
                                        children: [
                                          Text(
                                              '1. Make sure you have registered on SSC Portal.'),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 15, 0, 0),
                                    child: Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                            '2. Make sure you have Google Authenticator Application.')),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(10, 20, 0, 0),
                                    child: Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                            '3. Please Enter Computer Login ID (Username)')),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
            ],
          ),
        ),
      ),
    );
  }
}
