import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/resetPassword/questions.dart';
import 'package:vfas/sidemenu.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:external_app_launcher/external_app_launcher.dart';
import 'dart:io';

void main() {
  runApp(MaterialApp(home: Reset()));
}

class Reset extends StatefulWidget {
  @override
  _ResetState createState() => _ResetState();
}

class _ResetState extends State<Reset> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  TextEditingController codeController = new TextEditingController();

  String Username = '';
  var showAppbar = 'false';
  var _currentRoute;
  var loading = 'false';

  @override
  void initState() {
    super.initState();
    setUsername();
  }

  setUsername() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    _currentRoute = preferences.getString('otpStatus');

    setState(() {
      Username = preferences.getString('username');
      showAppbar =
          _currentRoute == null || _currentRoute == '0' ? 'false' : 'true';
    });
  }

  submitAuthentication() async {
    print(codeController.text);
    if (codeController.text == '') {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          //  title: Text('Do you want to exit App.'),
          content: Text('Enter Google Authentication Code first.'),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.pop(context, false);
                },
                child: Text('OK')),
          ],
        ),
      );
    } else {
      setState(() {
        loading = 'true';
      });
      var body = {
        "encType": "IOS",
        "login": Username,
        "otpCode": codeController.text
      };
      print(body);
      var response = await http.post(
          Uri.parse(
              'https://api.bseindia.com/SSOServices/SSOService.svc/userGAValidation'),
          headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'Charset': 'utf-8'
          },
          body: jsonEncode(body));

      var data = json.decode(response.body);
      setState(() {
        loading = 'false';
      });
      data['data'] != 'success'
          ? showDialog(
              context: context,
              builder: (context) => AlertDialog(
                //  title: Text('Do you want to exit App.'),
                content: Text(data['data']),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      child: Text('OK')),
                ],
              ),
            )
          : Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => new Questions(
                  question: data['quesmap'],
                ),
              ),
            ).then((value) => setState(() {}));

      print(data);
    }
  }

  backPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you want to exit App.'),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text('No')),
          TextButton(
              onPressed: () {
                exit(0);
                //Navigator.pop(context, true);
              },
              child: Text('Yes')),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
            appBar: showAppbar == 'true'
                ? AppBar(
                    backgroundColor: const Color(0xFF008577),
                    leading: IconButton(
                      icon: Icon(Icons.menu),
                      onPressed: () {
                        final _state = _sideMenuKey.currentState;
                        if (_state.isOpened)
                          _state.closeSideMenu();
                        else
                          _state.openSideMenu();
                      },
                    ),
                    actions: <Widget>[Count()],
                    title: Text('BSE SSO'),
                  )
                : AppBar(
                    backgroundColor: const Color(0xFF008577),
                    automaticallyImplyLeading: false,
                    actions: <Widget>[
                      IconButton(
                          onPressed: () {
                            backPressed();
                          },
                          icon: Icon(
                            Icons.power_settings_new,
                            size: 35,
                            color: Colors.white,
                          ))
                    ],
                    title: Text('BSE SSO'),
                  ),
            body: Container(
              // alignment: Alignment.center,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/back.jpg'), fit: BoxFit.cover)),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: Container(
                          child: TextButton.icon(
                              onPressed: () {},
                              icon: Icon(
                                Icons.person,
                                size: 30,
                                color: Colors.grey[900],
                              ),
                              label: Text(
                                Username,
                                style: TextStyle(
                                    color: Colors.grey[900], fontSize: 17),
                              )),
                        ),
                      ),
                      Expanded(
                          flex: 2,
                          child: Container(
                              child: Image.asset(
                            'assets/ic_arrow.png',
                            width: 40,
                            height: 40,
                          ))),
                      Expanded(
                          flex: 2,
                          child: GestureDetector(
                            onTap: () async {
                              await LaunchApp.openApp(
                                  androidPackageName:
                                      'com.google.android.apps.authenticator2',
                                  iosUrlScheme: 'pulsesecure://',
                                  appStoreLink:
                                      'itms-apps://itunes.apple.com/us/app/pulse-secure/id945832041',
                                  openStore: true);
                            },
                            child: Container(
                                child: Image.asset(
                              'assets/ic_authenticate_app.png',
                              width: 40,
                              height: 40,
                            )),
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 140,
                  ),
                  loading == 'true'
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(0, 120, 0, 0),
                          child: Container(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(
                              color: Colors.black,
                            ),
                          ),
                        )
                      : Container(
                          //  color: Colors.red,

                          child: Column(
                            children: [
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, right: 10.0, top: 0.0),
                                  child: TextField(
                                    controller: codeController,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                      fillColor: Colors.white.withOpacity(0.6),
                                      filled: true,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      labelText:
                                          'Enter your Google Authentication Code',
                                      labelStyle: TextStyle(
                                          color: Colors.black, fontSize: 17.0),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 40),
                                child: ElevatedButton(
                                    onPressed: () {
                                      submitAuthentication();
                                    },
                                    child: Text('Submit')),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(20, 30, 20, 0),
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  color: Colors.white.withOpacity(0.8),
                                  height: 130,
                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 10, 0, 0),
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            'NOTE :-',
                                            style: TextStyle(color: Colors.red),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 0, 0, 0),
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Row(
                                            children: [
                                              Text('1.'),
                                              TextButton(
                                                  onPressed: () async {
                                                    await LaunchApp.openApp(
                                                        androidPackageName:
                                                            'com.google.android.apps.authenticator2',
                                                        iosUrlScheme:
                                                            'pulsesecure://',
                                                        appStoreLink:
                                                            'itms-apps://itunes.apple.com/us/app/pulse-secure/id945832041',
                                                        openStore: true);
                                                  },
                                                  child: Text(
                                                      'Open Google Code Click Here')),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 0, 0, 10),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '2. Note Google Authentication Code')),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '3. Enter Google Authentication Code')),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                ],
              ),
            )),
      ),
    );
  }
}
