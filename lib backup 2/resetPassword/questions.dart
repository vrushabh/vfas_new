import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:http/http.dart' as http;
import 'package:vfas/notifyCount.dart';
import 'dart:convert';
import 'dart:ui';
import 'dart:io';
import 'package:vfas/resetPassword/changePass.dart';
import 'package:vfas/sidemenu.dart';

void main() {
  runApp(MaterialApp(home: Questions()));
}

class Questions extends StatefulWidget {
  Questions({this.question});
  var question;
  @override
  _QuestionsState createState() => _QuestionsState();
}

class _QuestionsState extends State<Questions> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  TextEditingController answer1 = new TextEditingController();
  TextEditingController answer2 = new TextEditingController();
  var _passwordVisible = true;
  var _passwordVisible2 = true;
  var key1;
  var key2;
  var loading = 'false';
  var showAppbar = 'false';
  var _currentRoute;
  String Username = '';

  @override
  void initState() {
    super.initState();
    setUsername();

    setState(() {
      key1 = widget.question[0]['key'];
      key2 = widget.question[1]['key'];
    });
    print(key1);
    print(key2);
  }

  setUsername() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    _currentRoute = preferences.getString('otpStatus');

    setState(() {
      Username = preferences.getString('username');

      showAppbar =
          _currentRoute == null || _currentRoute == '0' ? 'false' : 'true';
    });
  }

  submitAnswer() async {
    setState(() {
      loading = 'true';
    });
    print('hii');
    print(answer1.text);
    print(answer2.text);
    print(widget.question[0]['key']);
    print(widget.question[1]['key']);
    var body = {
      "a1": answer1.text,
      "a2": answer2.text,
      "encType": "ios",
      "login": Username,
      "q1": key1,
      "q2": key2
    };
    print(body);
    var response = await http.post(
        Uri.parse(
            'https://api.bseindia.com/SSOServices/SSOService.svc/userQuesValidation'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Charset': 'utf-8'
        },
        body: jsonEncode(body));

    var data = json.decode(response.body);
    setState(() {
      loading = 'false';
    });

    data['data'] != 'success'
        ? showDialog(
            context: context,
            builder: (context) => AlertDialog(
              //  title: Text('Do you want to exit App.'),
              content: Text(data['data']),
              actions: <Widget>[
                TextButton(
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                    child: Text('OK')),
              ],
            ),
          )
        : Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => new ChangePassword(
                token: data['token'],
              ),
            ),
          ).then((value) => setState(() {}));
    
    // print(data);
    //{data: Incorrect Answer, token: null}
//{data: success, token: 64981a53074e9327874c00bcbd1706f26c94af28a58ff49cb14cc6b8c07c2472}

    print(data);
  }

  backPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you want to exit App.'),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text('No')),
          TextButton(
              onPressed: () {
                exit(0);
                //Navigator.pop(context, true);
              },
              child: Text('Yes')),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
            appBar: showAppbar == 'true'
                ? AppBar(
                    backgroundColor: const Color(0xFF008577),
                    leading: IconButton(
                      icon: Icon(Icons.menu),
                      onPressed: () {
                        final _state = _sideMenuKey.currentState;
                        if (_state.isOpened)
                          _state.closeSideMenu();
                        else
                          _state.openSideMenu();
                      },
                    ),
                    actions: <Widget>[Count()],
                    title: Text('BSE SSO'),
                  )
                : AppBar(
                    backgroundColor: const Color(0xFF008577),
                    automaticallyImplyLeading: false,
                    actions: <Widget>[
                      IconButton(
                          onPressed: () {
                            backPressed();
                          },
                          icon: Icon(
                            Icons.power_settings_new,
                            size: 35,
                            color: Colors.white,
                          ))
                    ],
                    title: Text('BSE SSO'),
                  ),
            body: Container(
              // alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/back.jpg'), fit: BoxFit.cover)),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: TextButton.icon(
                              onPressed: () {},
                              icon: Icon(
                                Icons.person,
                                size: 30,
                                color: Colors.grey[900],
                              ),
                              label: Text(
                                Username,
                                style: TextStyle(
                                    color: Colors.grey[900], fontSize: 17),
                              )),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 160,
                  ),
                  loading == 'true'
                      ? Padding(
                          padding: const EdgeInsets.fromLTRB(0, 120, 0, 0),
                          child: Container(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(
                              color: Colors.black,
                            ),
                          ),
                        )
                      : Container(
                          //  color: Colors.red,

                          child: Column(
                            children: [
                              Container(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                    child: Text(
                                      widget.question[0]['value'],
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 10.0),
                                  child: TextField(
                                    controller: answer1,
                                    obscureText: _passwordVisible,
                                    decoration: InputDecoration(
                                      fillColor: Colors.white.withOpacity(0.6),
                                      filled: true,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      labelText: 'Enter Answer',
                                      labelStyle: TextStyle(
                                          color: Colors.black, fontSize: 17.0),
                                      hintText: 'Answer',
                                      hintStyle: TextStyle(fontSize: 15.0),
                                      suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _passwordVisible =
                                                  !_passwordVisible;
                                            });
                                          },
                                          icon: Padding(
                                            padding: const EdgeInsets.all(0.0),
                                            child: Icon(
                                              _passwordVisible
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Container(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                    child: Text(
                                      widget.question[1]['value'],
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20.0, top: 10.0),
                                  child: TextField(
                                    controller: answer2,
                                    obscureText: _passwordVisible2,
                                    decoration: InputDecoration(
                                      fillColor: Colors.white.withOpacity(0.6),
                                      filled: true,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black, width: 2.0)),
                                      labelText: 'Enter Answer',
                                      labelStyle: TextStyle(
                                          color: Colors.black, fontSize: 17.0),
                                      hintText: 'Answer',
                                      hintStyle: TextStyle(fontSize: 15.0),
                                      suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              _passwordVisible2 =
                                                  !_passwordVisible2;
                                            });
                                          },
                                          icon: Padding(
                                            padding: const EdgeInsets.all(0.0),
                                            child: Icon(
                                              _passwordVisible2
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                            ),
                                          )),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                  onPressed: () {
                                    submitAnswer();
                                  },
                                  child: Text('Submit')),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(20, 30, 20, 0),
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  color: Colors.white.withOpacity(0.8),
                                  height: 60,
                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 10, 0, 0),
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            'NOTE :-',
                                            style: TextStyle(color: Colors.red),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            9, 5, 0, 0),
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '1. Enter Answers for Above Questions. ')),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                ],
              ),
            )),
      ),
    );
  }
}
