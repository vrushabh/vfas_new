class LocationData {
  int ID;
  String IMEI_NO;
  String EMAIL_ID;
  String MOB_LATITUDE;
  String MOB_LONGITUDE;
  String ADDRESS;
  String DEV_DT_TM;

  final String KEY_ID = "liD";
  final String KEY_IMEI_NO = "imeino";
  final String KEY_EMAIL_ID = "emailID";
  final String KEY_MOB_LATITUDE = "mobilelati";
  final String KEY_MOB_LONGITUDE = "mobilelong";
  final String KEY_ADDRESS = "Address";
  final String KEY_DEV_DT_TM = "devDtTm";

  LocationData(this.ID, this.IMEI_NO, this.EMAIL_ID, this.MOB_LATITUDE,
      this.MOB_LONGITUDE, this.ADDRESS, [this.DEV_DT_TM]);

  LocationData.withId(this.ID, this.IMEI_NO, this.EMAIL_ID, this.MOB_LATITUDE,
      this.MOB_LONGITUDE, this.ADDRESS, [this.DEV_DT_TM]);

  int get liD => ID;

  String get imeino => IMEI_NO;

  String get emailID => EMAIL_ID;

  String get mobilelati => MOB_LATITUDE;

  String get mobilelong => MOB_LONGITUDE;

  String get Address => ADDRESS;

  String get devDtTm => DEV_DT_TM;

  // set title(String newTitle) {
  //   if (newTitle.length <= 255) {
  //     this._title = newTitle;
  //   }
  // }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (liD != null) {
      map[KEY_ID] = liD;
    }
    map[KEY_IMEI_NO] = imeino;
    map[KEY_EMAIL_ID] = emailID;
    map[KEY_MOB_LATITUDE] = mobilelati;
    map[KEY_MOB_LONGITUDE] = mobilelong;
    map[KEY_ADDRESS] = Address;
    map[KEY_DEV_DT_TM] = devDtTm;

    return map;
  }

  // Extract a Note object from a Map object
  LocationData.fromMapObject(Map<String, dynamic> map) {
    this.ID = map[KEY_ID];
    this.IMEI_NO = map[KEY_IMEI_NO];
    this.EMAIL_ID = map[KEY_EMAIL_ID];
    this.MOB_LATITUDE = map[KEY_MOB_LATITUDE];
    this.MOB_LONGITUDE = map[KEY_MOB_LONGITUDE];
    this.ADDRESS = map[KEY_ADDRESS];
    this.DEV_DT_TM = map[KEY_DEV_DT_TM];
  }
}
