import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart' hide Key;
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:uuid/uuid.dart';
import 'package:vfas/MyDrawer.dart';
import 'package:vfas/checkin.dart';
import 'package:vfas/healthForm.dart';
import 'package:vfas/login.dart';
import 'package:vfas/notification.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/myAttendance.dart';
import 'package:vfas/office_manager/myProfile.dart';
import 'package:vfas/office_manager/omNotification.dart';
import 'package:vfas/sidemenu.dart';

void main() {
  runApp(MaterialApp(home: Dashboard()));
}

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  bool visibilityObs = false;
  var counter = '0';
  final dateController = TextEditingController();
  final todateController = TextEditingController();
  List entries = [];
  var scandata;
  var scanIn = '';
  var scanOut = '';
  bool healthFlag = false;
  List notificationData = [];
  bool loading = true;
  bool loadingdata = true;
  bool loadingAddress = true;
  String Username = '';
  String Email = '';
  String IMEI = '';
  String code = '';
  String type = '';
  var name = '';
  var date = '';
  var notifycount = '';
  var visibleApprove = false;
  String emccount = '0';
  String sdcount = '0';
  String url = '';
  var logoutStatus = '';

  bool healthMsgFlag = false;
  SectionHeaderDelegate headerDelegate;

  String checkIn = '';

  @override
  void initState() {
    super.initState();
    this.getEmail();
    this.getToken();
    this.getPhone();
    this.getAttendance();
  }

  getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Username = preferences.getString('username');
    Email = preferences.getString('email');
    IMEI = preferences.getString('imei');
    code = preferences.getString('employeecode');
    type = preferences.getString('employeetype');
    logoutStatus = preferences.getString('otpStatus');
    name = Username.split('.')[0];
    print(logoutStatus + '---');
  }

  getToken() async {
    var uid1;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Username = preferences.getString('username');
    Email = preferences.getString('email');
    IMEI = preferences.getString('imei');
    var type = Platform.isAndroid ? '1' : '2';

    var uuid = Uuid();
    var v1 = uuid.v1();
    var uid = v1.substring(0, 8) + v1.substring(24, 32);

    uid1 = preferences.getString('uuid');
    if (uid1 == null) {
      preferences.setString('uuid', uid);
      uid1 = uid;
    } else {
      uid1 = preferences.getString('uuid');
    }
    String type1 = type + '|' + uid1;

    String token = await FirebaseMessaging.instance.getToken();
    // print('token-----------' + token);

    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');
    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    String body = '{ "EmailID" : ' +
        ' "' +
        Email +
        '" ' +
        ', "IMEMINo" : ' +
        ' "' +
        IMEI +
        '" ' +
        ', "address" : ' +
        ' "" ' +
        ', "QRCode" : ' +
        ' "" ' +
        ', "lat" : ' +
        ' ""' +
        ', "lng" : ' +
        '""' +
        ', "flag" : ' +
        '"1"' +
        ', "version" : ' +
        '"4.4"' +
        ', "devToken" : ' +
        '"' +
        token +
        '"' +
        ', "devType" : ' +
        '"' +
        type1 +
        '"' +
        '}';

    // print(body);

    final encrypted = encrypter.encrypt(body, iv: iv);
    // print(encrypted.base64.toString());

    var encryptBody = {
      'data': '',
      'type': 'IOS',
      'encData': encrypted.base64.toString()
    };
    //print(encryptBody);

    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/vfs/VMSService.svc/empDeviceScanQR_Enc'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Charset': 'utf-8'
        },
        body: jsonEncode(encryptBody));

    var data = json.decode(response.body);

    var dect = json.decode(encrypter.decrypt64(data['result'], iv: iv));

    // print(dect);
    setState(() {
      scandata = dect;
      date = scandata['dispDate'];
      counter = scandata['notifyCnt'];
      scanIn = scandata['scanIn'] == ''
          ? ''
          : scandata['scanOut'] == ''
              ? scandata['scanIn'] + ' /'
              : scandata['scanIn'] + ' /' + scandata['scanOut'];
      //scanOut = scandata['scanOut'];
      checkIn = scandata['scanIn'];
    });

    // IsLogout device Condition.------------------------------------------------

    if (scandata['Islogout'] != '') {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          //  title: Text('Do you want to exit App.'),
          content: Text(scandata['Islogout']),
          actions: <Widget>[
            TextButton(
                onPressed: () async {
                  SharedPreferences preferences =
                      await SharedPreferences.getInstance();

                  preferences.setString('otpStatus', '0');
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => new Login(),
                    ),
                  ).then((value) => setState(() {}));
                },
                child: Text('OK')),
          ],
        ),
      );
    } else {}

    scandata['Hflag'] == '1'
        ? setState(() {
            healthFlag = true;
          })
        : setState(() {
            healthFlag = false;
          });
    scandata['Hflag'] == '1'
        ? showDialog(
            context: context,
            builder: (context) => AlertDialog(
              //  title: Text('Do you want to exit App.'),
              content: Text(scandata['Hmsg']),
              actions: <Widget>[
                TextButton(
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                    child: Text('OK')),
              ],
            ),
          )
        : Container();

    scandata['healthFlag'] == '1'
        ? setState(() {
            healthMsgFlag = true;
          })
        : setState(() {
            healthMsgFlag = false;
          });

//    Reg device Condition.--------------------------------

    // if (scandata['regDevice'] == '1') {
    //   showDialog(
    //     context: context,
    //     builder: (context) => AlertDialog(
    //       //  title: Text('Do you want to exit App.'),
    //       content: Text(scandata['msgDisplay']),
    //       actions: <Widget>[
    //         TextButton(
    //             onPressed: () async {
    //               SharedPreferences preferences =
    //                   await SharedPreferences.getInstance();

    //               preferences.setString('otpStatus', '0');
    //               Navigator.push(
    //                 context,
    //                 MaterialPageRoute(
    //                   builder: (context) => new Login(),
    //                 ),
    //               ).then((value) => setState(() {}));
    //             },
    //             child: Text('OK')),
    //       ],
    //     ),
    //   );
    // } else {
    //   if (scandata['msgDisplay'] != '') {
    //     showDialog(
    //       context: context,
    //       builder: (context) => AlertDialog(
    //         //  title: Text('Do you want to exit App.'),
    //         content: Text(scandata['msgDisplay']),
    //         actions: <Widget>[
    //           TextButton(
    //               onPressed: () async {
    //                 SharedPreferences preferences =
    //                     await SharedPreferences.getInstance();

    //                 preferences.setString('otpStatus', '0');
    //                 Navigator.push(
    //                   context,
    //                   MaterialPageRoute(
    //                     builder: (context) => new Login(),
    //                   ),
    //                 ).then((value) => setState(() {}));
    //               },
    //               child: Text('OK')),
    //         ],
    //       ),
    //     );
    //   }
    // }
    //--------------------------------------------------------------

    preferences.setString('notifyCount', scandata['notifyCnt']);
    preferences.setString('canteenFlag', scandata['canteenFlg']);
    print(scandata);
    // print(scandata['scanIn'].toString());
  }

  void _changed(bool visibility) {
    setState(() {
      visibilityObs = visibility;
    });
    print(visibilityObs);
  }

  getPhone() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    type = preferences.getString('employeetype');
    url = type == 'Marketplace Employee'
        ? 'https://www.mkttech.in/OMService/OMJsonService.svc/'
        : 'https://dfws.bseindia.com/OMService/OMJsonService.svc/';

    var body = {"employeeType": type, "inputString": ""};
    // print(body);
    var response = await http.post(Uri.parse(url + 'getPhoneDirectory'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          'Charset': 'utf-8'
        },
        body: jsonEncode(body));

    var data = json.decode(response.body);
    // print(data);
    var s = json.encode(data);

    preferences.setString('phonedata', s);
  }

  getAttendance() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Email = preferences.getString('email');
    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');
    final encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

    String body = '{ "email" : ' +
        ' "' +
        Email +
        '" ' +
        ', "dtf" : ' +
        ' "" ' +
        ', "dtto" : ' +
        ' "" ' +
        ', "distanceMore" : ' +
        ' "' +
        '" ' +
        '}';

    // print(body);

    final encrypted = encrypter.encrypt(body, iv: iv);
    //print(encrypted.base64.toString());
    var encryptBody = {
      "data": "",
      "type": "IOS",
      "encData": encrypted.base64.toString()
    };
    // print(encryptBody);

    var response = await http.post(
        Uri.parse('https://dfws.bseindia.com/vfs/VMSService.svc/GetAttend_Enc'),
        headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
        body: jsonEncode(encryptBody));

    var data = json.decode(response.body);

    var dect = encrypter.decrypt64(data['result'], iv: iv);
    setState(() {
      loading = false;
      entries = json.decode(dect);
      loadingdata = false;
    });
    print(entries);
  }

  discloseAlert() {
    Navigator.pop(context, false);
  }

  getDateHistory(dt) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Email = preferences.getString('email');
    loadingAddress == true
        ? showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  content: Center(
                      child: Container(
                    child: CircularProgressIndicator(),
                  )),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        discloseAlert();
                      },
                    ),
                  ],
                ))
        : Container();

    var date = dt;
    var fulldate = date.trim();
    DateTime date1 = DateFormat("dd MMM yy").parse(fulldate);
    final df = new DateFormat('yyyyMMdd');
    var finalDate = df.format(date1);
    print(finalDate);

    final key = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv = encrypt.IV.fromUtf8('VirtualAttendanc');

    final key1 = encrypt.Key.fromUtf8('VirtualAttendanc');
    final iv1 = encrypt.IV.fromUtf8('VirtualAttendanc');

    final encrypter1 =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));
    final encrypter2 =
        encrypt.Encrypter(encrypt.AES(key1, mode: encrypt.AESMode.cbc));

    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);

    print(formattedDate);
    final encrypted1 = encrypter1.encrypt(formattedDate, iv: iv);
    print(encrypted1.base64.toString());

    String body = '{ "email" : ' +
        '"' +
        Email +
        '" ' +
        ', "date" :' +
        '"' +
        finalDate +
        '"' +
        ' }';
    print(body);
    final encrypted2 = encrypter2.encrypt(body, iv: iv1);
    print(encrypted2.base64.toString());

    var encbody = {
      'data': '',
      'type': 'IOS',
      'encData': encrypted2.base64.toString()
    };

    print(encbody);

    var response = await http.post(
        Uri.parse(
            'https://dfws.bseindia.com/vfs/VMSService.svc/GetEmpTodaysHistory_Enc'),
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          "Accept": "application/json",
          'Charset': 'utf-8',
          'sdt': encrypted1.base64.toString()
        },
        body: jsonEncode(encbody));

    var data = json.decode(response.body);

    var dect = json.decode(encrypter2.decrypt64(data['result'], iv: iv));

    print(dect);
    setState(() {
      loadingAddress = false;
    });
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(Username),
              content: ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: dect.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        child: Column(
                      children: [
                        Container(
                            alignment: Alignment.centerRight,
                            child: Text(
                              dect[index]['dt'] + ' ' + dect[index]['tm'],
                              style: TextStyle(color: Colors.blue[300]),
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Distance : ' + dect[index]['distance'],
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        Text(dect[index]['address']),
                        Divider(
                          height: 50,
                          color: Colors.black,
                        )
                      ],
                    ));
                  }),
              actions: <Widget>[
                TextButton(
                    onPressed: () {
                      discloseAlert();
                      Navigator.pop(context, false);
                    },
                    child: Text('OK')),
              ],
            ));

    // print(dect[1]['address']);
  }

  Future<bool> backPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Do you want to exit App.'),
        actions: <Widget>[
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text('No')),
          TextButton(
              onPressed: () {
                exit(0);
                //Navigator.pop(context, true);
              },
              child: Text('Yes')),
        ],
      ),
    );
  }

  search(value) {
    _changed(value);
    _showSheet();
  }

  getAttn(value) {
    _changed(value);
    _showSheet();
  }

  void _showSheet() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (BuildContext context,
              StateSetter setState /*You can rename this!*/) {
            return Container(
                child: Material(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 50,
                    color: Colors.grey[500],
                    child: Row(
                      children: [
                        Expanded(
                          flex: 4,
                          child: Container(
                              width: double.infinity,
                              height: 50,
                              alignment: Alignment.bottomLeft,
                              color: Colors.grey[500],
                              child: TextButton.icon(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.keyboard_arrow_up,
                                  color: Colors.white,
                                  size: 35,
                                ),
                                label: Text('Attendance',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                    )),
                              )),
                        ),
                      ],
                    ),
                  ),
                  Visibility(
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: Container(
                            child: Center(
                                child: Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: TextField(
                                readOnly: true,
                                controller: dateController,
                                decoration:
                                    InputDecoration(hintText: 'From Date'),
                                onTap: () async {
                                  var date = await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(1900),
                                      lastDate: DateTime(2050));
                                  dateController.text =
                                      date.toString().substring(0, 10);
                                },
                              ),
                            )),
                          ),
                        ),
                        Expanded(flex: 1, child: Container(child: Text(''))),
                        Expanded(
                          flex: 3,
                          child: Container(
                            child: Center(
                                child: TextField(
                              readOnly: true,
                              controller: todateController,
                              decoration: InputDecoration(hintText: 'To Date'),
                              onTap: () async {
                                var date = await showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(1900),
                                    lastDate: DateTime(2050));
                                todateController.text =
                                    date.toString().substring(0, 10);
                              },
                            )),
                          ),
                        ),
                        Expanded(child: Container(child: Text(''))),
                        Expanded(
                          flex: 3,
                          child: Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 10, 0),
                              child: ElevatedButton(
                                  onPressed: () async {
                                    SharedPreferences preferences =
                                        await SharedPreferences.getInstance();
                                    Email = preferences.getString('email');
                                    setState(() {
                                      loading = true;
                                    });
                                    var frm = dateController.text
                                        .replaceAll(RegExp('-'), '');
                                    var to = todateController.text
                                        .replaceAll(RegExp('-'), '');

                                    int frmdate = int.parse(frm);
                                    int todate = int.parse(to);

                                    print(frmdate);
                                    print(todate);
                                    if (frmdate < todate) {
                                      final key = encrypt.Key.fromUtf8(
                                          'VirtualAttendanc');
                                      final iv = encrypt.IV
                                          .fromUtf8('VirtualAttendanc');
                                      final encrypter = encrypt.Encrypter(
                                          encrypt.AES(key,
                                              mode: encrypt.AESMode.cbc));

                                      String body = '{ "email" : ' +
                                          ' "' +
                                          Email +
                                          '" ' +
                                          ', "dtf" : ' +
                                          ' "' +
                                          frm +
                                          '" ' +
                                          ', "dtto" : ' +
                                          ' "' +
                                          to +
                                          '" ' +
                                          ', "distanceMore" : ' +
                                          ' "' +
                                          '" ' +
                                          '}';

                                      print(body);

                                      final encrypted =
                                          encrypter.encrypt(body, iv: iv);
                                      print(encrypted.base64.toString());
                                      var encryptBody = {
                                        "data": "",
                                        "type": "IOS",
                                        "encData": encrypted.base64.toString()
                                      };
                                      print(encryptBody);

                                      var response = await http.post(
                                          Uri.parse(
                                              'https://dfws.bseindia.com/vfs/VMSService.svc/GetAttend_Enc'),
                                          headers: {
                                            'Content-Type': 'application/json',
                                            'Charset': 'utf-8'
                                          },
                                          body: jsonEncode(encryptBody));

                                      var data = json.decode(response.body);

                                      var dect = encrypter
                                          .decrypt64(data['result'], iv: iv);
                                      setState(() {
                                        entries = json.decode(dect);
                                        loading = false;
                                      });
                                      print(entries);
                                    } else if (frmdate > todate) {
                                      setState(() {
                                        loading = false;
                                      });
                                      showDialog(
                                          context: context,
                                          builder: (context) => AlertDialog(
                                                title: Text(
                                                    'From date is larger than To date.'),
                                                actions: <Widget>[
                                                  TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(
                                                            context, false);
                                                      },
                                                      child: Text('OK'))
                                                ],
                                              ));
                                    } else {
                                      setState(() {
                                        loading = false;
                                      });
                                      showDialog(
                                          context: context,
                                          builder: (context) => AlertDialog(
                                                title: Text(
                                                    'Please Select From and To date.'),
                                                actions: <Widget>[
                                                  TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(
                                                            context, false);
                                                      },
                                                      child: Text('OK'))
                                                ],
                                              ));
                                    }
                                  },
                                  child: Text('Submit'))),
                        ),
                      ],
                    ),
                    visible: visibilityObs,
                  ),
                  Container(
                    child: Row(
                      children: [
                        Expanded(
                            flex: 2,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Container(
                                  alignment: Alignment.center,
                                  color: Colors.white,
                                  child: Text(
                                    'Date',
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                  )),
                            )),
                        Expanded(
                            flex: 3,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Container(
                                  alignment: Alignment.center,
                                  color: Colors.white,
                                  child: Text(
                                    'IN/OUT',
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                  )),
                            )),
                        Expanded(
                            flex: 2,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Container(
                                  alignment: Alignment.center,
                                  color: Colors.white,
                                  child: Text(
                                    'WH',
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                  )),
                            )),
                      ],
                    ),
                  ),
                  loading == false
                      ? Expanded(
                          child: ListView.builder(
                              padding: const EdgeInsets.all(8),
                              itemCount: entries.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  height: 50,
                                  child: Container(
                                    child: Row(
                                      children: [
                                        Expanded(
                                            flex: 2,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              child: Container(
                                                  alignment: Alignment.center,
                                                  color: entries[index]
                                                              ['colorFlag'] ==
                                                          '1'
                                                      ? const Color(0xFFC6C6C6)
                                                      : entries[index][
                                                                  'colorFlag'] ==
                                                              '2'
                                                          ? const Color(
                                                              0xFFFFD29D)
                                                          : Colors.white,
                                                  child: TextButton(
                                                    onPressed: () {
                                                      this.getDateHistory(
                                                          entries[index]
                                                              ['dttm']);
                                                    },
                                                    child: Text(
                                                      entries[index]['dttm'],
                                                      style: TextStyle(
                                                          color:
                                                              Colors.blue[700]),
                                                    ),
                                                  )),
                                            )),
                                        Expanded(
                                            flex: 3,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              child: Container(
                                                  alignment: Alignment.center,
                                                  color: entries[index]
                                                              ['colorFlag'] ==
                                                          '1'
                                                      ? const Color(0xFFC6C6C6)
                                                      : entries[index][
                                                                  'colorFlag'] ==
                                                              '2'
                                                          ? const Color(
                                                              0xFFFFD29D)
                                                          : Colors.white,
                                                  child: Text(
                                                    entries[index]['scanIn'] +
                                                        ' / ' +
                                                        entries[index]
                                                            ['scanOut'],
                                                    style: TextStyle(),
                                                  )),
                                            )),
                                        Expanded(
                                            flex: 2,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10),
                                              child: Container(
                                                  alignment: Alignment.center,
                                                  color: entries[index]
                                                              ['colorFlag'] ==
                                                          '1'
                                                      ? const Color(0xFFC6C6C6)
                                                      : entries[index][
                                                                  'colorFlag'] ==
                                                              '2'
                                                          ? const Color(
                                                              0xFFFFD29D)
                                                          : Colors.white,
                                                  child: Text(
                                                    entries[index]['duration'],
                                                    style: TextStyle(),
                                                  )),
                                            )),
                                      ],
                                    ),
                                  ),
                                );
                              }),
                        )
                      : Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(),
                        ),
                ],
              ),
            ));
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false,
      // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),
      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: const Color(0xFF008577),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                final _state = _sideMenuKey.currentState;
                if (_state.isOpened)
                  _state.closeSideMenu();
                else
                  _state.openSideMenu();
              },
            ),
            actions: <Widget>[
              new Stack(
                children: <Widget>[
                  new IconButton(
                      icon: Icon(
                        Icons.notifications,
                        size: 35,
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => new Notify(),
                          ),
                        ).then((value) => getToken());
                      }),
                  int.parse(counter) > 0
                      ? new Positioned(
                          right: 4,
                          top: 5,
                          child: new Container(
                            padding: EdgeInsets.all(2),
                            decoration: new BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            constraints: BoxConstraints(
                              minWidth: 17,
                              minHeight: 17,
                            ),
                            child: Text(
                              '$counter',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 13,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                      : new Container()
                ],
              ),
            ],
            title: Text('Dashboard'),
          ),
          bottomSheet: Container(
            height: 50,
            color: Colors.grey[500],
            child: Row(
              children: [
                Expanded(
                  flex: 4,
                  child: Container(
                      width: double.infinity,
                      height: 50,
                      alignment: Alignment.bottomLeft,
                      color: Colors.grey[500],
                      child: TextButton.icon(
                        onPressed: () {
                          getAttn(false);
                        },
                        icon: Icon(
                          Icons.keyboard_arrow_up,
                          color: Colors.white,
                          size: 35,
                        ),
                        label: Text('Attendance',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            )),
                      )),
                ),
                Expanded(
                    flex: 1,
                    child: Container(
                      child: IconButton(
                          onPressed: () {
                            search(true);
                          },
                          icon: Icon(
                            Icons.search,
                            color: Colors.white,
                            size: 30,
                          )),
                    ))
              ],
            ),
          ),
          body: WillPopScope(
              onWillPop: backPressed,
              child: SizedBox.expand(
                  child: Stack(children: <Widget>[
                //FlutterLogo(size: 500),
                Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/back.jpg'),
                            fit: BoxFit.cover)),
                    child: Container(
                        margin: EdgeInsets.only(
                          left: 20,
                          right: 20,
                        ),
                        height: 380,
                        width: double.infinity,
                        child: loadingdata == true
                            ? Center(
                                child: Container(
                                child: CircularProgressIndicator(),
                              ))
                            : Card(
                                color: Colors.white.withOpacity(0.7),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: [
                                            Expanded(
                                              flex: 3,
                                              child: Container(
                                                // color:Colors.red,
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 3, right: 18),
                                                  child: Text(
                                                    'Welcome',
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 22,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 1,
                                              child: IconButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      loadingdata = true;
                                                    });
                                                    getToken();
                                                    getAttendance();
                                                  },
                                                  icon: Icon(Icons.refresh)),
                                            )
                                          ],
                                        ),
                                        Container(
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 5),
                                            child: Text(
                                              Username,
                                              style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black54,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 5),
                                            child: Text(
                                              date,
                                              style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.blue,
                                              ),
                                            ),
                                          ),
                                        ),
                                        healthFlag == true
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 25,
                                                    right: 25,
                                                    top: 10),
                                                child: ElevatedButton(
                                                    style: ButtonStyle(
                                                        backgroundColor:
                                                            MaterialStateProperty
                                                                .all(
                                                                    Colors.grey)

                                                        // MaterialStateProperty.all(const Color(0xff0F8525)),
                                                        ),
                                                    child: Container(
                                                      height: 60,
                                                      child: Row(
                                                        children: [
                                                          Expanded(
                                                            flex: 1,
                                                            child: Icon(
                                                              Icons
                                                                  .touch_app_rounded,
                                                              color:
                                                                  Colors.white,
                                                              size: 35,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(''),
                                                          ),
                                                          Expanded(
                                                              flex: 8,
                                                              child: Container(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                //color: Colors.red,
                                                                child: Column(
                                                                  children: [
                                                                    Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          top:
                                                                              12),
                                                                      child:
                                                                          Container(
                                                                        child: Text(
                                                                            'Check IN / OUT',
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 15,
                                                                            )),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                      height: 2,
                                                                    ),
                                                                    Container(
                                                                      child:
                                                                          Text(
                                                                        scanIn,
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              15,
                                                                          color:
                                                                              Colors.black,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ))
                                                        ],
                                                      ),
                                                    )),
                                              )
                                            : Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 25,
                                                    right: 25,
                                                    top: 10),
                                                child: ElevatedButton(
                                                    onPressed: () {
                                                      healthMsgFlag == true
                                                          ? Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        CheckIn(
                                                                  checkIn:
                                                                      checkIn,
                                                                ),
                                                              ),
                                                            ).then((value) =>
                                                              setState(() {
                                                                loadingdata =
                                                                    true;

                                                                getToken();
                                                                getAttendance();
                                                              }))
                                                          : showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) =>
                                                                      AlertDialog(
                                                                //  title: Text('Do you want to exit App.'),
                                                                content: Text(
                                                                    'Please fill health form first .'),
                                                                actions: <
                                                                    Widget>[
                                                                  TextButton(
                                                                      onPressed:
                                                                          () {
                                                                        Navigator.pop(
                                                                            context,
                                                                            false);
                                                                        Navigator
                                                                            .push(
                                                                          context,
                                                                          MaterialPageRoute(
                                                                            builder: (context) =>
                                                                                Health(
                                                                              email: Email,
                                                                            ),
                                                                          ),
                                                                        ).then((value) =>
                                                                            setState(() {
                                                                              loadingdata = true;
                                                                              getToken();
                                                                              getAttendance();
                                                                            }));
                                                                      },
                                                                      child: Text(
                                                                          'OK')),
                                                                ],
                                                              ),
                                                            );
                                                    },
                                                    style: ButtonStyle(
                                                        backgroundColor:
                                                            MaterialStateProperty
                                                                .all(const Color(
                                                                    0xff0F8525))
                                                        // MaterialStateProperty.all(const Color(0xff0F8525)),
                                                        ),
                                                    child: Container(
                                                      height: 60,
                                                      child: Row(
                                                        children: [
                                                          Expanded(
                                                            flex: 1,
                                                            child: Icon(
                                                              Icons
                                                                  .touch_app_rounded,
                                                              color:
                                                                  Colors.white,
                                                              size: 35,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(''),
                                                          ),
                                                          Expanded(
                                                              flex: 8,
                                                              child: Container(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                //color: Colors.red,
                                                                child: Column(
                                                                  children: [
                                                                    Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          top:
                                                                              12),
                                                                      child:
                                                                          Container(
                                                                        child: Text(
                                                                            'Check IN / OUT',
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.white,
                                                                              fontSize: 15,
                                                                            )),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                      height: 2,
                                                                    ),
                                                                    Container(
                                                                      child:
                                                                          Text(
                                                                        scanIn,
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              15,
                                                                          color:
                                                                              Colors.black,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ))
                                                        ],
                                                      ),
                                                    )),
                                              ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 25, right: 25, top: 10),
                                          child: ElevatedButton(
                                              onPressed: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        Health(
                                                      email: Email,
                                                    ),
                                                  ),
                                                ).then(
                                                    (value) => setState(() {}));
                                              },
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty.all(
                                                        const Color(
                                                            0xff450c0c)),
                                              ),
                                              child: Container(
                                                height: 60,
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      flex: 1,
                                                      child: Image(
                                                        image: AssetImage(
                                                          'assets/heart.png',
                                                        ),
                                                        fit: BoxFit.scaleDown,
                                                        height: 45,
                                                        width: 45,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 1,
                                                      child: Text(''),
                                                    ),
                                                    Expanded(
                                                        flex: 8,
                                                        child: Container(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          //color: Colors.red,
                                                          child: Column(
                                                            children: [
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top:
                                                                            20),
                                                                child:
                                                                    Container(
                                                                  child: Text(
                                                                      'Health Form',
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            15,
                                                                      )),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ))
                                                  ],
                                                ),
                                              )),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 25, right: 25, top: 10),
                                          child: ElevatedButton(
                                              onPressed: () {
                                                backPressed();
                                              },
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty.all(
                                                        const Color(
                                                            0xff1C83B3)),
                                              ),
                                              child: Container(
                                                height: 60,
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      flex: 1,
                                                      child: Icon(
                                                        Icons.close,
                                                        color: Colors.white,
                                                        size: 35,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 1,
                                                      child: Text(''),
                                                    ),
                                                    Expanded(
                                                        flex: 8,
                                                        child: Container(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          //color: Colors.red,
                                                          child: Column(
                                                            children: [
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top:
                                                                            20),
                                                                child:
                                                                    Container(
                                                                  child: Text(
                                                                      'Exit',
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            15,
                                                                      )),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ))
                                                  ],
                                                ),
                                              )),
                                        ),
                                        Container(
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(top: 10),
                                            child: Text(
                                              'Version 4.5',
                                              style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.normal,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )))))
              ]))),
        ),
      ),
    );
  }
}

class SectionHeaderDelegate extends SliverPersistentHeaderDelegate {
  final String title;
  final double height;
  final bool flag;

  //final PreferredSize child;

  SectionHeaderDelegate(this.title, this.flag, this.height);

  @override
  Widget build(context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(
      child: Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: Flexible(
              child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  // margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  child: Visibility(
                    visible: flag,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: TextField(
                              decoration: InputDecoration(
                                isDense: true,
                                border: InputBorder.none,
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.blueAccent, width: 2.0)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.blueAccent, width: 2.0)),
                                labelText: 'From Date',
                                labelStyle: TextStyle(
                                    color: Colors.blueAccent, fontSize: 15.0),
                                hintText: 'From Date',
                                hintStyle: TextStyle(fontSize: 13.0),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: TextField(
                              decoration: InputDecoration(
                                isDense: true,
                                border: InputBorder.none,
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.blueAccent, width: 2.0)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.blueAccent, width: 2.0)),
                                labelText: 'To Date',
                                labelStyle: TextStyle(
                                    color: Colors.blueAccent, fontSize: 15.0),
                                hintText: 'To Date',
                                hintStyle: TextStyle(fontSize: 13.0),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 5, 5, 0),
                              // ignore: deprecated_member_use
                              child: TextButton(
                                style: ButtonStyle(
                                    foregroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.white),
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.blue)),
                                onPressed: () {},
                                child: Text('Submit'),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        "Date",
                        style: TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "IN/Out",
                        style: TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "WH",
                        style: TextStyle(
                            fontSize: 14.0, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ))),
    );
  }

  @override
  double get maxExtent => height;

  @override
  double get minExtent => height;

  @override
  bool shouldRebuild(
          SliverPersistentHeaderDelegate sliverPersistentHeaderDelegate) =>
      true;
}

Widget buildMenu() {
  return SingleChildScrollView(
    padding: const EdgeInsets.symmetric(vertical: 10.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // ignore: missing_required_param

              Text(
                'Hello',
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
              SizedBox(height: 20.0),
            ],
          ),
        ),
        ListTile(
          dense: true,
          title: Text(
            "Dashboard",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
          ),
          leading: Icon(
            Icons.dashboard,
            color: Colors.white,
          ),
          //  onTap: () =>
        ),
        AppExpansionTile(
            title: Text(
              "Office Manager",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            leading: Icon(
              Icons.portrait_outlined,
              color: Colors.white,
            ),
            // backgroundColor: Colors.green,
            children: <Widget>[
              new ListTile(
                title: Text(
                  "My Attendance",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 13.0,
                  ),
                ),
                leading: Icon(
                  Icons.work_rounded,
                  color: Colors.white,
                ),
                dense: true,
                contentPadding: EdgeInsets.only(left: 50),
                onTap: () {},
              ),
              new ListTile(
                title: Text(
                  "My Profile",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 13.0,
                  ),
                ),
                leading: Icon(
                  Icons.person_rounded,
                  color: Colors.white,
                ),
                dense: true,
                contentPadding: EdgeInsets.only(left: 50),
                onTap: () {
                  // Navigator.of(context).pushNamed('/om_my_profile');
                  //setState(() {
                  // this.foos = 'Three';
                  // });
                },
              ),
              new ListTile(
                title: Text(
                  "Contect List",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 13.0,
                  ),
                ),
                dense: true,
                leading: Icon(
                  Icons.contact_phone,
                  color: Colors.white,
                ),
                contentPadding: EdgeInsets.only(left: 50),
                onTap: () {
                  //setState(() {
                  // this.foos = 'Three';
                  // });
                },
              ),
              new ListTile(
                title: Text(
                  "OM Notifications",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 13.0,
                  ),
                ),
                dense: true,
                leading: Icon(
                  Icons.notifications,
                  color: Colors.white,
                ),
                contentPadding: EdgeInsets.only(left: 50),
                onTap: () {
                  //setState(() {
                  // this.foos = 'Three';
                  // });
                },
              ),
              new ListTile(
                title: Text(
                  "Canteen Details",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 13.0,
                  ),
                ),
                dense: true,
                leading: Icon(
                  Icons.fastfood,
                  color: Colors.white,
                ),
                contentPadding: EdgeInsets.only(left: 50),
                onTap: () {
                  //setState(() {
                  // this.foos = 'Three';
                  // });
                },
              ),
              new ListTile(
                title: Text(
                  "Holiday List",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 13.0,
                  ),
                ),
                dense: true,
                leading: Icon(
                  Icons.calendar_today_rounded,
                  color: Colors.white,
                ),
                contentPadding: EdgeInsets.only(left: 50),
                onTap: () {
                  //setState(() {
                  // this.foos = 'Three';
                  // });
                },
              ),
            ]),
        ListTile(
          title: Text(
            "Reset Password",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
          ),
          leading: Icon(
            Icons.vpn_key,
            color: Colors.white,
          ),
          // onTap: () => ,
        ),
        ListTile(
          dense: true,
          title: Text(
            "Sacn QR",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
          ),
          leading: Icon(
            Icons.qr_code,
            color: Colors.white,
          ),
          // onTap: () => ,
        ),
        ListTile(
          title: Text(
            "Pay To Canteen",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
          ),
          leading: Icon(
            Icons.emoji_food_beverage_rounded,
            color: Colors.white,
          ),
          // onTap: () => navigateToPage(context, 'anotherPage'),
        ),
        ListTile(
          title: Text(
            "Logout",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
          ),
          leading: Icon(
            Icons.logout,
            color: Colors.white,
          ),
          //  onTap: () => navigateToPage(context, 'anotherPage'),
        ),
        ListTile(
          title: Text(
            "Exit",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
          ),
          leading: Icon(
            Icons.close,
            color: Colors.white,
          ),
          // onTap: () => navigateToPage(context, 'anotherPage'),
        ),
      ],
    ),
  );
}
