import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:vfas/locatioData.dart';

class DatabaseHelper {
  Database _database;
  int DATABASE_VERSION = 1;

  // Database Name
  String DATABASE_NAME = 'dblocation.db';
  String TABLE_RECORD = 'tblocation';

  final String KEY_ID = "liD";
  final String KEY_IMEI_NO = "imeino";
  final String KEY_EMAIL_ID = "emailID";
  final String KEY_MOB_LATITUDE = "mobilelati";
  final String KEY_MOB_LONGITUDE = "mobilelong";
  final String KEY_ADDRESS = "Address";
  final String KEY_DEV_DT_TM = "devDtTm";

  Future<Database> get database async {
    if (_database == null) {
      _database = await initializeDatabase();
    }
    return _database;
  }

  Future<Database> initializeDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + DATABASE_NAME;
    var todosDatabase = await openDatabase(path,
        version: DATABASE_VERSION, onCreate: _createDb);
    return todosDatabase;
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
        'CREATE TABLE $TABLE_RECORD($KEY_ID INTEGER, $KEY_IMEI_NO TEXT, '
        '$KEY_EMAIL_ID TEXT,$KEY_MOB_LATITUDE TEXT,$KEY_MOB_LONGITUDE TEXT,$KEY_ADDRESS TEXT, $KEY_DEV_DT_TM TEXT)');
  }

  // Fetch Operation: Get all todo objects from database
  Future<List<Map<String, dynamic>>> getTodoMapList() async {
    Database db = await this.database;
//		var result = await db.rawQuery('SELECT * FROM $todoTable order by $colTitle ASC');
    var result = await db.query(TABLE_RECORD, orderBy: '$KEY_ID ASC');
    return result;
  }

  // Insert Operation: Insert a todo object to database
  Future<int> insertTodo(LocationData todo) async {
    Database db = await this.database;
    var result = await db.insert(TABLE_RECORD, todo.toMap());
    return result;
  }

  // Update Operation: Update a todo object and save it to database
  Future<int> updateTodo(LocationData todo) async {
    var db = await this.database;
    var result = await db.update(TABLE_RECORD, todo.toMap(),
        where: '$KEY_ID = ?', whereArgs: [todo.ID]);
    return result;
  }

  // Delete Operation: Delete a todo object from database
  Future<int> deleteTodo(int id) async {
    var db = await this.database;
    int result =
        await db.rawDelete('DELETE FROM $TABLE_RECORD WHERE $KEY_ID = $id');
    return result;
  }

  // Get the 'Map List' [ List<Map> ] and convert it to 'todo List' [ List<Todo> ]
  Future<List<LocationData>> getTodoList() async {
    var todoMapList = await getTodoMapList(); // Get 'Map List' from database
    int count =
        todoMapList.length; // Count the number of map entries in db table
    List<LocationData> todoList = List<LocationData>();
    // For loop to create a 'todo List' from a 'Map List'
    for (int i = 0; i < count; i++) {
      todoList.add(LocationData.fromMapObject(todoMapList[i]));
    }

    return todoList;
  }

  // Get number of todo objects in database
  Future<int> getCount() async {
    Database db = await this.database;
    List<Map<String, dynamic>> x =
        await db.rawQuery('SELECT COUNT (*) from $TABLE_RECORD');
    int result = Sqflite.firstIntValue(x);
    return result;
  }
}
