import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/notification.dart';

void main() {
  runApp(MaterialApp(
    home: Count(),
  ));
}

class Count extends StatefulWidget {
  @override
  _CountState createState() => _CountState();
}

class _CountState extends State<Count> {
  var counter = '0';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCount();
  }

  getCount() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    counter = preferences.getString('notifyCount');
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        new IconButton(
            icon: Icon(
              Icons.notifications,
              size: 35,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new Notify(),
                ),
              ).then((value) => {});
            }),
        int.parse(counter) > 0
            ? new Positioned(
                right: 4,
                top: 5,
                child: new Container(
                  padding: EdgeInsets.all(2),
                  decoration: new BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 17,
                    minHeight: 17,
                  ),
                  child: Text(
                    '$counter',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
            : new Container()
      ],
    );
  }
}
