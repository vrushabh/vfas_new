import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:flutter/services.dart';
import 'package:vfas/dashboard.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(home: Fingerprint()));
}

class Fingerprint extends StatefulWidget {
  @override
  _FingerprintState createState() => _FingerprintState();
}

class _FingerprintState extends State<Fingerprint> {
  final LocalAuthentication auth = LocalAuthentication();

  @override
  void initState() {
    super.initState();
    _authenticateWithBiometrics();
  }

  Future<void> _authenticateWithBiometrics() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool authenticated = false;
    try {
      authenticated = await auth.authenticate(
          localizedReason: 'Scan your fingerprint or face to Open VFAS',
          useErrorDialogs: true,
          stickyAuth: true,
          biometricOnly: true);
    } on PlatformException catch (e) {
      print('errorrrr----' + e.toString());
      _authenticate();
      return;
    }
    if (!mounted) return;
    print(authenticated);
    if (authenticated) {
      preferences.setString('otpStatus', '1');

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new Dashboard(),
        ),
      ).then((value) => setState(() {}));
    } else {
      exit(0);
    }
  }

  Future<void> _authenticate() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    bool authenticated = false;
    try {
      authenticated = await auth.authenticate(
          localizedReason: 'Scan your fingerprint or face to Open VFAS',
          useErrorDialogs: true,
          stickyAuth: true);
    } on PlatformException catch (e) {
      print(e);

      return;
    }
    if (!mounted) return;
    print(authenticated);
    if (authenticated) {
      preferences.setString('otpStatus', '1');

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new Dashboard(),
        ),
      ).then((value) => setState(() {}));
    } else {
      exit(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal[600],
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 130,
              ),
              Icon(
                Icons.fingerprint,
                size: 70,
                color: Colors.white,
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                'One-touch Sign In',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
              SizedBox(
                height: 50,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 75, right: 45),
                child: Text(
                  'Please place your fingertip on the scanner to verify your Identify.',
                  style: TextStyle(color: Colors.white, fontSize: 15),
                ),
              ),
              SizedBox(
                height: 300,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Text(
                  '( Fingerprint sign in makes your app login much faster.Your device should have atleast one fingerprint registered in device settings. )',
                  style: TextStyle(color: Colors.grey[400], fontSize: 15),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
