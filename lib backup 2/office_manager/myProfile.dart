import 'package:flutter/material.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:vfas/dashboard.dart';
import 'package:vfas/notification.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/office_manager/canteen.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/holiday.dart';
import 'package:vfas/office_manager/myAttendance.dart';
import 'package:vfas/office_manager/omNotification.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/sidemenu.dart';

void main() {
  runApp(MaterialApp(home: Profile()));
}

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  var visibleApprove = false;
  String Username = '';
  String Email = '';
  String IMEI = '';
  String code = '';
  String type = '';
  String mobile = '';
  var name = '';
  bool loader = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmail();
  }

  getEmail() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      Username = preferences.getString('username');
      Email = preferences.getString('email');
      IMEI = preferences.getString('imei');
      code = preferences.getString('employeecode');
      type = preferences.getString('employeetype');
      mobile = preferences.getString('mobile');
      name = Username.split('.')[0];
      loader = false;
    });
    //print(code + '---' + type);
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
      background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
          appBar: AppBar(
            //centerTitle: true,
            backgroundColor: const Color(0xFF008577),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                final _state = _sideMenuKey.currentState;
                if (_state.isOpened)
                  _state.closeSideMenu();
                else
                  _state.openSideMenu();
              },
            ),
            actions: <Widget>[
            Count()
            ],
            title: Text('My Profile'),
          ),
          body: loader == true
              ? Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                )
              : Center(
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 160,
                        color: Colors.grey,
                        child: Column(
                          children: [
                            Icon(Icons.person, size: 80, color: Colors.white),
                            Text(
                              Username,
                              style:
                                  TextStyle(fontSize: 21, color: Colors.white),
                            ),
                            Text(
                              code,
                              style:
                                  TextStyle(fontSize: 19, color: Colors.white),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 35),
                        child: Text(
                          type,
                          style:
                              TextStyle(fontSize: 25, color: Colors.blue[300]),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: Row(
                          children: [
                            Icon(Icons.phone),
                            Text('    '),
                            Text(
                              mobile,
                              style: TextStyle(fontSize: 19),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(25, 5, 25, 0),
                        child: Row(
                          children: [
                            Icon(Icons.mail),
                            Text('    '),
                            Text(
                              Email,
                              style: TextStyle(fontSize: 19),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
