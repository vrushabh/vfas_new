import 'package:flutter/material.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:vfas/notification.dart';
import 'package:vfas/notifyCount.dart';
import 'package:vfas/office_manager/canteen.dart';
import 'package:vfas/office_manager/contact.dart';
import 'package:vfas/office_manager/myAttendance.dart';
import 'package:vfas/office_manager/myProfile.dart';
import 'package:vfas/office_manager/omNotification.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:ui';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vfas/sidemenu.dart';

void main() {
  runApp(MaterialApp(home: Holiday()));
}

class Holiday extends StatefulWidget {
  @override
  _HolidayState createState() => _HolidayState();
}

class _HolidayState extends State<Holiday> {
  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();
  var visibleApprove = false;
  List holidayList = [];
  var loadingData = true;
  final yearController = TextEditingController();

  String Email = '', employeecode = '', employeetype = '';

  @override
  void initState() {
    super.initState();
    DateTime date = DateTime.now();

    var yearFormat1 = new DateFormat('yyyy');
    var finalyear = yearFormat1.format(date);
    yearController.text = finalyear;
    this.getNotification(finalyear);
  }

  getNotification(year) async {
    setState(() {
      loadingData = true;
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    employeetype = preferences.getString('employeetype');

    var body = {'year': year, 'employeeType': employeetype};

    print(body);
    print(jsonEncode(body));

    var response = await http.post(
        Uri.parse('https://www.mkttech.in/OMService/OMJsonService.svc/Holiday'),
        headers: {'Content-Type': 'application/json', 'Charset': 'utf-8'},
        body: jsonEncode(body));

    var data = json.decode(response.body);

    print(data);

    setState(() {
      holidayList = data;
      loadingData = false;
    });
    print(holidayList);
  }

  @override
  Widget build(BuildContext context) {
    return SideMenu(
      key: _endSideMenuKey,
      inverse: false, // end side menu
       background: const Color(0xFF008577),
      type: SideMenuType.slideNRotate,
      menu: Sidemenu(),

      child: SideMenu(
        key: _sideMenuKey,
        background: const Color(0xFF008577),
        menu: Sidemenu(),
        type: SideMenuType.shrinkNSlide,
        child: Scaffold(
            appBar: AppBar(
             // centerTitle: true,
             backgroundColor: const Color(0xFF008577),
              leading: IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {
                  final _state = _sideMenuKey.currentState;
                  if (_state.isOpened)
                    _state.closeSideMenu();
                  else
                    _state.openSideMenu();
                },
              ),
              actions: <Widget>[
              Count()
            ],
              title: Text('Holiday'),
            ),
            body: Center(
              child: loadingData == true
                  ? Container(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(),
                    )
                  : Column(
                      children: [
                        Container(
                          height: 60,
                          width: double.infinity,
                          color: const Color(0xFF008577),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 7,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 20),
                                    child: TextField(
                                      readOnly: true,
                                      controller: yearController,
                                      style: TextStyle(color: Colors.white),
                                      decoration: InputDecoration(
                                         // border: InputBorder.none,
                                          //  hintText: 'Select Year',
                                          hintStyle:
                                              TextStyle(color: Colors.white)),
                                      onTap: () {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              title: Text("Select Year"),
                                              content: Container(
                                                width: 300,
                                                height: 300,
                                                child: YearPicker(
                                                  firstDate: DateTime(
                                                      DateTime.now().year - 10,
                                                      1),
                                                  lastDate: DateTime(
                                                      DateTime.now().year + 30,
                                                      1),
                                                  initialDate: DateTime.now(),
                                                  selectedDate: DateTime.now(),
                                                  onChanged:
                                                      (DateTime dateTime) {
                                                    print(dateTime);

                                                    yearController.text =
                                                        dateTime
                                                            .toString()
                                                            .substring(0, 4);
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ),
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  )),
                              // Expanded(flex: 2,child: Text(''),),

                              Expanded(
                                  flex: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 15),
                                    child: Container(
                                      alignment: Alignment.centerRight,
                                      child: IconButton(
                                          onPressed: () {
                                            print(yearController.text);
                                            getNotification(yearController.text);
                                          },
                                          icon: Icon(Icons.search,
                                              size: 35, color: Colors.white)),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                         Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      height: 50,
                                      color: Colors.grey[700],
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            right: 5, left: 5),
                                        child: Row(
                                          children: [
                                            Expanded(
                                                flex: 3,
                                                child: Center(
                                                    child: Text(
                                                  'Date',
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white),
                                                ))),
                                            Expanded(
                                                flex: 2,
                                                child: Center(
                                                    child: Text(
                                                  'Day',
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white),
                                                ))),
                                            Expanded(
                                                flex: 5,
                                                child: Center(
                                                    child: Text(
                                                  'Holiday',
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white),
                                                )))
                                          ],
                                        ),
                                      ),
                                    ),
                                    
                                    Expanded(
                                        child: ListView.builder(
                                            padding: const EdgeInsets.all(8),
                                            itemCount:
                                                holidayList
                                                    .length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return Container(
                                                  height: 70,
                                                  width: double.infinity,
                                                  decoration: BoxDecoration(
                                                      border: Border(
                                                          bottom: BorderSide(
                                                              width: 1.0,
                                                              color: Colors
                                                                  .grey[400]))),
                                                  child: Row(children: [
                                                    Expanded(
                                                        flex: 3,
                                                        child: Container(
                                                            // alignment: Alignment.center,
                                                            child: Text(
                                                                holidayList
                                                                        [index][
                                                                    'holiday_date'],
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16,
                                                                    color: Colors
                                                                            .blue[
                                                                        300])))),
                                                    Expanded(
                                                        flex: 3,
                                                        child: Container(
                                                            alignment: Alignment.center,
                                                            child: Text(
                                                                holidayList
                                                                        [index]
                                                                    ['dayname'],
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 16,
                                                                )))),
                                                   
                                                    Expanded(
                                                        flex: 5,
                                                        child: Center(
                                                          child: Padding(
                                                            padding: const EdgeInsets.only(left: 25),
                                                            child: Container(
                                                                alignment: Alignment.centerLeft,
                                                                child: Text(
                                                                   holidayList
                                                                            [
                                                                            index]
                                                                        [
                                                                        'desc'],
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          16,
                                                                    ))),
                                                          ),
                                                        )),
                                                  ]));
                                            }))
                                  ],
                                ),
                              ),
                      ],
                    ),
            )),
      ),
    );
  }
}
