/**
 * Automatically generated file. DO NOT MODIFY
 */
package octmon.flutter_des;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "octmon.flutter_des";
  public static final String BUILD_TYPE = "debug";
}
