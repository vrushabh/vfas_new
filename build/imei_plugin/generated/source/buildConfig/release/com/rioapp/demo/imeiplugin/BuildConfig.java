/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.rioapp.demo.imeiplugin;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "com.rioapp.demo.imeiplugin";
  public static final String BUILD_TYPE = "release";
}
